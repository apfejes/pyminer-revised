import argparse
import os
from PIL import Image

from modules.common_functions import make_file, run_cmd

BR = '<br></br>\n'


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument('-base_dir', '-in', '-i', '-input', dest='base_dir', type=str, required=True)
    parser.add_argument('-launch', action='store_true', default=False)

    args = parser.parse_args()
    return args


def add_h2(title):
    return "\n\t<h2>" + str(title) + "</h2>\n"


def add_h3(title):
    return "\n\t<h3>" + str(title) + "</h3>\n"


def add_p(p):
    return '\t\t\t<p>' + str(p) + '</p>\n'


def add_file_link(f, name):
    return '<a href=' + str(f) + '>' + str(name) + '</a>\n'


def add_file_link_list(f, name):
    return '<li><a href=' + str(f) + '>' + str(name) + '</a></li>\n'


def add_button_head(text):
    return """<button class="collapsible">""" + str(text) + """</button>
<div class="content">"""


def parse_file_name(img):
    old_name = img[:-4]
    temp_name = old_name.split('_')
    temp_name = ' '.join(temp_name)
    return old_name, temp_name


def add_statistics():
    sig_dir = "sample_clustering_and_summary/significance/"
    stats = [add_button_head("Statistics"),
             add_h3("basic stats"),
             add_p("If you find some interesting genes that are different between groups, here are the:"),
             add_file_link("sample_clustering_and_summary/k_group_means.txt", 'group means'),
             add_p('and...'),
             add_file_link("sample_clustering_and_summary/k_group_sd.txt", 'group standard deviations'),
             add_p('as well as the'),
             add_file_link("sample_clustering_and_summary/k_group_enrichment.txt", 'group level Z-scores'),
             add_h3("statistical comparisons"),
             add_file_link(sig_dir + 'groups_1way_anova_results.txt', "Benjamini-Hoschberg corrected 1-way Anovas"),
             '\n</div>\n']
    return "".join(stats)


def main():

    args = parse_arguments()

    mw = MakeWebpage(args.base_dir, args.launch)
    mw.make_webpage()


class MakeWebpage(object):

    def __init__(self, file_path, launch=False):

        self.launch = launch
        self.file_path = file_path
        if not self.file_path.endswith('/'):
            self.file_path += '/'
        self.output_file = self.file_path + "PyMINEr_summary.html"

    def add_img(self, img, alt):
        w, h = self.get_img_dims(img)
        return '<div><img src="' + str(img) + '" alt="' + str(alt) + '" width="' + str(w) + '" height="' + str(
            h) + '"></div>\n'

    def get_img_dims(self, img):
        if os.path.isfile(self.file_path + img):
            image = Image.open(self.file_path + img)
        elif os.path.isfile(img):
            image = Image.open(img)
        else:
            return 200, 200

        w, h = image.size
        # reduce size so it fits on the screne
        reduction_factor = w / 600
        return int(w / reduction_factor), int(h / reduction_factor)

    def add_img_list(self, list_of_images, directory):
        output = []
        for img in list_of_images:
            new, old = parse_file_name(img)
            output.append(add_p(new))
            output.append(self.add_img(directory + img, new))
        return "".join(output)

    def add_autocrine_paracrine(self):
        ap_dir = "autocrine_paracrine_signaling/"
        ap = []
        if not os.path.isdir(self.file_path + ap_dir):
            return ""
        else:
            ap.append(add_button_head("Autocrine/paracrine signaling"))
            ap.append(add_p("Below are the predicted signaling networks"))
            ap.append(add_file_link(ap_dir + 'all_cell_cell_interaction_summary.txt',
                                    'Number of interactions between all of the groups'))
            ap.append(BR)
            ap.append(add_file_link(ap_dir + 'all_cell_type_specific_interactions.txt',
                                    'A detailed summary of each autocrine/paracrine interaction'))
            ap.append(BR)
            ap.append(add_file_link(ap_dir + 'all_cell_type_specific_interactions_gprofiler.txt',
                                    'A detailed summary of the pathways signaling across and within groups'))
            ap.append(BR)
            ap.append(add_file_link(ap_dir + 'combined_neg_log10p_gprofiler.txt',
                                    'A table of negative log10 p-values that for each pathway in each '
                                    'interaction. (Zero just means it did not reach signficance)'))
            ap.append(BR)
            ap.append(add_file_link(ap_dir + 'individual_class_importance.txt',
                                    'A table of the individual importance of each pathway for the given group'))

        return "".join(ap)

    def add_gene_annotations(self):
        anno = []
        if os.path.isfile(self.file_path + "annotations.tsv"):
            anno.append(add_p(
                "Here are the annotations for your genes. You can use this in excel using v-lookup if you "
                "want to get gene symbols or definitions for any of the other files."))
            anno.append(add_file_link("annotations.tsv", "Annotation file"))
        if os.path.isfile(self.file_path + "human_orthologues.tsv"):
            anno.append(BR)
            anno.append(add_file_link("human_orthologues.tsv", "Human orthologues to your genes"))
        anno.append(BR)
        return "".join(anno)

    def add_high_marker_genes(self):
        marker = []
        marker_dir = "sample_clustering_and_summary/significance/high_markers/"
        if not os.path.isfile(self.file_path + marker_dir + 'marker_gene_annotations.tsv'):
            return ""
        else:
            marker.append(add_button_head("Highly expressed group-specific markers"))
            marker.append(add_p(
                "PyMINEr analyzes the mean expression of each sample group and then looks for genes"
                " that meet three criteria."))
            marker.append("\t1) The gene is significant by 1-way ANOVA (after BH correction, alpha=0.05).")
            marker.append(add_p(
                "\t2) The distance between the group with highest mean expression and second highest "
                "expression is calculated. If a gene is in the top 90th percentile of this calculation, "
                "it can make it through."))
            # marker_str+=BR
            marker.append(add_p(
                "\t3) A metric called the q-value is calculated (usually to identify outliers). "
                "The q-value is the ratio of the value calculated in number 2 compared to the range "
                "of sample group means. It's essentially what percent of the range is attributable to "
                "the distance between the highest expressing group vs the second highest expressing group."))
            # marker_str+=BR
            marker.append(add_p(
                "If all three of these criteria are, met you'll find it with an annotated group in "
                "the file below. Note that if you have several very closely related groups, there "
                "might not be many highly expressed genes that are exclusivly expressed in an individual "
                "group. In this case, you might need several markers at once to descriminate them."))
            # marker_str+=BR
            marker.append(add_file_link(marker_dir + 'marker_gene_annotations.tsv', "Highly expressed marker genes"))
            marker.append(self.add_img(marker_dir + 'genes_of_interest_subset_heatmap.png',
                                       "High expression marker genes"))
            marker.append('\n</div>\n')
            return "".join(marker)

    def add_genes_of_interest(self):
        goi_dir = 'genes_of_interest/'
        goi_str = []
        if os.path.isdir(self.file_path + goi_dir):
            goi_str.append(add_button_head('Genes of interest'))
            goi_str.append(add_p(
                "An interesting way to use the structure of the graphs generated by PyMINEr is looking "
                "at how far away all genes in your dataset are away from your genes of interest. We've "
                "shown in the PyMINEr paper that there are many types of functional enrichment that "
                "correlate with how far away a gene is from another gene in the graph network. For "
                "example, close to a transcription factor are more likely to have a binding site for "
                "that transcription factor when compared to a gene that's father away from it. There "
                "is also an increased probability that two genes will encoded proteins that have a "
                "physical intereaction when those two genes are directly connected (i.e. 1-degree of "
                "separation)."))
            goi_str.append(add_p(
                'below is a file containing a table that has the distance of all genes in the genome '
                'away from your gene(s) of interest.'))
            goi_str.append(add_file_link(goi_dir + 'genes_of_interest_shortest_path_list.txt',
                                         'Shortest path of all genes away from your genes of interest'))
            goi_str.append(add_p('Below is a heatmap of your genes of interest:'))
            goi_str.append(self.add_img(goi_dir + 'genes_of_interest_subset_heatmap.png', 'genes of interest heatmap'))

            # collect the other images
            additional_images = []
            for f in os.listdir(self.file_path + goi_dir):
                if f.endswith('.png') and f != 'genes_of_interest_subset_heatmap.png':
                    additional_images.append(f)
            goi_str.append(add_button_head("additional plots for your genes of interest"))
            goi_str.append(self.add_img_list(sorted(additional_images), goi_dir))
            goi_str.append('\n\t\t</div>\n')
            goi_str.append("\n</div>\n")

        return "".join(goi_str)

    def add_graph(self):
        graph_str = [add_button_head("Expression graphs"),
                     add_h3("Adjacency Lists:")]
        # graph_str+=add_h2("Expression graphs")
        adj_list_list = []
        for f in os.listdir(self.file_path):
            if 'adj_list' in f:
                adj_list_list.append(f)

        total_adj = None
        pos_adj = None
        neg_adj = None

        for f in adj_list_list:
            if '_pos.tsv' in f:
                pos_adj = f
            elif '_neg.tsv' in f:
                neg_adj = f
            else:
                total_adj = f
        # graph_str+=add_p('Full adjacency list:')[:-6]+'</p>'
        graph_str.append("\t\t\t\t<ul>\n")
        graph_str.append(add_file_link(total_adj, 'Full adjacency list'))
        graph_str.append(BR)
        graph_str.append(add_file_link(pos_adj, 'Positive correlation (co-expression) adjacency list'))
        graph_str.append(BR)
        graph_str.append(add_file_link(neg_adj, 'Negative correlation adjacency list'))
        graph_str.append(BR)
        graph_str.append("\t\t\t\t</ul>\n")

        graph_str.append(add_h2("Co-expression graph plots:"))
        coexpression_graph_dir = "pos_cor_graphs/"
        graph_str.append(self.add_img(coexpression_graph_dir + "full_graph.png", 'Co-expression graph'))
        graph_str.append(add_h3(
            "Here are the Z-scores for each group overlaid on the co-expression graph (red means "
            "it's enriched in that group, blue means it's low expression or not expressed):"))

        # find all of the pertinent files, and
        graph_str.append(add_button_head('Z-score overlaid co-expression graphs'))
        extra_images = []
        if not os.path.isdir(self.file_path + coexpression_graph_dir):
            return "".join(graph_str)

        for file in os.listdir(self.file_path + coexpression_graph_dir):
            if file.endswith('.png') and 'sample_group' in file:
                extra_images.append(file)

        extra_images.sort()
        for img in extra_images:
            new, old = parse_file_name(img)
            graph_str.append(add_p(new))
            graph_str.append(self.add_img(coexpression_graph_dir + img, new))
        graph_str.append("\n\t\t</div>\n")
        graph_str.append(BR)
        graph_str.append("\n\t</div>\n")
        return "".join(graph_str)

    def add_gene_enrichment(self):
        sig_dir = "sample_clustering_and_summary/significance/"
        enrich = [add_button_head("Gene enrichment in groups"),
                  add_h3("Significantly enriched genes in each group"),
                  add_p("Below is a file that contains a table with genes on the left, and groups in columns. If a "
                        "gene is considered significantly enriched, that means that the BH corrected 1-way Anova "
                        "was significant, and the gene had a high Z-score in that particular group. If a gene is "
                        "significantly enriched in that group, the value is True in the corresponding cell in "
                        "the table, False if it was not significantly enriched."),
                  add_file_link(sig_dir + "/significant_and_enriched_boolean_table.txt",
                                "True/False significantly enriched table"),
                  add_p("alternatively, you can use these separated files that simply have the list of significantly "
                        "enriched genes for each group:"),
                  add_button_head('Genes enriched in each group')]
        enrich_files = []

        for item in os.listdir(self.file_path + sig_dir):
            if item.endswith("*_significant_enriched.txt"):
                enrich_files.append(item)

        if enrich_files:
            enrich_files.sort()
            enrich.append("\t\t\t\t<ul>\n")
            for img in enrich_files:
                old_name = img[:-4]
                # enrich_str+=add_p(temp_name)
                enrich.append(add_file_link(sig_dir + img, old_name))
                enrich.append(BR)

            enrich.append("\t\t\t\t</ul>\n")

        enrich.append("\t\t</div>")  # end the sub-button
        enrich.append(BR)

        pathway_dir = sig_dir + 'gprofiler/'
        enrich_path = []
        for item in os.listdir(self.file_path + pathway_dir):
            if item.endswith(".txt"):
                enrich_path.append(item)

        if len(enrich_path) > 0:
            enrich_path.sort()
            # enrich_str+=add_h3('Pathway Enrichment for Each Group:')
            enrich.append(add_button_head('Pathway enrichment for each group'))
            if os.path.isfile(self.file_path + sig_dir + "/combined_neg_log10p_gprofiler.txt"):
                enrich.append(add_p(
                    "Below is a combined file with all of the pathways that came out of the analysis of "
                    "the above genes, enriched in the different groups. We've also created a new algorithm "
                    "for ranking the importance of these pathways (not to boast, but I'm kind of proud "
                    "of it). It's based on the information/entropy of the -log10(p-vals). If you look at "
                    "the -log10(p-vals) at the top of the list, you should find pathways that are really "
                    "high in some groups and really low in other groups. The overall formula is calculated "
                    "by taking the sum(KL-divergance)*range(-log10(p-vals)). The KL-divergence is looking "
                    "for a difference in the distribution between the observed -log10(p-vals) and the "
                    "distribution expected from an uninformative vector of -log10(p-vals) (this is a "
                    "Gaussian null hypothesis). Then we multiply the sum(KL-divergance) by the range of "
                    "-log10(p-vals) to let the most significant pathways with lots of information/low "
                    "entropy rise to the top."))
                enrich.append(
                    add_file_link(self.file_path + sig_dir +
                                  "/combined_neg_log10p_gprofiler.txt", 'combined pathway analyses'))
                enrich.append(add_p(
                    "and here is a file that has a normalized metric that ranks each pathway for their "
                    "individual importance within each group. It would be useful to sort each of these "
                    "and see what rises to the top for each 'cell type' or whatever your groups are."))
                enrich.append(
                    add_file_link(self.file_path + sig_dir +
                                  '/individual_class_importance.txt', 'individual class importance'))
                enrich.append(add_p(
                    "Below are all of the individual results so that you find which genes were "
                    "in which pathways in individual groups:"))
                enrich.append(add_button_head("individual pathway files"))
            enrich.append("\t\t\t\t<ul>\n")
            for img in enrich_path:
                old_name = img[:-4]
                # temp_name = old_name.split('_')
                # temp_name = ' '.join(temp_name)
                # enrich_str+=add_p(temp_name)
                enrich.append(add_file_link(pathway_dir + img, old_name))
                enrich.append(BR)

            if os.path.isfile(self.file_path + sig_dir +
                              "/combined_neg_log10p_gprofiler.txt"):
                enrich.append("\n\t\t</div>\n")

            enrich.append("\t\t\t\t</ul>\n")
            enrich.append("\n\t\t</div>\n")  # end the sub-button
            enrich.append(BR)

        enrich.append('\n</div>\n')
        return "".join(enrich)

    def add_clustering(self):
        cluster = [add_button_head('Clustering')]

        # check if anti_correlation clustering was done
        if os.path.isfile(self.file_path + 'sig_neg_count_vs_total_neg_count.png'):
            # if it is, start annotating
            cluster.append(add_h3("Negative Control Bootstrap Shuffling"))
            cluster.append(add_p(
                "Here, PyMINEr shuffled up your data to make it randomized. This will maintain the "
                "overall distribution of your data, while at the same time randomizing it so that we "
                "can come up with a reasonable cutoff for performing the anti-correlation based feature "
                "selection. Shown below is the distribution of all randomized Spearman rhos."))
            cluster.append(self.add_img("boostrap_cor_rhos.png", 'boot_all'))
            cluster.append(add_p("Here are just the negative correlations from the shuffled up dataset."))
            cluster.append(self.add_img("boostrap_neg_cor_rhos.png", 'boot_neg'))
            cluster.append(add_p(
                "Here is a scatter plot that shows the number of total negative correlations observed "
                "for each gene (y-axis), and the log2 number of significant negative correlations "
                "(x-axis). All of the genes to the left/below the black line were used for clustering."))
            cluster.append(self.add_img("sig_neg_count_vs_total_neg_count.png", "sig_vs_total_neg"))
            cluster.append(add_p(
                "Similarly, here is a plot showing the ratio of significant to non-sigificant. "
                "Everything to the right was used for clustering."))

        # link to the file with all of the sample annotations
        cluster.append(add_h3("Clustering Results:"))
        cluster.append(
            add_file_link("sample_clustering_and_summary/sample_k_means_groups.txt", 'sample group annotations'))

        # get the images from the sample_clustering_and_summary folder

        additional_images = []
        for file in os.listdir(self.file_path + "sample_clustering_and_summary/"):
            if file.endswith(".png"):
                additional_images.append(file)

        if len(additional_images) > 0:
            cluster.append(add_h3('Here are some additional images:'))
            for img in additional_images:
                old_name = img[:-4]
                temp_name = old_name.split('_')
                temp_name = ' '.join(temp_name)
                cluster.append(add_p(temp_name))
                cluster.append(self.add_img("sample_clustering_and_summary/" + img, old_name))

        cluster.append('\n</div>\n')
        return "".join(cluster)

    def make_webpage(self):

        # first add the generic heading
        out_web = ["""<!DOCTYPE html>
        <html>
        
        <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        .collapsible {
            background-color: #777;
            color: white;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
        }
        
        .active, .collapsible:hover {
            background-color: #555;
        }
        
        .content {
            padding: 0 18px;
            display: none;
            overflow: hidden;
            background-color: #f1f1f1;
        }
        </style>
        </head>
        
        <body>
        
        
        
        
        
        <h1>PyMINEr results</h1>
        
        
        """,
                   self.add_gene_annotations(),
                   self.add_clustering(),
                   add_statistics(),
                   self.add_gene_enrichment(),
                   self.add_high_marker_genes(),
                   self.add_graph(),
                   self.add_genes_of_interest(),
                   self.add_autocrine_paracrine(),
                   """
       
       
       <script>
       var coll = document.getElementsByClassName("collapsible");
       var i;
       
       for (i = 0; i < coll.length; i++) {
           coll[i].addEventListener("click", function() {
               this.classList.toggle("active");
               var content = this.nextElementSibling;
               if (content.style.display === "block") {
                   content.style.display = "none";
               } else {
                   content.style.display = "block";
               }
           });
       }
       </script>
       
       
       
       </body>
       </html>
       
       
       
       """]
        ###########################################################
        for idx, item in enumerate(out_web):
            out_web[idx] = item.replace('//', '/')
            out_web[idx] = out_web[idx].replace(self.file_path, '')

        out_web = "".join(out_web)
        make_file(out_web, self.output_file)
        if self.launch:
            run_cmd(['firefox', self.output_file])


if __name__ == "__main__":
    main()
