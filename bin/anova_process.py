from scipy.stats.mstats import f_oneway as aov

from modules.multiprocessing.generic_process import GenericProcess


class AnovaProcess(GenericProcess):
    def __init__(
        self,
        input_queue,
        output_queue,
        error_queue,
        terminator_obj,
        terminators_expected,
        death_obj,
        process_name,
        full_expression,
        list_of_k_sample_indices,
    ):

        GenericProcess.__init__(
            self,
            incoming_queue=input_queue,
            outgoing_queue=output_queue,
            error_queue=error_queue,
            death_obj=death_obj,
            terminator_obj=terminator_obj,
            terminators_expected=terminators_expected,
            process_name=process_name,
        )

        self.full_expression = full_expression
        self.list_of_k_sample_indices = list_of_k_sample_indices

    def start_up(self):
        pass

    def close_and_place_terminators(self):
        pass

    def get_anova(self, index):
        list_of_group_values = []
        for group in self.list_of_k_sample_indices:
            list_of_group_values.append(self.full_expression[index, group])
        return index, list(aov(*list_of_group_values))

    def process_items(self, item):
        values = self.get_anova(item)
        if values:
            self.outgoing_queue.put(values)
