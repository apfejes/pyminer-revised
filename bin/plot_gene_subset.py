import argparse
import os
import h5py
import itertools
import logging

from matplotlib import use
use('Agg')

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from modules.common_functions import read_file, read_table, import_dict, run_cmd

logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument(
        '-infile', '-in', '-i', '-input',
        dest='infile',
        type=str)

    parser.add_argument("-nodes_of_interest", "-noi", "-nodes",
                        help="the nodes that you want to subset and get their degrees of separation from")

    parser.add_argument("-out_dir", "-o", "-out",
                        help="the directory to place the output file(s)", required=True)

    parser.add_argument("-cluster_pkl",
                        help="the file created by clustering.py that stores the plots")

    parser.add_argument(
        '-no_cluster', '-no_clust',
        help="if you don't want the heatmap to cluster the genes together, but rather, "
             "just want it to be in the order that you input them.",
        dest="cluster",
        default=True,
        action='store_false')
    parser.add_argument(
        '-only_heatmap', '-heatmap',
        help="if you only want the heatmap",
        dest="only_heatmap",
        default=False,
        action='store_true')

    # hdf5 options
    parser.add_argument(
        '-hdf5',
        help='The input file is an HDF5 file',
        default=False,
        action='store_true')

    parser.add_argument(
        "-ID_list", "-ids", "-id_list",
        help='If we are using an hdf5 file, give the row-wise IDs in this new line delimeted file',
        type=str, required=True)

    args = parser.parse_args()
    return args


def process_noi(nodes_of_interest):
    noi_table = []
    for i in range(0, len(nodes_of_interest)):
        if '\t' in nodes_of_interest[i]:
            noi_table.append(nodes_of_interest[i].split('\t'))
        elif '::' in nodes_of_interest[i]:
            noi_table.append(nodes_of_interest[i].split('::'))
        else:
            noi_table.append([nodes_of_interest[i], nodes_of_interest[i]])

    # also make the ailias look-up dictionary
    alias_dict = {}
    # make the string that would be passed through to command line if needed
    pass_through_arg = ""
    # and the final list of nodes in the dataset
    final_nodes = []
    for i in range(0, len(noi_table)):
        alias_dict[noi_table[i][0]] = noi_table[i][1]
        pass_through_arg += "," + noi_table[i][0] + '::' + noi_table[i][1]
        final_nodes.append(noi_table[i][0])
    # remove the leading comma
    pass_through_arg = pass_through_arg[1:]

    return alias_dict, pass_through_arg, final_nodes


def ids_to_idxs(in_ids, id_hash):
    temp_index_list = [id_hash[i] for i in in_ids]
    return temp_index_list


def linear_normalize(vect):
    vect = vect - min(vect)
    vect = vect / max(vect)
    return vect


def vect_to_color(vect, style='hot'):
    vect = linear_normalize(vect)
    cmap = cm.get_cmap(name=style)
    return cmap(vect)


def draw_plot(plot, full_expression, id_hash, colorize_by_gene=None):
    plt.clf()

    # get the colorized vector if need be
    if colorize_by_gene is not None:
        # fix for entrez..
        try:
            float(colorize_by_gene)
        except ValueError:
            colorize_by_gene = colorize_by_gene
        else:
            colorize_by_gene = str(float(colorize_by_gene))
        expression_vect = full_expression[id_hash[colorize_by_gene], :]
        colorized_vect = vect_to_color(expression_vect)
        plt.scatter(plot["x"], plot["y"],
                             edgecolor='k',  # plot["c"],
                             facecolor=colorized_vect,
                             s=10)
    else:
        plt.scatter(plot["x"], plot["y"], color=plot["c"], s=10)

    plt.xlabel(plot['xlab'])
    plt.ylabel(plot['ylab'])

    return plt


def lin_norm_rows(in_mat):
    in_mat = np.transpose(np.array(in_mat))
    in_mat = in_mat - np.min(in_mat, axis=0)
    in_mat = in_mat / np.max(in_mat, axis=0)
    in_mat[np.isnan(in_mat)] = 0
    return np.transpose(in_mat)


def names_to_alias(names, alias_dict):
    aliases = []
    for name in names:
        # fix for entrez..
        try:
            float(name)
        except ValueError:
            temp_id = name
        else:
            temp_id = "{:.0f}".format(name)
        aliases.append(alias_dict[temp_id])
    return aliases


def main():
    args = parse_arguments()

    plot_gene_subset(nodes_of_interest_data=args.nodes_of_interest,
                     id_list_file=args.ID_list,
                     hdf5=args.hdf5,
                     infile=args.infile,
                     out_dir=args.out_dir,
                     cluster_pkl=args.cluster_pkl,
                     cluster=args.cluster,
                     only_heatmap=args.only_heatmap)


def plot_gene_subset(nodes_of_interest_data,
                     id_list_file,
                     hdf5,
                     infile,
                     out_dir,
                     cluster_pkl,
                     cluster=True,
                     only_heatmap=False
                     ):
    if nodes_of_interest_data is not None:
        # check if it's a file
        if os.path.isfile(nodes_of_interest_data):
            nodes_of_interest_raw = read_file(nodes_of_interest_data, 'lines')
        else:
            # check if it's a comma separated list of ids
            nodes_of_interest_raw = nodes_of_interest_data.split(',')
    else:
        nodes_of_interest_raw = None
    # check if there are aliases
    alias_dict, pass_through_arg, temp_nodes_of_interest = process_noi(nodes_of_interest_raw)
    if id_list_file is not None:
        id_list = read_file(id_list_file, "lines")
        id_hash = {}
        for i in range(0, len(id_list)):
            id_hash[id_list[i]] = i
    else:
        raise Exception('-ID_list </path/to/file> is a required argument')

    # double check that the ids are found
    nodes_of_interest = []
    for i in range(0, len(temp_nodes_of_interest)):
        # fix for entrez..
        try:
            float(temp_nodes_of_interest[i])
        except Exception:
            temp_id = temp_nodes_of_interest[i]
        else:
            temp_id = str(float(temp_nodes_of_interest[i]))
        if temp_id in id_hash:
            nodes_of_interest.append(temp_id)
        else:
            logger.warning("couldn't find '{}'".format(temp_id))
    if not nodes_of_interest:
        raise Exception("couldn't find any of the desired genes")

    # import expression data
    if not hdf5:
        full_expression_str = read_table(infile)
        full_expression_np = np.array(full_expression_str)
        id_list = full_expression_np[1:, 0]
        full_expression = np.array(full_expression_np[1:, 1:], dtype=float)
    else:
        id_list = read_file(id_list_file, 'lines')
        logger.info('making a maliable hdf5 file to preserve the original data')
        run_cmd(['cp', infile, infile + '_copy'])
        logger.info('reading in hdf5 file')
        infile_path = infile + '_copy'
        h5f = h5py.File(infile_path, 'r+')
        full_expression = h5f["infile"]

    id_hash = {name: idx for idx, name in enumerate(id_list)}
    # read in the plots
    cluster_result_dict = import_dict(cluster_pkl)
    linear_groups = cluster_result_dict['linear_groups']
    sample_k_lists = cluster_result_dict["sample_k_lists"]
    if not out_dir.endswith('/'):
        out_dir += '/'

    # plot the heatmap
    group_reordering_vector = np.array(cluster_result_dict['group_reordering_vector'])
    reordered_colors = np.array(cluster_result_dict['reordered_colors'])
    # get the column labels
    reordered_array = list(np.array(linear_groups)[np.array(group_reordering_vector)])
    reordered_groups = [reordered_array[0]]

    for i in range(1, len(reordered_array)):
        if int(reordered_array[i]) != int(reordered_groups[-1]):
            reordered_groups.append(reordered_array[i])

    temp_label_lists = []
    for i in range(len(sample_k_lists)):
        temp_list = [''] * len(sample_k_lists[i])
        temp_list[int(len(temp_list) / 2)] = i
        temp_label_lists.append(temp_list)

    heatmap_x_labels = list(itertools.chain.from_iterable(temp_label_lists))

    original_order = ids_to_idxs(nodes_of_interest, id_hash)
    original_order_dict = {j: i for i, j in enumerate(original_order)}
    subset_indices = sorted(original_order)
    original_order_vector = []
    for i in subset_indices:
        # fix for entrez..
        try:
            float(id_list[i])
        except ValueError:
            temp_id = id_list[i]
        else:
            temp_id = "{:.0f}".format(id_list[i])
        logger.debug('{} {} {} {}'.format(i, original_order_dict[i], temp_id, alias_dict[temp_id]))
        original_order_vector.append(original_order_dict[i])
    original_order_rank = np.zeros((len(original_order_vector)), dtype=int)
    for i in range(0, len(original_order_vector)):
        index = original_order_vector[i]
        original_order_rank[index] = i
    original_order_rank = original_order_rank.tolist()
    original_order_names = np.array(id_list)[original_order]
    subset_sorted_names = np.array(id_list)[subset_indices]
    subset_indices = np.array(subset_indices)
    if np.shape(subset_indices)[0] == 0:
        raise Exception('no subset found...')
    subset_array = np.array(full_expression[subset_indices, :])

    # do the linear normalization
    subset_array = lin_norm_rows(subset_array[:, group_reordering_vector])
    subset_array = subset_array - np.transpose(np.array([np.mean(subset_array, axis=1)]))
    name_col = np.array(names_to_alias(subset_sorted_names, alias_dict))

    name_col_df = pd.DataFrame({"name_col": name_col}, index=list(range(len(subset_sorted_names))))
    subset_df = pd.DataFrame(data=subset_array)
    subset_df = pd.concat([name_col_df, subset_df], axis=1)
    subset_df = subset_df.set_index([list(original_order_rank), "name_col"])
    subset_df.columns = heatmap_x_labels

    # set some stuff around if we are or are not clustering the rows
    if not cluster:
        logger.debug(original_order_rank)
        subset_df = subset_df.iloc[original_order_rank, :]
        logger.debug(subset_df)
        row_cluster = False
    else:
        row_cluster = True
    if not cluster:
        original_order_aliases = names_to_alias(original_order_names, alias_dict)
        for i in (zip(original_order_names, original_order_aliases)):
            logger.debug(i)
    global_cmap = sns.color_palette("coolwarm", 256)

    # actually plot the heatmap finally...
    # get the non-empty colnames
    indices_to_plot_for_names = np.where(np.array(heatmap_x_labels) != '')[0]
    none_indices = np.where(np.array(heatmap_x_labels) == '')[0].tolist()
    plot_names = np.array(heatmap_x_labels)
    plot_names[none_indices] = None
    logger.debug(indices_to_plot_for_names)
    try:
        sub = sns.clustermap(subset_df,
                             col_cluster=False,
                             row_cluster=row_cluster,
                             col_colors=reordered_colors,
                             yticklabels=True,
                             cmap=global_cmap)
    except Exception:
        # if it were too big to plot successfully, just do a subset
        sample = np.arange((np.shape(subset_df)[0]))
        subset_length = min([np.shape(subset_df)[0], 400])
        np.random.shuffle(sample)
        sub_sample = np.sort(sample[:subset_length])
        sub = sns.clustermap(np.array(subset_df)[sub_sample, :],
                             col_cluster=False,
                             row_cluster=row_cluster,
                             col_colors=reordered_colors,
                             yticklabels=True,
                             cmap=global_cmap)

    # this sets the heatmap labels on the x axis
    sub.ax_heatmap.xaxis.set_ticks(indices_to_plot_for_names)
    sub.ax_heatmap.xaxis.set_ticklabels(plot_names[indices_to_plot_for_names])
    plt.setp(sub.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    plt.setp(sub.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
    # plt.tight_layout()
    # plt.show()
    sub.savefig(out_dir + 'genes_of_interest_subset_heatmap.png',
                dpi=600,
                bbox_inches='tight')
    if only_heatmap:
        return
    # # plot the nodes of interest on the tSNE and PCAs

    for noi in nodes_of_interest:
        logger.info("drawing: {}".format(noi))
        # fix for entrez..
        try:
            float(noi)
        except ValueError:
            noi = noi
        else:
            noi = "{:.0f}".format(noi)
        for plot in cluster_result_dict["plots"]:
            p = draw_plot(plot=cluster_result_dict["plots"][plot],
                          full_expression=full_expression,
                          id_hash=id_hash,
                          colorize_by_gene=noi)
            temp_plot_out = plot.split('/')
            temp_plot_out = temp_plot_out[-1]
            p.savefig(out_dir + temp_plot_out[:-4] + '_' + alias_dict[noi] + '.png',
                      dpi=600,
                      bbox_inches='tight')


if __name__ == "__main__":
    main()
