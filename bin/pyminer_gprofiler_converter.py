import argparse
import random
import logging
from gprofiler import GProfiler

from modules.common_functions import read_file, write_table, save_dict
from modules.references import HSAPIENS

logger = logging.getLogger(__name__)


def convert_to_human(in_genes, gp, species):
    temp_lookup = {}
    gene_symbol_dict = {}
    for g in in_genes:
        temp_lookup[g] = []
        gene_symbol_dict[g] = []

    results = gp.gorth(in_genes, source_organism=species, target_organism=HSAPIENS)

    for result in results[1:]:
        temp_id = result[1]
        temp_hu_symbol = result[5]
        if temp_id not in gene_symbol_dict:
            logger.error('weired mapping event with:', temp_id)
        elif temp_hu_symbol is not None:
            temp_symbol_list = gene_symbol_dict[temp_id]
            temp_symbol_list.append(temp_hu_symbol)
            gene_symbol_dict[temp_id] = temp_symbol_list

    return gene_symbol_dict, results

    # TODO: not finished


def convert_to_ensg(in_genes, gp, species):
    temp_lookup = {}
    gene_symbol_dict = {}
    gene_def_dict = {}
    for g in in_genes:
        temp_lookup[g] = []
        gene_symbol_dict[g] = []
        gene_def_dict[g] = []

    results = gp.gconvert(in_genes, organism=species, target='ENSG', numeric_ns="ENTREZGENE_ACC")

    for result in results[1:]:
        temp_id = result[1].replace("ENTREZGENE_ACC:", "")
        temp_hu_symbol = result[4]
        temp_def = result[5]
        if temp_id not in gene_symbol_dict:
            logger.error('weird mapping event with:', temp_id)
        elif temp_hu_symbol is not None:
            temp_symbol_list = gene_symbol_dict[temp_id]
            temp_symbol_list.append(temp_hu_symbol)
            gene_symbol_dict[temp_id] = temp_symbol_list
            temp_def_dict = gene_def_dict[temp_id]
            temp_def_dict.append(temp_def)

    return gene_symbol_dict, gene_def_dict, results


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-ID_list", '-ids', '-IDs', '-i',
                        help="a file containing gene ids separated on new lines",
                        type=str)

    parser.add_argument("-out", '-outfile', '-o',
                        help="the file that we'll write the results to",
                        type=str)

    parser.add_argument("-species", '-s',
                        help="a gProfiler accepted species code. Dafault = '{}'".format(HSAPIENS),
                        type=str,
                        default=HSAPIENS)

    parser.add_argument("-annotations",
                        help="just get the annotation table",
                        action='store_true',
                        default=False)

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    pyminer_gprofiler_converter(out=args.out,
                                id_list_file=args.ID_list,
                                species=args.species,
                                annotations=args.annotations)


def pyminer_gprofiler_converter(out, id_list_file, species, annotations):
    # if we will need to convert to human IDs
    convert = bool(species != HSAPIENS and not annotations)

    # setup the api
    gp = GProfiler('PyMINEr_' + str(random.randint(0, int(1e6))),
                   want_header=True)
    # convert id_list to a gene lookup table and write it to file
    # I'm also thinking it could be good to pickle some useful objects
    # load in the ID list
    temp_id_list = read_file(id_list_file)
    id_list = []
    for temp_id in temp_id_list:
        try:
            id_list.append("{:.0f}".format(temp_id))
        except ValueError:
            id_list.append(temp_id)
    # convert to ensg
    symbol_dict, def_dict, annotation_table = convert_to_ensg(id_list, gp, species=species)
    write_table(annotation_table, out + ".tsv")

    for i in range(0, 5):
        logger.debug(id_list[i])
        logger.debug(symbol_dict[id_list[i]])
    save_dict((symbol_dict, def_dict), out + ".pkl")

    # write the ortholgoues
    if convert:
        human_symbol_dict, full_orthologue_table = convert_to_human(id_list, gp, species=species)
        save_dict(human_symbol_dict, out + ".pkl")
        write_table(full_orthologue_table, out + ".tsv")


if __name__ == "__main__":
    main()
