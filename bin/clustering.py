from __future__ import division, print_function, absolute_import

import logging
import sys
import argparse
import fileinput
import h5py
import os
import random
import warnings
from matplotlib import use
from random import sample as sample_random
use('Agg')

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

import seaborn as sns
from gprofiler import GProfiler
from numpy import ma
from scipy import linalg
from scipy.cluster.vq import kmeans2
from scipy.stats import gaussian_kde, mstats_basic, rankdata, norm
from sklearn import metrics
from sklearn.cluster import AffinityPropagation as ap
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from modules.common_functions import read_file, make_file, strip_split, get_file_path, read_table, \
    write_table, save_dict, run_cmd
from modules.references import HSAPIENS

from bin.hd5_afinity_propagation import Hdf5AffinityPropagation
from bin.mat_to_adj_list import mat_to_adj_list

from modules.mito_ribo_go import GO_MITOCHONDRIAL, GO_RIBOSOME

sys.setrecursionlimit(10000)

try:
    import umap
except ModuleNotFoundError:
    HAS_UMAP = False
else:
    HAS_UMAP = True

logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument('-infile', '-in', '-i', '-input', required=True, dest='infile', type=str)

    parser.add_argument('-out', '-o', '-out_dir', dest='out_dir', type=str, required=True)

    parser.add_argument(
        "-sample_k_known",
        help='If you know how many groups there should be, give an int with this argument',
        dest='sample_k_known',
        default=0)

    parser.add_argument(
        "-sample_cluster_iter",
        help='the number of iterations for clustering. Default = 10, but going higher will '
             'cost more time and yeild better results.',
        dest='sample_cluster_iter',
        type=int,
        default=10)

    parser.add_argument(
        "-rand_seed", '-seed',
        help='the random number input for random number seed, default = 12345',
        dest='rand_seed',
        type=int,
        default=12345)

    parser.add_argument(
        "-rows",
        help='if you want to cluster the rows instead of columns (default is to cluster columns).',
        dest='do_rows',
        action='store_true',
        default=False)

    parser.add_argument(
        "-hdf5", '-do_hdf5',
        help='if we are dealing with an hdf5 file format. This also requires the -columns argument',
        dest='do_hdf5',
        action='store_true',
        default=False)

    parser.add_argument(
        '-columns', '-c', '-col', '-cols', '-column_ids', '-column_IDs',
        dest='columns',
        type=str)

    parser.add_argument(
        '-ID_list', '-ids', '-IDs', '-ID',
        type=str)

    parser.add_argument(
        "-var_norm",
        help='if you want to normalize the rows prior to clustering',
        dest='var_norm',
        action='store_true',
        default=False)

    parser.add_argument(
        "-log", '-log2', '-log_transform',
        help='do a log transformation prior to clustering',
        action='store_true',
        default=False)

    parser.add_argument(
        "-rank",
        help='do a rank transformation on the features prior to clustering',
        action='store_true',
        default=False)

    parser.add_argument(
        "-no_var_norm",
        help='do not normalize the variables for sample clustering',
        dest='var_norm',
        action='store_false',
        default=True)

    parser.add_argument(
        "-ap_clust", '-ap',
        help='do affinity propagation clustering',
        action='store_true',
        default=False)

    parser.add_argument(
        '-clust_on_genes',
        help="cluster on the genes listed in the input text file with this argument.",
        type=str)

    parser.add_argument(
        '-neg_cor_clust',
        help="if you want to do clustering based off of only negatively correlated variables, feed in "
             "the table which summarizes the positive and negative relationships",
        type=str)

    parser.add_argument(
        '-neg_cor_count_clust',
        help="if you want to do clustering based off of the number of significantly negative correlations, "
             "as determined by bootstrap shuffled negative control",
        type=str)

    parser.add_argument(
        "-no_spearman_clust",
        help='will not perform clustering on the sample-wise spearman correlation matrix',
        dest='spearman_dist',
        action='store_false',
        default=True)

    parser.add_argument(
        "-neg_cor_cutoff",
        help='the cutoff for number of relationships to include for a negative correlation based '
             'clustering run (default = 15)',
        type=int,
        default=15)

    parser.add_argument(
        "-first_neg_neighbor", '-neg_neighbor',
        help='For negative correlation clusters only. Get the first neighbors (co-regulated) of the '
             'negative correlated genes',
        action='store_true',
        default=False)

    parser.add_argument(
        "-leave_mito_ribo",
        help="don't remove the mitochondrial and ribosomal genes for clustering.",
        action="store_true",
        default=False)

    parser.add_argument(
        "-sc_clust",
        help='set parameters for single cell RNAseq cell-type identification',
        action='store_true',
        default=False)

    parser.add_argument(
        '-pos_adj_list',
        help="for smoothing out the negative correlation clustering, add the first neighbors of these genes",
        type=str)

    parser.add_argument(
        '-merge',
        help="for mergering groups based on similarity. Particularly useful for reconstructing lineage trees.",
        dest='do_merger',
        action='store_true',
        default=False)

    parser.add_argument(
        '-ap_merge',
        help="This changes the ap clustering to an agglomerative ap clustering which merges clusters of "
             "sufficient similarity. Note that this *can* result in non-spaerical clusters, but "
             "might not necessarily.",
        dest='ap_merge',
        action='store_true',
        default=False)

    parser.add_argument(
        '-point_size', '-pt_size', '-ps',
        help='For plotting the points, how big should they be? (default = 6)',
        type=float,
        default=6)

    parser.add_argument(
        "-perplexity",
        help='Perplexity value to use for tSNE',
        default=None)

    parser.add_argument(
        "-tsne_iter",
        help='iterations for tSNE. Default = 1e6',
        type=str,
        default='1e6')

    parser.add_argument(
        "-dpi",
        help='dots per inch',
        default=600,
        type=int)

    parser.add_argument(
        "-block_size",
        help='how many variables will be used at once for correlation analyzis; this helps keep '
             'memory requirements down and prevents a segmentation fault memory error',
        type=int,
        default=5000)

    parser.add_argument(
        "-manual_sample_groups",
        help='if you already know the sample groups, but just want some plots',
        type=str)

    parser.add_argument("-species", '-s',
                        help="a gProfiler accepted species code. Dafault = '{}'".format(HSAPIENS),
                        type=str,
                        default=HSAPIENS)

    parser.add_argument("-rho_cutoff", '-rho',
                        dest="rho_cutoff",
                        help="Suggested to leave this blank, but you can provide your own cutoff "
                             "instead of allowing PyMINEr to set it for you. This is the absolute "
                             "value of the spearman rho to use for a cutoff. If using scRNAseq, "
                             "try something in the range of 0.25-0.35. For bulk RNAseq, try something "
                             "closer to 0.70 - 0.90.",
                        type=float)

    args = parser.parse_args()

    return args


def _chk_asarray(a, axis):
    if axis is None:
        a = np.ravel(a)
        outaxis = 0
    else:
        a = np.asarray(a)
        outaxis = axis
    if a.ndim == 0:
        a = np.atleast_1d(a)
    return a, outaxis


def _chk2_asarray(a, b, axis):
    if axis is None:
        a = np.ravel(a)
        b = np.ravel(b)
        outaxis = 0
    else:
        a = np.asarray(a)
        b = np.asarray(b)
        outaxis = axis
    if a.ndim == 0:
        a = np.atleast_1d(a)
    if b.ndim == 0:
        b = np.atleast_1d(b)
    return a, b, outaxis


def _contains_nan(a, nan_policy='propagate'):
    policies = ['propagate', 'raise', 'omit']
    if nan_policy not in policies:
        raise ValueError("nan_policy must be one of {%s}" %
                         ', '.join("'%s'" % s for s in policies))
    try:
        # Calling np.sum to avoid creating a huge array into memory
        # e.g. np.isnan(a).any()
        with np.errstate(invalid='ignore'):
            contains_nan = np.isnan(np.sum(a))
    except TypeError:
        # If the check cannot be properly performed we fallback to omiting
        # nan values and raising a warning. This can happen when attempting to
        # sum things that are not numbers (e.g. as in the function `mode`).
        contains_nan = False
        nan_policy = 'omit'
        warnings.warn("The input array could not be properly checked for nan "
                      "values. nan values will be ignored.", RuntimeWarning)
    if contains_nan and nan_policy == 'raise':
        raise ValueError("The input contains nan values")
    return contains_nan, nan_policy


def no_p_spear(a, b=None, axis=0, nan_policy='propagate'):
    a, axisout = _chk_asarray(a, axis)

    contains_nan, nan_policy = _contains_nan(a, nan_policy)

    if contains_nan and nan_policy == 'omit':
        a = ma.masked_invalid(a)
        b = ma.masked_invalid(b)
        return mstats_basic.spearmanr(a, b, axis=axis)
    if a.size <= 1:
        raise Exception("Only one value?  Something is wrong.")
    ar = np.apply_along_axis(rankdata, axisout, a)

    br = None
    if b is not None:
        b, axisout = _chk_asarray(b, axis)

        contains_nan, nan_policy = _contains_nan(b, nan_policy)

        if contains_nan and nan_policy == 'omit':
            b = ma.masked_invalid(b)
            return mstats_basic.spearmanr(a, b, axis=axis)

        br = np.apply_along_axis(rankdata, axisout, b)

    rs = np.corrcoef(ar, br, rowvar=axisout)

    if rs.shape == (2, 2):
        return rs[1, 0]
    else:
        return rs


def lin_norm_rows(in_mat):
    in_mat = np.transpose(np.array(in_mat))
    in_mat = in_mat - np.min(in_mat, axis=0)
    in_mat = in_mat / np.max(in_mat, axis=0)
    in_mat[np.isnan(in_mat)] = 0
    return np.transpose(in_mat)


def lin_norm_rows_hdf5(full_expression):
    mins = np.transpose(np.array([np.min(full_expression, axis=1)]))
    full_expression -= mins
    maxs = np.transpose(np.array([np.max(full_expression, axis=1)]))
    full_expression /= maxs
    return full_expression


# if we are doing a negative correlation based run, subset the input matrix
def get_density(in_vect):
    density = gaussian_kde(in_vect)
    xs = np.arange(0, max(in_vect), .1)
    density.covariance_factor = lambda: 2.5
    density._compute_covariance()
    return xs, density(xs)


def plot_densities(neg_cor_density_x, neg_cor_density_y, pos_cor_density_x, pos_cor_density_y, out_file, dpi):
    plt.plot(neg_cor_density_x, neg_cor_density_y,
             label='negative correlation degree density',
             color='blue',
             linewidth=3)
    plt.plot(pos_cor_density_x, pos_cor_density_y,
             label='positive correlation degree density',
             color='red',
             linewidth=3)
    plt.legend()
    plt.savefig(out_file,
                dpi=dpi,
                bbox_inches='tight')


def remove_zeros(in_vect):
    out_vect = []
    for i, vect in enumerate(in_vect.tolist()):
        if vect != 0:
            out_vect.append(vect)
    return out_vect


def get_first_neighbors(passing_ids, pos_adj_list):
    first_neighbors = []
    for line in fileinput.input(pos_adj_list):
        temp_line = strip_split(line)
        if temp_line[0] in passing_ids or temp_line[1] in passing_ids:
            if temp_line[0] not in first_neighbors:
                first_neighbors.append(temp_line[0])
            if temp_line[1] not in first_neighbors:
                first_neighbors.append(temp_line[1])
    fileinput.close()
    return first_neighbors


def process_dict(in_file, ensg_idx):
    out_dict = {}
    for i, single in enumerate(in_file):
        out_dict[single[ensg_idx]] = True
    return out_dict


def get_known_centers(sample_k_lists, full_expression):
    # get the point that is closest to the median for each group
    best_centers = []
    for i, temp_idxs in enumerate(sample_k_lists):
        temp_medoid = np.median(full_expression[:, temp_idxs], axis=0)
        temp_dist = np.sqrt(np.sum((full_expression[:, temp_idxs] - temp_medoid) ** 2, axis=0))
        if len(temp_idxs) != np.shape(temp_dist)[0]:
            logger.warning('wrong dimension')
        best_idx = np.argmin(temp_dist)
        logger.info("best idx: {}".format(best_idx))
        best_idx = temp_idxs[best_idx]
        best_centers.append(best_idx)
    return best_centers


def get_big_spearman(block_size, temp_dir, usable_indices, infile, columns, id_list, rho_cutoff):
    if usable_indices:
        make_file('\n'.join(str(i) for i in usable_indices), temp_dir + '/used_indices.txt')

    mat_to_adj_list(infile_path=infile,
                    time_it=True,
                    transpose=True,
                    hdf5=True,
                    hdf5_out=True,
                    col_ids=columns,
                    block_size=str(int(block_size)),
                    rho_dict_dir=temp_dir + '/sample_clustering_and_summary/rho_dicts/',
                    row_subset=temp_dir + '/used_indices.txt' if usable_indices else None,
                    euclidean_dist=True,
                    adj_list_out=temp_dir + '/adj_list_rho_spearman.tsv',
                    id_list=id_list,
                    rho_cutoff=rho_cutoff)


# functions for performing k-means clustering
def linear_normalization(in_matrix, axis=1):
    in_matrix = in_matrix - np.transpose(np.array([np.min(in_matrix, axis=axis)]))
    return in_matrix / np.transpose(np.array([np.max(in_matrix, axis=axis)]))


def convert_to_prob(vect):
    # calculate probabilities based on standard deviation
    probabilities = vect - min(vect)
    probabilities = probabilities / max(probabilities)
    return probabilities / sum(probabilities)


def get_all_distances_from_a_centroid(centroid, in_mat):
    euclidean_distances = []
    for i in range(0, in_mat.shape[0]):
        # get the euclidean distance of each variable from given centroid
        temp_dist = linalg.norm(in_mat[i] - centroid)
        euclidean_distances.append(temp_dist ** 2)
    #        euclidean_distances.append(temp_dist)
    return np.transpose(np.array([np.array(euclidean_distances)]))


def get_next_centroid_index(distance_list, multiply_by_min, existing_centroids=None, prob=False):
    # this function calculates the sum of the squares of the euclidean distances from
    # the all of the current centroids for all variables, then returns the index of the
    # variable with the maximum sum of the square euclidean distances from all centroids

    if not existing_centroids:
        existing_centroids = []

    temp_dist_mat = np.array(distance_list)
    row_sum = np.sum(temp_dist_mat, axis=1)
    if multiply_by_min:
        row_min = np.amin(temp_dist_mat, axis=1)
        all_distances = row_sum * row_min
    else:
        all_distances = row_sum

    # add all subsequent squared distances
    if prob:
        # calculate probabilities based on standard deviation
        probabilities = convert_to_prob(all_distances)
        max_indices = np.random.choice(list(range(0, np.shape(distance_list)[0])), size=1, replace=False,
                                       p=probabilities)
        num_max_indices = 1
    else:
        max_indices = np.where(all_distances == np.max(all_distances))[0]
        num_max_indices = np.shape(max_indices)[0]
        logger.info('number equal to max {}'.format(num_max_indices))

    if num_max_indices > 1:
        max_indices = list(max_indices)
        final_candidate_next_centroids = []
        for i in range(0, num_max_indices):
            if max_indices[i] not in existing_centroids:
                final_candidate_next_centroids.append(max_indices[i])
        if not final_candidate_next_centroids:
            return sample_random(max_indices, 1)[0]
        else:
            return sample_random(final_candidate_next_centroids, 1)[0]

    else:
        return sample_random(list(max_indices), 1)[0]


def get_distance_matrix(in_array):
    # this function returns the symetric matrix of euclidean distance between
    # all genes based on the principal components that are passed into this
    # function

    # initialize the distance matrix with zeros
    return metrics.pairwise.euclidean_distances(in_array, in_array)


def global_impact_of_k_clusters(centroids, members, interaction_matrix):
    """centroids is a list of the centroids
    members is a list of the indices of each member belonging to the ith centroid
    the interaction matrix is given for subsetting the members out
    print('centroids')"""

    if len(members) != centroids.shape[0]:
        logger.error('incorrect dimentions\tmembers: {}, \tcentroids: {}'.format(len(members), centroids.shape[0]))
        raise Exception('incorrect dimentions')
    sk = 0
    sk_vector = []
    for i, member in enumerate(members):
        # dummy is set because this function returns the erroneous IDs for the subset matrix
        # this function does this because it thinks it's getting the full interaction matrix,
        # even though in reality it is getting a subsetted matrix
        # dummy, member_subset = subset_interaction_matrix(members[i], interaction_matrix)
        member_subset = interaction_matrix[member, :]

        # trim the centroid to only include the indices of the subset matrix

        # temp_centroid = centroids[i,members[i]]
        temp_centroid = centroids[i, ]

        distance_vector = get_all_distances_from_a_centroid(temp_centroid, member_subset)
        temp_sk = np.sum(distance_vector)

        sk_vector.append(temp_sk)
        sk += temp_sk

    return sk


def f_of_k(prior_sk, prior_ak, centroids, members, interaction_matrix):
    """* note that if k == 1, or k == 2, the 'prior_Ak' argument is not actually used
    so any dumby value can be fed into the function without ill effect"""

    logger.info('prior_Sk {}'.format(prior_sk))
    logger.info('prior_Ak {}'.format(prior_ak))
    k = len(members)
    n_d = interaction_matrix.shape[0]
    cur_ak = None

    # this function should only be run on 'clusters' with at least two possible members
    if n_d == 1:
        return None, None, None
    else:  # calculate Sk
        cur_sk = global_impact_of_k_clusters(centroids, members, interaction_matrix)

    # calculate the current Ak
    if k == 2 and n_d > 1:
        cur_ak = 1 - (3 / (4 * n_d))
    elif k > 2 and n_d > 1:
        cur_ak = prior_ak + ((1 - prior_ak) / 6)

    # calculate the k evaluation function
    if k == 1 or (prior_sk == 0 and k > 1):
        f = 1
    else:
        f = cur_sk / (cur_ak * prior_sk)

    return f, cur_sk, cur_ak


def k_stopping_function(f_list, sample_k_known, num_vars=None, starting_length=None,
                        stopping_length=5):
    # returning True allows the k search to continue, while returning False makes it stop
    if starting_length is None:
        # this is a guess for the max number of groups to test at the beginning, barring any run-offs of local minimas
        starting_length = int(min([np.sqrt(num_vars) + 1, 50]))

    if sample_k_known:
        if len(f_list) < sample_k_known:
            return True
        if len(f_list) == sample_k_known:
            return False
    else:
        if len(f_list) == 1:
            return True
        if f_list[-1] - f_list[-2] > 10:
            return False
        if len(f_list) < starting_length + 1:
            return True
        if min(f_list) in f_list[-stopping_length:]:
            return True


def get_estimated_k_from_f_list(f_list, sample_k_known):
    if sample_k_known:
        return sample_k_known - 1
    else:
        f_deltas = []
        f_delta_delta = []
        for f in range(1, len(f_list)):
            f_deltas.append(f_list[f] - f_list[f - 1])
            f_delta_delta.append(f_list[f] - f_deltas[-1])

        min_f = min(f_list)
        return f_list.index(min_f)


def rearrange_variable_labels(variable_labels):
    """ this function will take variable labels of format [0,1,1,0,0,2]
    and change it to:
    [[0,3,4], # centroid 0
       [1,2],   # centroid 1
        [5]]     # centroid 2"""
    max_label = max(variable_labels)
    variable_labels = np.array(variable_labels)
    new_var_labels = []
    for i in range(0, max_label + 1):  # plus1 because the range function is exclusive in python, not inclusive
        new_var_labels.append(list(np.where(variable_labels == i)[0]))
    return new_var_labels


def do_unknown_k_means_clustering_iter(sample_ids, full_expression,
                                       sample_k_known, cluster_prob, first_prob,
                                       out_dir, multiply_by_min,
                                       cluster_iter=10, performed_on_samples=False):
    logger.info("sample_k_known {}".format(sample_k_known))
    sample_k_table_list_list = []
    sample_k_lists_list = []
    f_list_list = []
    optimal_centroid_indices_list = []
    min_f = []
    k_estimate_list = []

    # transposed dataset to feed into k-means
    full_expression_t = np.array(np.transpose(full_expression), dtype=np.float32)

    logger.info("cluster iterations planned: {}".format(cluster_iter))
    for i in range(0, cluster_iter):
        logger.info("cluster iteration: {}".format(i))

        temp_sample_k_table, temp_sample_k_lists, temp_f_list, temp_optimal_centroid_indices = \
            unknown_k_means_sample(sample_names=sample_ids,
                                   expression_matrix=full_expression_t,
                                   sample_k_known=sample_k_known,
                                   first_prob=first_prob,
                                   multiply_by_min=multiply_by_min,
                                   prob=False if i == 0 else cluster_prob)

        sample_k_table_list_list.append(temp_sample_k_table)
        sample_k_lists_list.append(temp_sample_k_lists)
        f_list_list.append(temp_f_list)
        if sample_k_known:
            min_f.append(temp_f_list[sample_k_known - 1])
        else:
            min_f.append(min(temp_f_list))
        k_estimate_list.append(temp_f_list.index(min_f[-1]) + 1)

        optimal_centroid_indices_list.append(temp_optimal_centroid_indices)

    if performed_on_samples:
        if sample_k_known:
            write_table(f_list_list, out_dir + '/f_lists_k_known.txt')
        else:
            write_table(f_list_list, out_dir + '/f_lists_k_not_known.txt')

    if not sample_k_known:
        # temp_k_list = k_estimate_list[:]
        # temp_k_list = sorted(temp_k_list)
        return do_unknown_k_means_clustering_iter(sample_ids=sample_ids,
                                                  full_expression=full_expression,
                                                  sample_k_known=sample_k_known,
                                                  cluster_prob=cluster_prob,
                                                  first_prob=first_prob,
                                                  out_dir=out_dir,
                                                  multiply_by_min=multiply_by_min,
                                                  cluster_iter=cluster_iter,
                                                  performed_on_samples=performed_on_samples)

    logger.info('k estimate list {}'.format(k_estimate_list))
    for f in range(0, len(f_list_list)):
        logger.info('iter {} k estimate {} f {} {}'.format(f, k_estimate_list[f], min_f[f], f_list_list[f]))

    global_min_f = min(min_f)
    logger.info('global_min_f {}'.format(global_min_f))
    global_min_f_indices = min_f.index(global_min_f)
    logger.info('global_min_f_indices {}'.format(global_min_f_indices))
    if isinstance(global_min_f_indices, int):
        optimal_k_iter = global_min_f_indices
    else:
        optimal_k_iter = sample_random(list(global_min_f_indices), 1)[0]
    f_list = f_list_list[optimal_k_iter]
    optimal_centroid_indices = optimal_centroid_indices_list[optimal_k_iter]

    logger.debug(optimal_centroid_indices)

    sample_k_table_list = sample_k_table_list_list[optimal_k_iter]
    sample_k_lists = sample_k_lists_list[optimal_k_iter]

    return sample_k_table_list, sample_k_lists, f_list, optimal_centroid_indices


def group_list_to_sample_k_table(labels, sample_names):
    num_clust = len(list(set(labels)))
    logger.debug(labels)
    sample_k_table = []
    for i, sample_name in enumerate(sample_names):
        sample_k_table.append([sample_name, labels[i]])
    sample_k_lists = [[] for _ in range(num_clust)]
    for i, sample_name in enumerate(sample_names):
        sample_k_lists[labels[i]].append(sample_name)
    return sample_k_table, sample_k_lists


def unknown_k_means_sample(sample_names, expression_matrix,
                           sample_k_known, first_prob, multiply_by_min,
                           prob=False):
    # expression matrix format:
    #        sample1, sample2, sample3 ...
    # var1[[  1.0   ,   1.5  ,   2.0  ],
    # var2 [  0.2   ,   1.5  ,   2.0  ],
    # var3 [  1.0   ,   1.5  ,   2.0  ]]

    # because the kmeans2 function from scipy clusters based on rows,
    # the expression matrix will be transposed

    # samples are now in rows, and expression values are now in columns

    # go_past_for_local_min is the variable which is used for seeing how far in
    # k selection we should go past a local minimum

    # expression_matrix = np.transpose(expression_matrix)
    # expression_matrix = expression_matrix.astype('float32')

    # this variable is for testing passed a local minimum of the f(k) function
    stopping_length = 10

    if expression_matrix.shape[0] <= stopping_length:
        logger.warning('expression_matrix is too small to subdivide')

        output_cluster_annotations = list(zip(sample_names, [0] * len(sample_names)))
        for i in range(0, len(output_cluster_annotations)):
            output_cluster_annotations[i] = list(output_cluster_annotations[i])
        # output_cluster_annotations, out_var_group_list_of_lists, f_list, optimal_centroid_indices
        return output_cluster_annotations, [sample_names], [1], [0]

    # find the variable with the greatest number of interactions (or total expression)
    row_std = np.std(expression_matrix, axis=1)
    logger.debug(expression_matrix)
    logger.debug(row_std)
    max_row_std = np.max(row_std)
    if prob or first_prob:
        probabilities = convert_to_prob(row_std)
        logger.debug(row_std)
        logger.debug(np.sum(row_std))
        logger.debug(len(sample_names))
        logger.debug(len(probabilities))
        centroid_initialization_index = np.random.choice(list(range(0, len(sample_names))), size=1, replace=False,
                                                         p=probabilities)
    else:
        centroid_initialization_index = sample_random(list(np.where(row_std == max_row_std)[0]), 1)
    logger.info('first centroid index: {}'.format(centroid_initialization_index))
    centroid_indices = list(centroid_initialization_index[:])

    temp_centroids, variable_labels = kmeans2(expression_matrix, expression_matrix[centroid_indices, :], minit='matrix',
                                              iter=10)

    logger.info('centroid_indices for k = {}'.format(len(centroid_indices)))
    logger.debug(centroid_indices)

    # initialize with k = 1
    sk = None
    ak = None
    temp_f, sk, ak = f_of_k(sk, ak, temp_centroids, [list(variable_labels)], expression_matrix)
    f_list = [temp_f]
    centroid_distances_matrix = np.array(
        get_all_distances_from_a_centroid(expression_matrix[centroid_indices[-1]], expression_matrix))

    logger.info('k = {}\tf(k) = {}\tSk = {}\tAk = {}'.format(len(f_list), temp_f, sk, ak))

    while k_stopping_function(f_list, sample_k_known, num_vars=len(sample_names)):
        # len(f_list) must be at least the length of the stopping_length +1 for k=1
        # then the first time that min(f(k)) is not within the stopping length,
        # then stop and return the optimal k

        # first thing to do is find out all the current distances from all centroids
        if len(centroid_indices) > 1:
            centroid_distances_matrix = np.hstack((centroid_distances_matrix, np.array(
                get_all_distances_from_a_centroid(expression_matrix[centroid_indices[-1]], expression_matrix))))

        # find the index of the next centroid based on the current distance matrix
        if prob:
            next_centroid = get_next_centroid_index(centroid_distances_matrix,
                                                    multiply_by_min, existing_centroids=centroid_indices,
                                                    prob=True)
        else:
            next_centroid = get_next_centroid_index(centroid_distances_matrix, multiply_by_min,
                                                    existing_centroids=centroid_indices)
        if next_centroid in centroid_indices:
            # this means that the newly of the remaining points, there are now ties
            # with already existing centroids for farthest away from other centroids
            logger.warn('already established centroid was picked again')
            break

        centroid_indices.append(next_centroid)
        logger.info('centroid_indices for k = {}'.format(len(centroid_indices)))

        temp_centroids, variable_labels = kmeans2(expression_matrix, expression_matrix[centroid_indices, :],
                                                  minit='matrix', iter=10)

        # this function will take variable labels of format [0,1,1,0,0,2]
        # and change it to:
        # [[0,3,4], # centroid 0
        #    [1,2],   # centroid 1
        #     [5]]     # centroid 2
        variable_labels = rearrange_variable_labels(variable_labels)
        if len(variable_labels) < len(centroid_indices):
            # this means that two centroids have converged during the k-means clustering, indicating
            # that the number of centroids is already oversaturated
            logger.warn("centroids converged, stopping due to overfit")
            break

        temp_f, sk, ak = f_of_k(sk, ak, temp_centroids, variable_labels, expression_matrix)
        f_list.append(temp_f)
        logger.info('k = {}\tf(k) = {}\tSk = {}\tAk = {}'.format(len(f_list), temp_f, sk, ak))

    logger.debug(k_stopping_function(f_list, sample_k_known, num_vars=len(sample_names)))
    if not k_stopping_function(f_list, sample_k_known, num_vars=len(sample_names)):
        logger.debug(f_list)
    optimal_k_index = None
    if not sample_k_known:
        optimal_k_index = get_estimated_k_from_f_list(f_list, sample_k_known)
        logger.info('optimal k = {}'.format(optimal_k_index + 1))
    elif sample_k_known:
        logger.info('sample k known')
        optimal_k_index = int(sample_k_known - 1)
    logger.info('optimal k index: {}'.format(optimal_k_index))
    optimal_centroid_indices = centroid_indices[:optimal_k_index + 1]

    final_centroids, final_variable_labels = kmeans2(expression_matrix, expression_matrix[optimal_centroid_indices, :],
                                                     minit='matrix', iter=10)

    # convert the final variable labels into a list of lists containing the variable names
    rearranged_var_labels = rearrange_variable_labels(final_variable_labels)
    out_var_group_list_of_lists = []
    for r in rearranged_var_labels:
        out_var_group_list_of_lists.append(list(sample_names[var] for var in r))

    output_cluster_annotations = [list(i) for i in zip(sample_names, final_variable_labels)]

    if sample_k_known:
        logger.debug([str(i) for i in optimal_centroid_indices])

    return output_cluster_annotations, out_var_group_list_of_lists, f_list, optimal_centroid_indices


def do_big_ap(neg_euc_hdf5_file, block_size, out_dir, usable_indices, infile, columns, id_list, rho_cutoff):
    if not os.path.isfile(neg_euc_hdf5_file):
        get_big_spearman(block_size, out_dir, usable_indices, infile, columns, id_list, rho_cutoff)

    run_cmd(['cp', neg_euc_hdf5_file, neg_euc_hdf5_file + '_copy'])
    neg_euc_hdf5_file = neg_euc_hdf5_file + '_copy'
    f_ap = h5py.File(neg_euc_hdf5_file, "w")
    neg_euc_dist = f_ap["infile"]
    logger.debug('top of neg euc dist mat')
    logger.debug(neg_euc_dist[:5, :5])

    logger.info('making the hdf5 file for ap clustering')
    temp_ap_hdf5 = os.path.splitext(out_dir)[0] + '/temp_ap_clust.hdf5'
    logger.debug('\t {}'.format(temp_ap_hdf5))

    # af = AffinityPropagation(preference = None, affinity = "precomputed", copy = False).fit(neg_euc_dist)
    # note that the default preference in this function is min, axis=1
    af = Hdf5AffinityPropagation(preference=None, affinity="precomputed", copy=False, temp_ap_hdf5=f_ap,
                                 block_size=block_size).fit(neg_euc_dist)
    return af


def do_ap_clust(sample_ids, clust_array, neg_euc_hdf5_file, mem_err, spearman_dist, block_size, out_dir,
                usable_indices, infile, columns, id_list, rho_cutoff,
                pref_multiplier=1):
    clust_array = np.transpose(clust_array)
    logger.debug('cluster array shape: {}'.format(np.shape(clust_array)))
    logger.debug('number of samples: {}'.format(len(sample_ids)))

    logger.info('performing AP clustering')
    if mem_err:
        af = do_big_ap(neg_euc_hdf5_file=neg_euc_hdf5_file,
                       block_size=block_size,
                       out_dir=out_dir,
                       usable_indices=usable_indices,
                       infile=infile,
                       columns=columns,
                       id_list=id_list,
                       rho_cutoff=rho_cutoff)
    else:
        if not spearman_dist:
            # calculate the affinity matrix
            try:
                am = -metrics.pairwise.euclidean_distances(clust_array, squared=True) / np.log2(
                    np.shape(clust_array)[1])
            except MemoryError:
                logger.warning('hit a memory error, going to try it again with an '
                               'HDF5 format affinity matrix intermediate')
                af = do_big_ap(neg_euc_hdf5_file, block_size, out_dir,
                               usable_indices, infile, columns, id_list, rho_cutoff=rho_cutoff)
            else:  # if the affinity matrix calcualtion was successful
                try:
                    af = ap(preference=np.min(am, axis=1), affinity="precomputted").fit(am)
                except MemoryError:
                    logger.warning('hit a memory error, going to try it again with '
                                   'an HDF5 format affinity matrix intermediate')
                    af = do_big_ap(neg_euc_hdf5_file=neg_euc_hdf5_file,
                                   block_size=block_size,
                                   out_dir=out_dir,
                                   usable_indices=usable_indices,
                                   infile=infile,
                                   columns=columns,
                                   id_list=id_list,
                                   rho_cutoff=rho_cutoff)
        else:
            pref = np.min(clust_array, axis=1) * pref_multiplier
            logger.debug(np.shape(clust_array))
            logger.debug("preference:", pref)
            try:
                logger.info("Running AffinityPropagation...")
                af = ap(preference=pref, affinity="precomputed").fit(clust_array)
                logger.info("AffiinityPropagation completed.")
            except MemoryError:
                logger.warn('hit a memory error, going to try it again with an HDF5 format affinity matrix intermediate')
                af = do_big_ap(
                    neg_euc_hdf5_file=neg_euc_hdf5_file,
                    block_size=block_size,
                    out_dir=out_dir,
                    usable_indices=usable_indices,
                    infile=infile,
                    columns=columns,
                    id_list=id_list,
                    rho_cutoff=rho_cutoff)
    logger.debug("ap auto pref: {}".format(af.preference))
    optimal_centroid_indices = af.cluster_centers_indices_
    if optimal_centroid_indices is None:
        optimal_centroid_indices = [0]
        labels = [0] * len(sample_ids)
    else:
        labels = af.labels_

    num_clust = len(optimal_centroid_indices)

    f_list = None
    sample_k_table = []
    for i in range(0, len(sample_ids)):
        sample_k_table.append([sample_ids[i], labels[i]])
    sample_k_lists = [[] for _ in range(num_clust)]
    for i in range(0, len(sample_ids)):
        sample_k_lists[labels[i]].append(sample_ids[i])
    return sample_k_table, sample_k_lists, f_list, optimal_centroid_indices, af


def zero_to_na(in_mat):
    in_mat[in_mat == 0] = np.nan
    return in_mat


def sample_k_lists_to_cluster_indices(sample_k_lists, sample_id_hash):
    cluster_indices = []
    for i in range(0, len(sample_k_lists)):
        cluster_indices.append([sample_id_hash[k] for k in sample_k_lists[i]])
    return cluster_indices


def calc_dist_sd(cluster1, in_mat):
    if len(cluster1) == 0:
        return 0
    # first find the average minimum distance within each cluster
    # Euclidean distance
    clust1_euc_dist = get_distance_matrix(in_mat[:, cluster1])
    clust1_euc_dist += np.transpose(clust1_euc_dist)
    clust1_euc_dist = zero_to_na(clust1_euc_dist)
    # return the standard deviation of those distances
    clust1_dist_sd = np.nanstd(clust1_euc_dist)
    return clust1_dist_sd


def get_sample_means(sample_k_lists, cluster_indices, full_expression):
    sample_means = np.zeros((np.shape(full_expression)[0], len(sample_k_lists)))
    for i, clust_idx in enumerate(cluster_indices):
        temp_samples = clust_idx
        sample_means[:, i] = np.mean(full_expression[:, temp_samples], axis=1)
    return sample_means


def get_exemplar_distances_mat(exemplar_indices, full_expression):
    return metrics.pairwise.euclidean_distances(np.transpose(full_expression[:, exemplar_indices]))


def ids_to_idxs(in_ids, sample_id_hash):
    return [sample_id_hash[i] for i in in_ids]


def do_cluster_merger(sample_k_lists, optimal_centroid_indices,
                      out_plot_dict, full_expression, out_dir, sample_id_hash, dpi, global_cmap,
                      name_leader='', colors=None):
    # first calculate the within cluster spread
    do_sd_spread = True

    if do_sd_spread:
        logger.info('finding the spread within each cluster')
        within_clust_dist_sd = []
        for i, k_list in enumerate(sample_k_lists):
            logger.info('\tworking on cluster {}'.format(i))

            # get the indices for this group
            cluster1 = [sample_id_hash[k] for k in k_list]
            # sd of Euclidean distances of all points in cluster from each other
            temp_sd = calc_dist_sd(cluster1, full_expression)
            within_clust_dist_sd.append(temp_sd)

        # get the Euclidean distance of all exemplars from each other
        exemplar_distance_mat = get_exemplar_distances_mat(optimal_centroid_indices, full_expression)

        # make the matrix showing the SD1 + SD2 standard deviation for each cluster pair
        sum_sds = np.zeros((len(sample_k_lists),
                            len(sample_k_lists)))  # matrix for each cluster by each cluster adding their internal SDs
        for i in range(0, len(within_clust_dist_sd)):
            for j in range(i, len(within_clust_dist_sd)):
                sum_sds[i, j] = within_clust_dist_sd[i] + within_clust_dist_sd[j]
        sum_sds += np.transpose(sum_sds)

        # calculate the Z-score distance of clusters from each other
        cluster_z_distance_mat = exemplar_distance_mat / sum_sds
        # calculate the probabilty of clusters merging based on this
        # use 2-tailed Z probabilities
        transition_probability_matrix = norm.sf(abs(cluster_z_distance_mat))
        # cell fraction transitioning at current state
        reorder_cluster_map = None

        if isinstance(colors, np.ndarray):
            # plot a heatmap of the transition probability matrix
            reorder_cluster_map = sns.clustermap(transition_probability_matrix,
                                                 col_colors=colors,
                                                 row_colors=colors,
                                                 cmap=global_cmap)
            reorder_cluster_map.savefig(out_dir + '/sample_clustering_and_summary/' +
                                        name_leader + 'cell_type_transition_probabilities.png',
                                        dpi=dpi,
                                        bbox_inches='tight')

            # put the transition probability into the output dictionary
            out_plot_dict["transition_probability"] = reorder_cluster_map

        # write transition probabilities
        transition_probability_matrix_copy = transition_probability_matrix[:]
        transition_probability_matrix_copy = transition_probability_matrix_copy.tolist()
        col_titles = ["cell_types"]
        for i in range(0, len(transition_probability_matrix_copy)):
            temp_cell_type = "sample_group_" + str(i)
            transition_probability_matrix_copy[i] = [temp_cell_type] + transition_probability_matrix_copy[i]
            col_titles.append(temp_cell_type)
        write_table(transition_probability_matrix, out_dir + "/" + name_leader +
                    "cell_type_transition_probabilities.txt")

        return transition_probability_matrix, reorder_cluster_map


def should_clusters_be_merged(transition_probability_matrix, prob_cutoff=0.25):
    # takes in trantransition_probability_matrix
    # returns a graph network based on  the transition probability cutoff
    transition_probability_matrix = np.array(transition_probability_matrix)

    adj_mat = transition_probability_matrix >= prob_cutoff

    # turn it into a graph
    temp_graph = nx.Graph(adj_mat)

    # get the connected components and convert it to lists
    comp_list = list(nx.connected_components(temp_graph))
    for i in range(0, len(comp_list)):
        comp_list[i] = list(comp_list[i])
        logger.debug(comp_list[i])
    return comp_list


def get_updated_sample_group_lists(sample_k_lists, comp_list):
    # get new lists
    new_sample_k_lists = []
    for i in range(0, len(comp_list)):
        logger.info('working on comp {}'.format(i))
        temp_comp_groups = comp_list[i]
        temp_list = []
        for j in range(0, len(temp_comp_groups)):
            logger.info('adding group {}'.format(temp_comp_groups[j]))
            temp_list += sample_k_lists[temp_comp_groups[j]]
        new_sample_k_lists.append(temp_list)
    # now sort them again
    for i in range(0, len(new_sample_k_lists)):
        new_sample_k_lists[i] = sorted(new_sample_k_lists[i])
    return new_sample_k_lists


def get_sample_k_table_from_sample_k_list(sample_k_lists, sample_names, sample_id_hash):
    # take in the sample k lists (list of list of sample indices by their cluster)
    sample_group_vect = np.zeros(len(sample_names))
    # convert the linked list to an array for faster search
    if len(sample_k_lists) > 0:
        for i in range(0, len(sample_k_lists)):
            temp_sample_list = sample_k_lists[i]
            for j in range(0, len(temp_sample_list)):
                temp_sample_idx = sample_id_hash[temp_sample_list[j]]
                sample_group_vect[temp_sample_idx] = i
    else:
        sample_group_vect = [0] * len(sample_names)
    # go through the samples and catelogue their group
    sample_k_table = []
    for i in range(0, len(sample_names)):
        sample_k_table.append([sample_names[i], sample_group_vect[i]])
    return sample_k_table


def get_ap_merge_new_clusters(sample_k_lists, optimal_centroid_indices, sample_ids, sample_id_hash, out_plot_dict,
                              full_expression, global_cmap, out_dir, dpi):
    original_transition_prob, reorder_cluster_map = do_cluster_merger(sample_k_lists=sample_k_lists,
                                                                      optimal_centroid_indices=optimal_centroid_indices,
                                                                      out_plot_dict=out_plot_dict,
                                                                      full_expression=full_expression,
                                                                      out_dir=out_dir,
                                                                      global_cmap=global_cmap,
                                                                      dpi=dpi,
                                                                      sample_id_hash=sample_id_hash,
                                                                      name_leader="original_ap_")
    comp_list = should_clusters_be_merged(original_transition_prob)
    sample_k_lists = get_updated_sample_group_lists(sample_k_lists, comp_list)
    cluster_indices = sample_k_lists_to_cluster_indices(sample_k_lists, sample_id_hash)
    sample_k_table = get_sample_k_table_from_sample_k_list(sample_k_lists, sample_ids, sample_id_hash)
    optimal_centroid_indices = get_known_centers(cluster_indices, full_expression)
    return sample_k_table, sample_k_lists, optimal_centroid_indices, reorder_cluster_map


def plot_2d(projection, out_plot, sample_id_hash, sample_k_lists, sample_k_table, colors, point_size, dpi,
            out_dir, x_ax='', y_ax=''):
    logger.info("Plotting {}".format(out_plot))
    plt.clf()
    for i, k_list in enumerate(sample_k_lists):
        logger.debug("{}".format(i))
        temp_idxs = [sample_id_hash[k] for k in k_list]
        try:
            plt.scatter(projection[temp_idxs, 0], projection[temp_idxs, 1],
                        label='sample_group_' + str(i),
                        color=colors[i],
                        s=point_size)
        except Exception as e:
            logger.error("exception triggered - {}".format(e))
            raise Exception("couldn't get {} to work successfully".format(out_plot))
        logger.info("done Plotting.")
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.xlabel(x_ax)
    plt.ylabel(y_ax)
    if out_plot is not None:
        logger.info("saving plot to file")
        plt.savefig(out_dir + out_plot,
                    dpi=dpi,
                    bbox_inches='tight')
        logger.info("written.")
    logger.info("process colours.")
    out_color = np.zeros((len(sample_k_table), 4))
    for i, k_list in enumerate(sample_k_lists):
        temp_idxs = [sample_id_hash[k] for k in k_list]
        out_color[temp_idxs, :] = colors[i]
    logger.info("completed plot.")
    return {'x': projection[:, 0],
            'y': projection[:, 1],
            'c': out_color,
            'xlab': x_ax,
            'ylab': y_ax}


def do_pca(in_mat, pca_model, out_plot_dict, sample_id_hash, sample_k_lists, sample_k_table, colors, dpi, point_size,
           out_dir, out_file='/sample_clustering_and_summary/pca_projection.png'):
    pca_projection = pca_model.fit_transform(np.transpose(in_mat))
    out_plot_dict['plots'][out_dir + out_file] = plot_2d(projection=pca_projection,
                                                         out_plot=out_file,
                                                         sample_id_hash=sample_id_hash,
                                                         sample_k_lists=sample_k_lists,
                                                         sample_k_table=sample_k_table,
                                                         colors=colors,
                                                         out_dir=out_dir,
                                                         x_ax='PC1',
                                                         y_ax='PC2',
                                                         point_size=point_size,
                                                         dpi=dpi)
    return pca_projection


def do_tsne(in_mat, model, out_plot_dict, out_dir,
            sample_id_hash, sample_k_lists, sample_k_table, colors, point_size, dpi,
            out_file='/sample_clustering_and_summary/tsne_projection.png'):
    logger.info("transforming model.")
    tsne_projection = model.fit_transform(np.transpose(in_mat))
    if HAS_UMAP:
        logger.info('doing UMAP')
        out_file = '/sample_clustering_and_summary/umap_projection.png'
        out_plot_dict['plots'][out_dir + out_file] = plot_2d(projection=tsne_projection,
                                                             out_plot=out_file,
                                                             sample_id_hash=sample_id_hash,
                                                             sample_k_lists=sample_k_lists,
                                                             sample_k_table=sample_k_table,
                                                             colors=colors,
                                                             out_dir=out_dir,
                                                             point_size=point_size,
                                                             dpi=dpi,
                                                             x_ax='UMAP1', y_ax='UMAP2')
    else:
        logger.info('doing tSNE')
        out_plot_dict['plots'][out_dir + out_file] = plot_2d(projection=tsne_projection,
                                                             out_plot=out_file,
                                                             sample_id_hash=sample_id_hash,
                                                             sample_k_lists=sample_k_lists,
                                                             sample_k_table=sample_k_table,
                                                             colors=colors,
                                                             out_dir=out_dir,
                                                             dpi=dpi,
                                                             point_size=point_size,
                                                             x_ax='tSNE1', y_ax='tSNE2')

    return tsne_projection


def main():
    args = parse_arguments()

    clustering(infile=args.infile,
               sc_clust=args.sc_clust,
               neg_cor_count_clust=args.neg_cor_count_clust,
               sample_cluster_iter=args.sample_cluster_iter,
               rand_seed=args.rand_seed,
               sample_k_known=args.sample_k_known,
               out_dir=args.out_dir,
               neg_cor_clust=args.neg_cor_clust,
               id_list=args.ID_list,
               columns=args.columns,
               clust_on_genes=args.clust_on_genes,
               do_hdf5=args.do_hdf5,
               dpi=args.dpi,
               first_neg_neighbor=args.first_neg_neighbor,
               var_norm=args.var_norm,
               pos_adj_list=args.pos_adj_list,
               log=args.log,
               rank=args.rank,
               species=args.species,
               manual_sample_groups=args.manual_sample_groups,
               leave_mito_ribo=args.leave_mito_ribo,
               do_rows=args.do_rows,
               block_size=args.block_size,
               perplexity=int(args.perplexity) if args.peplexity else None,
               ap_clust=args.ap_clust,
               ap_merge=args.ap_merge,
               tsne_iter=args.tsne_iter,
               spearman_dist=args.spearman_dist,
               point_size=args.point_size,
               do_merger=args.do_merger,
               neg_cor_cutoff=args.neg_cor_cutoff,
               rho_cutoff=args.rho_cutoff)


def clustering(infile,
               neg_cor_count_clust,
               out_dir,
               neg_cor_clust,
               id_list,
               columns,
               clust_on_genes,
               pos_adj_list,
               manual_sample_groups,
               rho_cutoff,
               sample_cluster_iter=10,
               rand_seed=12345,
               sample_k_known=0,  # 0 means unknown
               perplexity=None,
               sc_clust=False,
               do_hdf5=False,
               dpi=600,
               first_neg_neighbor=False,
               var_norm=True,
               log=False,
               rank=False,
               species=HSAPIENS,
               leave_mito_ribo=False,
               do_rows=False,
               block_size=5000,
               ap_clust=False,
               ap_merge=False,
               spearman_dist=True,
               tsne_iter='1e6',
               point_size=6,
               do_merger=False,
               neg_cor_cutoff=15,
               ):
    mem_err = False
    reorder_cluster_map = None
    full_expression = None
    full_expression_np = None
    full_expression_str = None
    optimal_centroid_indices = None
    row_names = None
    cor_stat_table = None
    multiply_by_min = True
    first_prob = True
    cluster_prob = False

    if sc_clust:
        do_merger = True
        spearman_dist = True
        ap_clust = True
        # ap_merge = True
        if neg_cor_count_clust is None:
            raise Exception('-sc_clust requires an input for the -neg_cor_count_clust option')
    infile = os.path.realpath(infile)
    infile_original_dir = get_file_path(infile)
    np.random.seed(rand_seed)
    random.seed(rand_seed)
    # make a good heatmap color scheme
    global_cmap = sns.color_palette("coolwarm", 256)
    if sample_cluster_iter < 1:
        raise Exception('-sample_cluster_iter must be greater than zero, or else we have nothing to do')
    if not out_dir:
        out_dir = get_file_path(infile) + 'sample_clustering_and_summary/'
    if not out_dir.endswith(os.sep):
        out_dir = out_dir + os.sep
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    sample_dir = out_dir + 'sample_clustering_and_summary/'

    if sample_k_known < 0:
        raise Exception('sample_k_known must be a positive integer, or zero, but got:' + str(sample_k_known))
    if neg_cor_clust is not None and clust_on_genes is not None:
        raise Exception("-neg_cor_clust and -clust_on_genes are mutually exclusive arguments")
    if not do_hdf5:
        full_expression_str = read_table(infile)
    else:
        row_names = read_file(id_list, 'lines')
        logger.info('making a maliable hdf5 file to preserve the original data')
        run_cmd(['cp', infile, infile + '_copy'])

        infile_path = infile + '_copy'
        h5f = h5py.File(infile_path, 'r+')
        full_expression = h5f["infile"]

    if neg_cor_clust is not None:
        cor_stat_table, neg_cor_cutoff = get_negative_correlations(dpi, neg_cor_clust, out_dir)
    passing_ids = set()
    usable_indices = []
    if neg_cor_clust is not None:
        for i in range(1, len(cor_stat_table)):
            # start at 1 because of the title line
            if cor_stat_table[i][2] >= neg_cor_cutoff:
                # format is: id, # pos cor, # neg cor
                passing_ids.add(cor_stat_table[i][0])

        if not passing_ids:
            # if there aren't any anti-correlated genes,
            # then you probably only have one one cell type/state
            logger.warning('we only found one cell type/state')
    if neg_cor_count_clust is not None:
        neg_cor_clust_table = read_table(neg_cor_count_clust)
        for idx, item in enumerate(neg_cor_clust_table):
            if item[-1] == "True":
                passing_ids.add(item[0])
    if neg_cor_clust is not None or neg_cor_count_clust is not None:
        # to prevent giving too much weight to a single gene's expression, we can get the
        # first neighbor genes of the co-regulatory network, and add them to the genes
        # which got into the first pass negative correlation subset
        if first_neg_neighbor and False:
            logger.info('getting the first neighbors of the highly negatively correlated genes')
            first_neighbors = get_first_neighbors(passing_ids, pos_adj_list)
            passing_ids.update(first_neighbors)

        if not do_hdf5:
            logger.info('{} of the variables had enough negative correlations to make the cut'.format(len(passing_ids)))
            # collect the IDs that
            usable_indices = []
            for i in range(1, len(full_expression_str)):
                # start at 1 because of the title line
                if full_expression_str[i][0] in passing_ids:
                    usable_indices.append(i - 1)  # -1 because of the title line
                    # temp_full_expression_str.append(full_expression_str[i])
            # full_expression_str = temp_full_expression_str[:]
            # del temp_full_expression_str
            logger.info('found {} usable indicies'.format(len(usable_indices)))
            # write_table(full_expression_str[:],out_dir+'neg_cor_mat_used_for_clustering.txt')
        else:
            usable_indices = []
            new_row_names = []
            for i, row_name in enumerate(row_names):
                if row_name in passing_ids:
                    usable_indices.append(i)
                    new_row_names.append(row_name)
            logger.info('{} of the variables had enough negative correlations to make the cut'
                        .format(len(usable_indices)))
    if not do_hdf5:
        full_expression_np = np.array(full_expression_str)
    if do_rows:
        if not do_hdf5:
            full_expression_np = np.transpose(full_expression_np)
        else:
            full_expression = np.transpose(full_expression)
    # remove nans
    if not do_hdf5:
        full_expression_num = np.array(full_expression_np[1:, 1:], dtype=float)
        full_expression_num[np.isnan(full_expression_num)] = 0.0
        full_expression_np[1:, 1:] = full_expression_num
    else:
        for i in range(0, np.shape(full_expression)[0]):
            temp_row = full_expression[i]
            temp_row[np.isnan(temp_row)] = 0.0
            full_expression[i] = temp_row
    if log:
        if not do_hdf5:
            full_expression_num = np.array(full_expression_np[1:, 1:], dtype=float)
            full_expression_np[1:, 1:] = np.log2(full_expression_num - np.min(full_expression_num) + 1)
        else:
            for i in range(0, np.shape(full_expression)[0]):
                temp_row = full_expression[i, :]
                temp_row = np.log2(temp_row + 1)
                full_expression[i, :] = temp_row
    if rank:

        # this will rank transform the features being clustered
        if not do_hdf5:
            full_expression_num = np.array(full_expression_np[1:, 1:], dtype=float)
            for i in range(0, np.shape(full_expression_num)[0]):
                full_expression_num[i, :] = rankdata(full_expression_num[i, :])
            full_expression_np[1:, 1:] = full_expression_num
        else:
            for i in range(0, np.shape(full_expression)[0]):
                full_expression[i, :] = rankdata(full_expression[i, :])
    if var_norm or ap_clust:
        logger.info('normalizing the rows')
        if not do_hdf5:
            full_expression_np[1:, 1:] = lin_norm_rows(np.array(full_expression_np[1:, 1:], dtype=float))
        else:
            full_expression = lin_norm_rows_hdf5(full_expression)
    if not do_hdf5:
        if not do_rows:
            title = list(full_expression_np[0])
            row_names = full_expression_np[1:, 0]
            full_expression = np.array(full_expression_np[1:, 1:], dtype=float)
        else:
            row_names = list(full_expression_np[0])
            title = full_expression_np[:, 0]
            full_expression = np.array(full_expression_np[1:, 1:], dtype=float)
    else:
        if not do_rows:
            title = read_file(columns)
            row_names = read_file(id_list)
        else:
            row_names = read_file(columns)[1:]
            title = ['variables'] + read_file(id_list)

    sample_ids = title[1:]
    sample_id_hash = {key: value for value, key in enumerate(sample_ids)}

    id_list = list(row_names)
    id_hash = {key: idx for idx, key in enumerate(id_list)}
    logger.debug(len(id_list))
    logger.debug(id_list[:5])
    if clust_on_genes is not None:
        logger.info('picking out the specified genes')
        # this can either be a table with the genes in the left most column or a
        # new-line delimited list of only the gene ids
        # if the last column is "False" it means this gene will not be included
        clust_on_genes = read_table(clust_on_genes)
        clust_gene_ids = []
        if isinstance(clust_on_genes[0], list):
            for i, c_o_g in enumerate(clust_on_genes):
                clust = c_o_g[0]
                try:
                    id_hash[str(clust)]
                except KeyError:
                    logger.error("couldn't find {}".format(clust))
                else:
                    if c_o_g[-1] != "False":
                        clust_gene_ids.append(clust)
        else:
            clust_gene_ids = clust_on_genes
        clust_gene_idxs = [id_hash[str(x)] for x in clust_gene_ids]
        if len(clust_gene_idxs) == 0:
            raise Exception("couldn't find any of the genes in your input -clust_on_genes")
        usable_indices = clust_gene_idxs[:]
        logger.info('we found {} genes in -clust_on_genes input'.format(len(clust_gene_idxs)))
    else:
        # if we've already filtered for negative correlations or something like that
        if usable_indices:
            clust_gene_idxs = usable_indices[:]
            # clust_gene_ids = np.array(id_list)[usable_indices]
        # If there weren't any other filters prior to this step
        else:
            # clust_gene_ids = id_list[:]
            clust_gene_idxs = np.arange(len(id_list)).tolist()

    # remove the ribosomal and mitochondrial genes if we need to
    if not leave_mito_ribo:
        clust_gene_idxs, usable_indices = remove_mito_ribo_genes(id_list, infile_original_dir, species, usable_indices)

    logger.info("{} currently usable indices".format(len(usable_indices)))
    # see which of these passing ids
    temp_usable_indices = list(set(usable_indices))
    # remove genes that are only expressed in a very small number of cells
    remove_sparse = True
    if remove_sparse and clust_on_genes is None:
        logger.info('removing genes that are only observed in 1 percent or less of cells')
        cutoff = np.shape(full_expression)[1] * .05  # 1% cutuff for expression
        usable_indices = []
        for i in temp_usable_indices:
            num_seen = np.sum(np.array(full_expression[i, :] > 0, dtype=int))
            if num_seen >= cutoff:
                usable_indices.append(i)
        logger.info('{} genes removed because of sparse expression'.format(len(temp_usable_indices) - len(usable_indices)))
        logger.info('{} used for clustering'.format(len(usable_indices)))
        clust_gene_idxs = usable_indices[:]
    # end feature selection
    usable_indices = clust_gene_idxs

    # handle null case, where there are no genes that met the selection criterion
    # this means there is only a single sample group
    if not usable_indices:
        out_table = []
        for i in range(1, len(title)):
            out_table.append([title[i],0])
        out_sample_file = sample_dir + "sample_k_means_groups.txt"
        write_table(out_table, out_sample_file)
        manual_sample_groups = out_sample_file

    if usable_indices:
        full_expression = full_expression[usable_indices, :]
    # do the clustering
    grouping_vector = None
    if manual_sample_groups is not None:
        logger.info('Manual sample groups!')
        sample_group_table = read_table(manual_sample_groups)
        sample_group_table_np = np.array(sample_group_table)

        grouping_vector = list(np.transpose(sample_group_table_np[:, 1]))

        if len(list(set(grouping_vector))) == 1:
            raise Exception('only one sample group, nothing to calculate')

        sample_cluster_ids = []
        for sample_group in sample_group_table:
            # THIS IS IMPORTANT
            # here we assume that the samples are all listed in the same order as in '-infile'
            # we also assume that the group indexing starts at 0
            sample_cluster_ids.append(sample_group[1])
        sample_cluster_ids = [int(i) for i in sample_cluster_ids]

        sample_k_lists = [[] for _ in range(max(sample_cluster_ids) + 1)]
        # now populate the list of lists
        for idx, cluster in enumerate(sample_cluster_ids):
            # this appends the sample index to
            sample_k_lists[cluster].append(idx)

        # get the 'optimal_centroid_indices' for manual sample_groups
        optimal_centroid_indices = get_known_centers(sample_k_lists, full_expression)

    neg_euc_hdf5_file = sample_dir + "rho_dicts/neg_euc_dist.hdf5"
    if spearman_dist:

        full_expression, mem_err = run_spearman_dist(block_size=block_size,
                                                     columns=columns,
                                                     dpi=dpi,
                                                     full_expression=full_expression,
                                                     global_cmap=global_cmap,
                                                     id_list=id_list,
                                                     infile=infile,
                                                     output_dir=sample_dir,
                                                     usable_indices=usable_indices,
                                                     rho_cutoff=rho_cutoff)
    # find k means for all samples
    # original ap
    if spearman_dist and not ap_clust and os.path.isfile(neg_euc_hdf5_file):
        # read in the spearman hdf5 file and make full_expression that
        logger.info('reading in the spearman correlation matrix from the hdf5 file')
        spear_h5f = h5py.File(neg_euc_hdf5_file, 'r+')
        full_expression = spear_h5f["infile"]
    logger.info('starting clustering on:')
    logger.info(full_expression)
    ap_results = None

    # and make the output for plotting later
    out_plot_dict = {"transition_probability": None, "plots": {}}
    if manual_sample_groups is None:
        if not ap_clust:
            sample_k_table, sample_k_lists, f_list, optimal_centroid_indices = \
                do_unknown_k_means_clustering_iter(sample_ids=sample_ids,
                                                   full_expression=full_expression,
                                                   sample_k_known=sample_k_known,
                                                   cluster_prob=cluster_prob,
                                                   first_prob=first_prob,
                                                   out_dir=out_dir,
                                                   multiply_by_min=multiply_by_min,
                                                   cluster_iter=sample_cluster_iter,
                                                   performed_on_samples=True)
        else:

            sample_k_table, sample_k_lists, f_list, optimal_centroid_indices, ap_results = \
                do_ap_clust(
                    sample_ids=sample_ids,
                    clust_array=full_expression,
                    neg_euc_hdf5_file=neg_euc_hdf5_file,
                    mem_err=mem_err,
                    spearman_dist=spearman_dist,
                    block_size=block_size,
                    out_dir=out_dir,
                    usable_indices=usable_indices,
                    infile=infile,
                    columns=columns,
                    id_list=id_list,
                    rho_cutoff=rho_cutoff)

            # if ap clustering split it into too many, the preference was likely too low (beyond the point of stability)
            # so we'll increase it a bit
            logger.info("len(optimal_centroid_indices)>.25*len(sample_ids)")
            logger.info(len(optimal_centroid_indices), .1 * len(sample_ids))
            logger.info(len(optimal_centroid_indices) > .1 * len(sample_ids))
            if len(optimal_centroid_indices) > .1 * len(sample_ids):
                logger.info("affinity propogation failed likely because there were too "
                            "few real clusters: trying again with PyMINEr k-means")
                sample_k_table, sample_k_lists, f_list, optimal_centroid_indices = \
                    do_unknown_k_means_clustering_iter(sample_ids=sample_ids,
                                                       full_expression=full_expression,
                                                       sample_k_known=sample_k_known,
                                                       cluster_prob=cluster_prob,
                                                       first_prob=first_prob,
                                                       out_dir=out_dir,
                                                       multiply_by_min=multiply_by_min,
                                                       cluster_iter=sample_cluster_iter,
                                                       performed_on_samples=True)
            else:
                if ap_merge:
                    # if we're doing the ap_merge protocol for agglomerative ap_clusering
                    # cluster_indices = sample_k_lists_to_cluster_indices(sample_k_lists, sample_id_hash)
                    # sample_means = get_sample_means(sample_k_lists, cluster_indices, full_expression)
                    sample_k_table, sample_k_lists, optimal_centroid_indices, reorder_cluster_map = \
                        get_ap_merge_new_clusters(sample_k_lists=sample_k_lists,
                                                  optimal_centroid_indices=optimal_centroid_indices,
                                                  sample_ids=sample_ids,
                                                  sample_id_hash=sample_id_hash,
                                                  out_plot_dict=out_plot_dict,
                                                  full_expression=full_expression,
                                                  out_dir=out_dir,
                                                  dpi=dpi,
                                                  global_cmap=global_cmap)

    else:
        labels = ["{:.0f}".format(i) for i in grouping_vector]
        sample_k_table, sample_k_lists = group_list_to_sample_k_table(labels, sample_ids)

    logger.info("sample_k_table size: {}".format(len(sample_k_table)))
    logger.info("sample_k_lists size: {}".format(len(sample_k_lists)))

    out_plot_dict["exemplar_indices"] = np.array([int(i) for i in optimal_centroid_indices], dtype=int)
    # now we need to write it all down
    # here we create the vector of sample ids

    if len(sample_ids) != len(sample_k_table):
        raise Exception('number of input known sample groups does not equal the number of samples in the input file')
    sample_cluster_ids = np.zeros(len(sample_ids), dtype=int) - 1  # first start out with -1 as default groups
    # then we go through the sample_k_table finding the index of that sample
    # then plug in the sample cluster id in the right spot
    for i in range(0, len(sample_ids)):
        temp_index = np.where(sample_ids == sample_k_table[i][0])
        sample_cluster_ids[temp_index] = sample_k_table[i][1]
    logger.info("we found {} clusters".format(len(sample_k_lists)))
    # cluster_indices = sample_k_lists_to_cluster_indices(sample_k_lists, sample_id_hash)
    # write the results of the cell clustering
    write_table(sample_k_table, sample_dir + 'sample_k_means_groups.txt')
    if manual_sample_groups is None:
        copy_centroids = optimal_centroid_indices[:]
        make_file('\n'.join(str(i) for i in copy_centroids), out_dir + '/centroid_indices.txt')
    # plot some results
    colors = cm.nipy_spectral(np.arange(len(sample_k_lists)) / len(sample_k_lists))
    out_plot_dict['color_vect'] = colors

    # calculate the means for each group
    # cluster_indices = sample_k_lists_to_cluster_indices(sample_k_lists, sample_id_hash)
    # sample_means = get_sample_means(sample_k_lists, cluster_indices, full_expression)
    if do_merger:
        do_cluster_merger(sample_k_lists=sample_k_lists,
                          optimal_centroid_indices=optimal_centroid_indices,
                          out_plot_dict=out_plot_dict,
                          full_expression=full_expression,
                          out_dir=out_dir,
                          sample_id_hash=sample_id_hash,
                          global_cmap=global_cmap,
                          dpi=dpi,
                          name_leader='final_',
                          colors=colors)

    if perplexity is None:
        # get the average group size
        avg_grp_size = len(sample_ids) / len(sample_k_lists)
        if avg_grp_size < 5:
            perplexity = 5
        elif avg_grp_size > 100:
            perplexity = 100
        else:
            perplexity = round(avg_grp_size)
        if HAS_UMAP:
            perplexity = 5  # int(avg_grp_size / 5) + 1
        logger.info('average group size: {}'.format(avg_grp_size))
        logger.info('perplexity is set to: {}'.format(perplexity))
    if not HAS_UMAP:
        model = TSNE(n_components=2,  # init = 'pca',
                     perplexity=perplexity, random_state=rand_seed, n_iter=int(eval(tsne_iter)))
    else:
        model = umap.UMAP(n_neighbors=perplexity, min_dist=0.3, metric='correlation')
    pca_model = PCA(n_components=2)
    logger.info('doing PCA')
    do_pca(in_mat=full_expression[optimal_centroid_indices, :],
           pca_model=pca_model,
           out_plot_dict=out_plot_dict,
           sample_id_hash=sample_id_hash,
           sample_k_lists=sample_k_lists,
           sample_k_table=sample_k_table,
           colors=colors,
           out_dir=out_dir,
           dpi=dpi,
           point_size=point_size)
    try:
        do_tsne(in_mat=full_expression[optimal_centroid_indices, :],
                model=model,
                out_plot_dict=out_plot_dict,
                out_dir=out_dir,
                colors=colors,
                dpi=dpi,
                point_size=point_size,
                sample_id_hash=sample_id_hash,
                sample_k_lists=sample_k_lists,
                sample_k_table=sample_k_table)
    except Exception as e:
        logger.error("couldn't get the full t-sne working unfortunately...")
        logger.error(e)
    if ap_clust and not spearman_dist and manual_sample_groups is None:
        logger.info('doing AP PCA')
        do_pca(in_mat=ap_results.affinity_matrix_[optimal_centroid_indices, :],
               pca_model=pca_model,
               out_plot_dict=out_plot_dict,
               sample_id_hash=sample_id_hash,
               sample_k_lists=sample_k_lists,
               sample_k_table=sample_k_table,
               colors=colors,
               out_dir=sample_dir,
               dpi=dpi,
               point_size=point_size,
               out_file='affinity_matrix_pca.png')
        logger.info('doing AP tSNE')
        try:

            do_tsne(in_mat=ap_results.affinity_matrix_[optimal_centroid_indices, :],
                    model=model,
                    out_plot_dict=out_plot_dict,
                    out_dir=sample_dir,
                    sample_id_hash=sample_id_hash,
                    sample_k_lists=sample_k_lists,
                    sample_k_table=sample_k_table,
                    colors=colors,
                    dpi=dpi,
                    point_size=point_size,
                    out_file='affinity_matrix_tsne.png')

        except Exception as e:
            logger.error("couldn't get the ap t-sne working unfortunately...")
            logger.error(e)
    # get a vector with group number
    group_reordering_vector, linear_groups = group_reordering(reorder_cluster_map, sample_id_hash, sample_ids,
                                                              sample_k_lists)
    out_plot_dict['linear_groups'] = linear_groups
    out_plot_dict["sample_k_lists"] = sample_k_lists
    logger.info('plotting some heatmaps and similarity scatters')
    count_of_plots = 1
    logger.info("plot {}.".format(count_of_plots))
    count_of_plots += 1
    try:
        plot = sns.clustermap(full_expression, cmap=global_cmap)
        plot.savefig(sample_dir + 'used_for_clustering_heatmap.png',
                     dpi=dpi,
                     bbox_inches='tight')
    except Exception:
        logger.error("couldn't get the full heatmap going")
        sample = np.random.choice(np.shape(full_expression)[0], size=(750,), replace=False)
        plot = sns.clustermap(full_expression[sample, :], cmap=global_cmap)
        plot.savefig(sample_dir + 'sample_used_for_clustering_heatmap.png',
                     dpi=dpi,
                     bbox_inches='tight')
    logger.info("plot {}.".format(count_of_plots))
    count_of_plots += 1
    try:
        plot = sns.clustermap(full_expression[:, group_reordering_vector],
                              col_cluster=False,
                              col_colors=colors[linear_groups][group_reordering_vector],
                              cmap=global_cmap)
        plot.savefig(sample_dir + 'clustering_groups.png',
                     dpi=dpi,
                     bbox_inches='tight')
    except Exception:
        logger.error("couldn't get the group clustered heatmap going")
    if ap_clust and not spearman_dist and manual_sample_groups is None:
        ap_mat = ap_results.affinity_matrix_[:, group_reordering_vector]
        ap_mat = ap_mat[group_reordering_vector, :]
        # ap_mat = np.log2(ap_mat**2+1)
        logger.debug(np.shape(ap_mat))
        logger.info("plot {}.".format(count_of_plots))
        count_of_plots += 1
        try:
            plot = sns.clustermap(ap_mat,
                                  col_cluster=False,
                                  col_colors=colors[linear_groups][group_reordering_vector],
                                  row_cluster=False,
                                  row_colors=colors[linear_groups][group_reordering_vector],
                                  cmap=global_cmap)
            plot.savefig(sample_dir + 'affinity_matrix.png',
                         dpi=dpi,
                         bbox_inches='tight')
        except Exception:
            logger.error("couldn't get the affinity map figure")
        logger.info("plot {}.".format(count_of_plots))
        count_of_plots += 1
        try:
            plot = sns.clustermap(ap_mat,
                                  col_cluster=True,
                                  col_colors=colors[linear_groups][group_reordering_vector],
                                  row_cluster=True,
                                  row_colors=colors[linear_groups][group_reordering_vector],
                                  cmap=global_cmap)
            plot.savefig(sample_dir + 'clustered_affinity_matrix.png',
                         dpi=dpi,
                         bbox_inches='tight')
        except Exception:
            logger.error("couldn't get the affinity map figure")
    if spearman_dist and not ap_clust:
        temp_full_mat = full_expression[:, group_reordering_vector]
        temp_full_mat = temp_full_mat[group_reordering_vector, :]
        # ap_mat = np.log2(ap_mat**2+1)
        logger.debug(np.shape(temp_full_mat))
        logger.info("plot {}.".format(count_of_plots))
        count_of_plots += 1
        try:
            plot = sns.clustermap(temp_full_mat,
                                  col_cluster=False,
                                  col_colors=colors[linear_groups][group_reordering_vector],
                                  row_cluster=False,
                                  row_colors=colors[linear_groups][group_reordering_vector],
                                  cmap=global_cmap)
            plot.savefig(sample_dir + 'spearman_matrix.png',
                         dpi=dpi,
                         bbox_inches='tight')
        except Exception:
            logger.error("couldn't get the spearman map figure")

        logger.info("plot {}.".format(count_of_plots))
        count_of_plots += 1
        try:
            plot = sns.clustermap(temp_full_mat,
                                  col_cluster=True,
                                  col_colors=colors[linear_groups][group_reordering_vector],
                                  row_cluster=True,
                                  row_colors=colors[linear_groups][group_reordering_vector],
                                  cmap=global_cmap)
            plot.savefig(sample_dir + 'clustered_spearman_matrix.png',
                         dpi=dpi,
                         bbox_inches='tight')
        except Exception:
            logger.error("couldn't get the spearmanS map figure")
    out_plot_dict['group_reordering_vector'] = group_reordering_vector
    out_plot_dict['reordered_colors'] = colors[linear_groups][group_reordering_vector]
    save_dict(out_plot_dict, sample_dir + 'clustering_plots.pkl')
    logger.info('finished clustering')


def group_reordering(reorder_cluster_map, sample_id_hash, sample_ids, sample_k_lists):
    linear_groups = []
    group_reordering_vector = []

    logger.info("reordering - linear groups")
    for name in sample_ids:
        for i, sample_k in enumerate(sample_k_lists):
            if name in sample_k:
                linear_groups.append(i)

    if reorder_cluster_map:
        logger.info("reordering - cluster map")
        group_order_list = list(reorder_cluster_map.dendrogram_row.reordered_ind)
        for temp_group in group_order_list:
            group_reordering_vector.extend(sample_k_lists[temp_group])
    else:
        logger.info("reordering - sample_k_lists")
        for group in sample_k_lists:
            group_reordering_vector.extend(group)

    temp_list = [sample_id_hash[i] for i in group_reordering_vector]
    logger.info("completed reordering.")
    return temp_list, linear_groups


def get_negative_correlations(dpi, neg_cor_clust, out_dir):
    logger.info('getting the negatively correlated variables')
    # first we get the variables that pass muster (ie: have enough negative correlations)
    cor_stat_table = read_table(neg_cor_clust)
    # make the plots
    cor_stat_array = np.array(cor_stat_table[:])
    # get the vectors for the correlation counts
    neg_cor_vect = np.array(cor_stat_array[1:, 2], dtype=float)
    pos_cor_vect = np.array(cor_stat_array[1:, 1], dtype=float)
    neg_cor_density_x, neg_cor_density_y = get_density(neg_cor_vect)
    pos_cor_density_x, pos_cor_density_y = get_density(pos_cor_vect)
    # plot them
    plot_densities(neg_cor_density_x, neg_cor_density_y, pos_cor_density_x, pos_cor_density_y,
                   out_dir + 'negative_positive_correlation_degree_density.png', dpi)
    # do the same but after removing the zeros
    # remove the zeros from these vectors
    neg_cor_vect_no_zero = neg_cor_vect[:]
    pos_cor_vect_no_zero = pos_cor_vect[:]
    neg_cor_vect_no_zero = remove_zeros(neg_cor_vect_no_zero)
    pos_cor_vect_no_zero = remove_zeros(pos_cor_vect_no_zero)
    # get their densities and plot them
    non_zero_neg_cor_density_x, non_zero_neg_cor_density_y = get_density(neg_cor_vect_no_zero)
    non_zero_pos_cor_density_x, non_zero_pos_cor_density_y = get_density(pos_cor_vect_no_zero)
    plot_densities(non_zero_neg_cor_density_x, non_zero_neg_cor_density_y, non_zero_pos_cor_density_x,
                   non_zero_pos_cor_density_y,
                   out_dir + 'non_zero_negative_positive_correlation_degree_density.png',
                   dpi)
    # come up with the dynamically determined cutoff
    neg_cor_vect_no_zero = sorted(neg_cor_vect_no_zero)
    percentile_cutoff = 0.85
    top_num = int(len(neg_cor_vect_no_zero) * percentile_cutoff)
    logger.debug(neg_cor_vect_no_zero[top_num])
    neg_cor_cutoff = neg_cor_vect_no_zero[top_num]
    return cor_stat_table, neg_cor_cutoff


def run_spearman_dist(block_size, columns, dpi, full_expression, global_cmap, id_list, infile, output_dir,
                      usable_indices, rho_cutoff):
    row_dim = np.shape(full_expression)[0]
    col_dim = np.shape(full_expression)[1]
    sample_rows = np.array(
        list(sorted(
            np.random.choice(np.shape(full_expression)[0], size=(min([1000, row_dim]),), replace=False))),
        dtype=int)
    sample_cols = np.array(
        list(
            sorted(np.random.choice(np.shape(full_expression)[1], size=(min([500, col_dim]),), replace=False))),
        dtype=int)
    full_expression_r = full_expression[sample_rows, :]
    full_expression_r = full_expression_r[:, sample_cols]
    plot = sns.clustermap(full_expression_r, cmap=global_cmap)
    plot.savefig(output_dir + 'sample_genes_used_for_clustering_heatmap.png',
                 dpi=dpi,
                 bbox_inches='tight')
    full_expression_r = full_expression_r - np.mean(full_expression_r, axis=0)
    plot = sns.clustermap(full_expression_r, cmap=global_cmap)
    plot.savefig(output_dir + 'sample_genes_used_for_clustering_heatmap_mean_centered.png',
                 dpi=dpi,
                 bbox_inches='tight')
    logger.info("calculating sample-wise spearman correlations")
    # gene_full_expression_copy = full_expression[:]
    mem_err = False
    try:
        full_expression = no_p_spear(full_expression)
    except MemoryError:
        logger.error("Memory error while trying to run no_p_spear().")
        mem_err = True
    else:
        logger.debug("spear cor: {}".format(full_expression[:5, :5]))
        total_vars = np.shape(full_expression)[1]
        num_nan = np.sum(np.array(np.isnan(full_expression), dtype=int))
        num_inf = np.sum(np.array(np.isinf(full_expression), dtype=int))
        if num_nan + num_inf > 0:
            logger.warning("{} nans found!".format(num_nan))
            logger.warning("{} inf found!".format(num_inf))
        full_expression = np.nan_to_num(full_expression)
        num_nan = np.sum(np.array(np.isnan(full_expression), dtype=int))
        num_inf = np.sum(np.array(np.isinf(full_expression), dtype=int))
        if num_nan + num_inf > 0:
            logger.warning("{} nans found!".format(num_nan))
            logger.warning("{} inf found!".format(num_inf))

        full_expression = -metrics.pairwise.euclidean_distances(full_expression, squared=True) / np.log2(total_vars)
        logger.debug('-euclidean dist: {}'.format(full_expression[:5, :5]))
    if mem_err:
        logger.error(
            "couldn't do the full spearman in one go, we'll have to break it up into chuncks and do it peice meal")
        get_big_spearman(block_size, output_dir, usable_indices, infile, columns, id_list, rho_cutoff)
    return full_expression, mem_err


def remove_mito_ribo_genes(id_list, infile_original_dir, species, usable_indices):
    ensg_idx = None
    annotations = None
    logger.info("finding all of the ribosomal and mitchondrial genes to remove")
    # if we'll remove the mitochondrial and ribsomal related genes first

    ribo_mito_go = GO_MITOCHONDRIAL + GO_RIBOSOME
    gp = GProfiler('PyMINEr_' + str(random.randint(0, int(1e6))),
                   want_header=True)
    results = gp.gconvert(ribo_mito_go, organism=HSAPIENS, target="ENSG")
    # first go to the annotation file, and read that in.
    if species == HSAPIENS:
        if os.path.isfile(infile_original_dir + 'annotations.tsv'):
            annotations = read_table(infile_original_dir + 'annotations.tsv')
            ensg_idx = 3
    else:
        if os.path.isfile(infile_original_dir + 'human_orthologues.tsv'):
            annotations = read_table(infile_original_dir + 'human_orthologues.tsv')
            ensg_idx = 4
    # catelogue the results
    ensg_mito_ribo_dict = process_dict(results, 3)
    all_mito_ribo_genes = list(ensg_mito_ribo_dict.keys())
    logger.info('found {} mito or ribo genes'.format(len(all_mito_ribo_genes)))
    logger.debug(all_mito_ribo_genes[:3])
    final_clust_gene_ids = []
    final_clust_gene_idxs = []
    for i in range(1, len(annotations)):
        temp_gene = annotations[i][ensg_idx]
        if temp_gene not in ensg_mito_ribo_dict:
            if int(annotations[i][0]) - 1 in usable_indices:
                final_clust_gene_ids.append(id_list[int(annotations[i][0]) - 1])
                final_clust_gene_idxs.append(int(annotations[i][0]) - 1)
    clust_gene_idxs = list(set(final_clust_gene_idxs))
    usable_indices = clust_gene_idxs
    return clust_gene_idxs, usable_indices


if __name__ == "__main__":
    main()
