import argparse
import fileinput
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import os
import random
import logging
import sys

from modules.common_functions import read_file, strip_split, read_table, write_table, import_dict, save_dict

logger = logging.getLogger(__name__)

COMMUNITY_INSTALLED = False

try:
    import community

    COMMUNITY_INSTALLED = True

except ModuleNotFoundError:
    logger.warning('https://github.com/taynaud/python-louvain is a requirement for finding communites')


def valid_layout(in_arg):
    if in_arg in ['spring', 'spectral', 'circle', 'random', 'shell']:
        return in_arg
    else:
        msg = '\n' + in_arg + " is not a valid layout, specify one of the following:\n" \
                              "'spring','spectral','circle','random','shell'"
        raise argparse.ArgumentTypeError(msg)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-graph", '-g',
                        help="if you already have a graph object that was saved by this program, we can "
                             "just use that instead of recalculating everything",
                        type=str)
    parser.add_argument("-id_list", '-id', '-ID', '-IDs', '-ids',
                        dest='id_list',
                        help="these are sorted IDs matching the order of the original input dataset")
    parser.add_argument("-adj_list", "-adj",
                        help="this file contains the adjacency list which will make the graph")
    parser.add_argument("-rand_seed",
                        help="the random seed used in generating the layout",
                        type=int,
                        default=12345678)
    parser.add_argument("-no_default_plot", "-no_default",
                        help="Don't do the default plot with red nodes",
                        action='store_true',
                        default=False)
    parser.add_argument("-node_color", "-nc",
                        help="Name of the node attribute(s) (separated by commas) to be used for coloring the "
                             "nodes. As currently implemented, this only works for continuous variables")
    parser.add_argument("-color_scheme", '-cs',
                        help="matplotlib color scheme for the node colors if used",
                        default="bwr")
    parser.add_argument("-layout",
                        help="the layout algorithm used",
                        type=valid_layout,
                        default="spring")
    parser.add_argument("-redo_layout",
                        help="If you already have a graph, but want to re-do the layout",
                        action='store_true',
                        default=False)
    parser.add_argument("-layout_iters",
                        help="number of iterations to be used in generating the graph layout",
                        type=int,
                        default=35)
    parser.add_argument("-k",
                        help="the desired distance between each node",
                        type=float,
                        default=0.25)
    parser.add_argument("-directed",
                        help="whether or not the graph is directed",
                        action='store_true',
                        default=False)
    parser.add_argument("-output", '-out_dir',
                        help="the directory to place the graph files in",
                        type=str)
    parser.add_argument("-node_attribute_lists", '-node_attributes', '-node_atts', '-node_attrs',
                        help="1 or more files (comma separated list) containing file(s) for logging node attributes",
                        type=str)
    parser.add_argument("-edge_attribute_lists", '-edge_atts',
                        help="this is the leading string for generating the saved graph, as well as the images",
                        type=str)
    parser.add_argument("-show_intermediates",
                        help="NOT YET SUPPORTED - whether or not to show the intermediate graphs while the "
                             "layout algorithm is iterating",
                        type=bool)
    parser.add_argument("-verbose", '-v',
                        help="whether or not to show some verbose trouble shooting lines",
                        action='store_true')
    parser.add_argument("-min_comp_size",
                        help="the minimum size of a component for plotting",
                        type=int,
                        default=10)
    parser.add_argument("-dpi",
                        help="dots per inch for plotting networks",
                        type=int,
                        default=360)
    parser.add_argument("-plot_all",
                        help="if you want to plot all of the attributes",
                        default=False,
                        action='store_true')
    parser.add_argument("-dont_replot",
                        help="if you want to re-plot the attributes, but don't want to, including those "
                             "which were already plotted before in the output directory",
                        default=True,
                        action='store_false',
                        dest='replot')
    parser.add_argument("-float_color_range", '-range', '-color_range', '-col_range',
                        help="a comma delimited range of two floats which give the low and high end for "
                             "showing expression levels. This can be used to enhance contrast of an image.",
                        type=str)
    args = parser.parse_args()
    return args


def get_id_list_if_none(adj_list):
    # if we don't have any id list given as an argument, we'll generate the id_list from the adj list
    id_list = []
    first = True
    for file_line in fileinput.input(adj_list):
        if first:
            first = False
        else:
            temp_line = strip_split(file_line)
            id_list += temp_line[:2]
    return sorted(list(set(id_list)))


def process_node(temp_node):
    try:
        float(temp_node)
    except ValueError:
        return temp_node
    else:
        return str(float(temp_node))


def get_color_from_var(in_attr_dict, large_comp_g, min_express, max_express):
    if is_categorical(in_attr_dict):
        return get_vals_from_dict(get_color_from_cat(in_attr_dict, min_express, max_express), large_comp_g)
    else:
        return get_vals_from_dict(get_color_from_float(in_attr_dict, min_express, max_express), large_comp_g)


def get_vals_from_dict(val_map, large_comp_g):
    return [val_map[node] for node in large_comp_g.nodes()]


def get_color_from_cat(in_attr_dict, min_express, max_express):
    all_vals = []
    for n in in_attr_dict.keys():
        all_vals.append(in_attr_dict[n])
    unique_entries = list(set(all_vals))
    num_vars = len(unique_entries)
    cat_to_float_dict = {}
    for entry in enumerate(unique_entries):
        cat_to_float_dict[entry[1]] = entry[0] / num_vars

    # update the in_attr_dict to be float rather than cat
    for n in in_attr_dict.keys():
        in_attr_dict[n] = cat_to_float_dict[in_attr_dict[n]]

    return get_color_from_float(in_attr_dict, min_express, max_express)


def is_categorical(in_attr_dict):
    """first we need to figure out if it's a float or a categorical variable
    the first sign that we have a categorical var is if there are strings
    in the attribute dict"""
    is_categorical_bool = False
    all_vals = []
    for n in in_attr_dict.keys():
        all_vals.append(in_attr_dict[n])
        if isinstance(in_attr_dict[n], str):
            is_categorical_bool = True

    # now we can check how variant the vector is
    # if there are not that many unique values compared to the number of nodes
    # then it's probably categorical
    # by default this is set to 10%
    # num_unique = len(list(set(all_vals)))
    # if number_of_nodes(G)/10 > num_unique:
    #    is_categorical_bool = True

    return is_categorical_bool


def get_color_from_float(in_attr_dict, min_express, max_express):
    if min_express is not None:
        logger.info('setting mins to: {}'.format(min_express))
        logger.info('setting maxs to: {}'.format(max_express))
        for n in in_attr_dict.keys():
            if in_attr_dict[n] > max_express:
                in_attr_dict[n] = max_express
            elif in_attr_dict[n] < min_express:
                in_attr_dict[n] = min_express

    # do linear normalization between 0 and 1
    # get the min

    temp_min = None
    for n in in_attr_dict.keys():
        if temp_min is None:
            temp_min = in_attr_dict[n]
        else:
            if temp_min > in_attr_dict[n]:
                temp_min = in_attr_dict[n]
    # subtract min
    for n in in_attr_dict.keys():
        in_attr_dict[n] = in_attr_dict[n] - temp_min

    # get the max
    temp_max = None
    for n in in_attr_dict.keys():
        if temp_max is None:
            temp_max = in_attr_dict[n]
        else:
            if temp_max < in_attr_dict[n]:
                temp_max = in_attr_dict[n]
    # divide max
    for n in in_attr_dict.keys():
        if temp_max == 0:
            in_attr_dict[n] = 0
        else:
            in_attr_dict[n] = in_attr_dict[n] / temp_max

    return in_attr_dict


def get_attrs(large_comp_g):
    # get the names of all attributes
    # TODO: this function is really inefficient
    first_node = None
    for i in iter(large_comp_g.node.items()):
        first_node = i[1]
        break
    return list(first_node.keys())


def main():
    args = parse_arguments()

    if not args.output.endswith('/'):
        args.output += '/'

    plot_network(rand_seed=args.rand_seed,
                 id_list=args.id_list,
                 float_color_range=args.float_color_range,
                 adj_list=args.adj_list,
                 output=args.output,
                 node_color=args.node_color,
                 plot_all=args.plot_all,
                 verbose=args.verbose,
                 dpi=args.dpi,
                 no_default_plot=args.no_default_plot,
                 node_attribute_lists=args.node_attribute_lists,
                 replot=args.replot,
                 color_scheme=args.color_scheme,
                 redo_layout=args.redo_layout,
                 layout_iters=args.layout_iters,
                 k=args.k,
                 directed=args.directed)


def plot_network(id_list,
                 adj_list,
                 float_color_range,
                 output,
                 verbose,
                 node_attribute_lists,
                 graph_param=None,
                 node_color=None,
                 rand_seed=12345678,
                 replot=True,
                 plot_all=False,
                 dpi=360,
                 no_default_plot=False,
                 color_scheme="bwr",
                 layout="spring",
                 redo_layout=False,
                 layout_iters=35,
                 k=0.25,
                 directed=False):
    np.random.seed(rand_seed)
    random.seed(rand_seed)
    pos = None
    min_express = None
    max_express = None
    if float_color_range is not None:
        if ',' not in float_color_range:
            sys.exit('float_color_range must be comma delimited')
        express_range = float_color_range.split(',')
        express_range[0] = float(express_range[0])
        express_range[1] = float(express_range[1])
        min_express = min(express_range)
        max_express = max(express_range)
    if adj_list is None and graph_param is None:
        g = nx.lollipop_graph(4, 6)
    else:
        if graph_param is not None:
            # read in the input graph
            in_graph = import_dict(graph_param)
            g = in_graph['graph']
            id_list = g.nodes()
            if 'positions' in in_graph:
                pos = in_graph['positions']
        else:
            # read in the IDs for setting up the layout of the adj matrix
            if id_list is None:
                id_list = get_id_list_if_none(adj_list)
            else:
                id_list = read_file(id_list, "lines")
            g = nx.DiGraph() if directed else nx.Graph()
            ID_hash = {}

            # add the nodes to the graph
            logger.info('adding the nodes to the graph')
            for i in range(0, len(id_list)):
                ID_hash[id_list[i]] = i
                g.add_node(id_list[i])

            # go through the adj_list and add the edges
            logger.info('adding the edges to the graph')
            num_edge = 0
            first = True
            for fi_line in fileinput.input(adj_list):
                if first:
                    first = False
                else:
                    num_edge += 1
                    temp_line = strip_split(fi_line)
                    g.add_edge(temp_line[0], temp_line[1])
    min_comp_size = len(id_list) * 0.1

    # get the comminities based on the louvain method described in:
    # Fast unfolding of communities in large networks,
    # Vincent D Blondel, Jean-Loup Guillaume, Renaud Lambiotte, Renaud Lefebvre,
    # Journal of Statistical Mechanics: Theory and Experiment 2008(10), P10008
    if COMMUNITY_INSTALLED:
        logger.info('finding the communities')
        if 'community' not in g:
            partition_table = None
            try:
                partition = community.best_partition(g)
            except Exception:
                logger.warning("the community was not found.")
            else:
                pr = nx.pagerank(g)  # ,nstart=0.15)
                partition_table = [['ID', 'community', 'PageRank']]
                for i in id_list:
                    partition[i] = 'community_' + str(partition[i])
                    partition_table.append([i, partition[i], pr[i]])

                try:
                    nx.set_node_attributes(g, pr, name="PageRank")
                except Exception:
                    nx.set_node_attributes(g, 'PageRank', pr)
                try:
                    nx.set_node_attributes(g, partition, name="community")
                except Exception:
                    nx.set_node_attributes(g, 'community', partition)

            if partition_table:
                write_table(partition_table, output + 'communities.txt')

    logger.info('finding the connected components')
    comps = nx.connected_components(g)
    # figure out which nodes to plot based on the connected component size
    big_comp_nodes = []
    small_comp_nodes = []
    for i in comps:
        if len(i) >= min_comp_size:
            big_comp_nodes += i
        else:
            small_comp_nodes += i
    logger.info('{} nodes were found in the components larger than {}'.format(len(big_comp_nodes), min_comp_size))
    large_comp_g = g.subgraph(big_comp_nodes)

    # calculate the positions based on the given layout ########
    if not redo_layout:  # graph is None or graph_positons is None:
        logger.info('calculating the node positions in the graph (this could take a while)')
        layout_function_dict = {'spring': nx.spring_layout, 'spectral': nx.spectral_layout,
                                'circle': nx.circular_layout, 'random': nx.random_layout,
                                'shell': nx.shell_layout}

        layout_alg = layout_function_dict[layout]
        if layout == 'spring':
            pos = layout_alg(large_comp_g, iterations=layout_iters, k=k)
        else:
            pos = layout_alg(large_comp_g)

    # import node attributes
    empty_node_dict = {}
    all_nodes = g.nodes()
    for n in all_nodes:
        empty_node_dict[n] = 0
    if node_attribute_lists is not None:
        list_of_na_files = node_attribute_lists.split(',')
        for f in list_of_na_files:
            temp_attr_table = read_table(f)
            for i in range(1, len(temp_attr_table[0])):
                temp_attr_name = temp_attr_table[0][i]
                temp_attr_name = temp_attr_name.replace('/', '|')
                logger.info('setting the node attribute: {}'.format(temp_attr_name))
                temp_attr_dict = empty_node_dict.copy()
                logger.debug(list(temp_attr_dict.keys())[:5])
                for n in range(1, len(temp_attr_table)):
                    temp_node = process_node(temp_attr_table[n][0])
                    if temp_node in temp_attr_dict:
                        temp_attr_dict[temp_node] = temp_attr_table[n][i]
                    else:
                        # this means that we couldn't find the node from the
                        # attr list in the graph
                        if verbose:
                            logger.debug("{} not in temp_attr_dict".format(temp_node))

                try:
                    logger.debug("temp_attr_name {}".format(temp_attr_name))
                    nx.set_node_attributes(g, temp_attr_dict, name=temp_attr_name)
                except Exception:
                    nx.set_node_attributes(g, temp_attr_name, temp_attr_dict)
    # node and edge sizes
    node_size = 2.5
    edge_width = 2.5 / np.sqrt(nx.number_of_edges(g))
    # plot the graph
    # select some random edges to draw so that it can do it in a reasonable amnt of time
    try:
        all_edges = list(large_comp_g.edges)
    except:
        all_edges = list(large_comp_g.edges())
    num_edge = int(1e6)
    if len(all_edges) > num_edge:
        new_order = all_edges[:]
        np.random.shuffle(all_edges)
        subset_edges = new_order[:num_edge]
    else:
        subset_edges = all_edges
    if not no_default_plot:
        plt.clf()
        logger.info('plotting the graph')
        nx.draw(large_comp_g, pos, node_shape='o', linewidths=0, node_size=node_size, width=edge_width, font_size=0)
        plt.draw()
        if output is not None:
            # save the image
            if output[-1] != '/':
                output += '/'
            plt.savefig(output + 'full_graph.png', dpi=dpi, bbox_inches='tight')
            plt.clf()
        else:
            plt.show()
    # either save or display the graph(s)
    if output is not None:
        if node_color is not None or plot_all:
            # if it's a big graph, we'll only plot a subset of the edges
            # Not implemented yet
            # all_edges = list(large_comp_G.edges())
            # max_edges = int(min([1e5,len(all_edges)]))
            # sample_edges = random.sample(all_edges,max_edges)
            # plot the other node colors
            plt.clf()
            node_color_list = None
            if node_color is not None:
                node_color_list = node_color.split(',')
            if plot_all:
                node_color_list = get_attrs(large_comp_g)
            for nc in node_color_list:
                if replot or not os.path.isfile(output + nc + '.png'):
                    plt.clf()
                    logger.info('plotting the {} graph'.format(nc))
                    values = get_color_from_var(nx.get_node_attributes(large_comp_g, nc), large_comp_g,
                                                max_express=max_express, min_express=min_express)
                    if is_categorical(nx.get_node_attributes(large_comp_g, nc)):
                        logger.info('categorical colours set')
                        temp_colors = 'gist_rainbow'  # 'pipy_spectral'#'Set1'
                    else:
                        logger.info("non-categorical colours set")
                        temp_colors = color_scheme

                    nx.draw(large_comp_g, pos, cmap=plt.get_cmap(temp_colors),
                            node_color=values, node_shape='o', linewidths=0,
                            node_size=node_size, width=edge_width, font_size=0,
                            nodelist=list(large_comp_g.nodes()),
                            edgelist=subset_edges)
                    # save the image
                    logger.info('{}'.format(output + nc + '.png'))
                    plt.savefig(output + nc + '.png',
                                dpi=dpi,
                                bbox_inches='tight')
                else:
                    if os.path.isfile(output + nc + '.png'):
                        logger.info('{}, already exists - skipping'.format(output + nc + '.png'))
    # save the graphs
    if output is not None:
        logger.info('saving the graph')
        save_dict({'graph': large_comp_g, 'positions': pos}, output + 'large_comps.graphpkl')
        save_dict({'graph': g}, output + 'full_graph_full.graphpkl')


if __name__ == "__main__":
    main()
