import sys
import os
import argparse
import h5py
import logging

import numpy as np
from numpy import array, empty
from scipy.stats.mstats import f_oneway as aov

from bin.pyminer_gprofile import pyminer_gprofile
from modules.common_functions import read_file, make_file, get_file_path, read_table, write_table, run_cmd
from modules.references import HSAPIENS

from multiprocessing import Queue, Process
from modules.multiprocessing.generic_process import TERMINATOR, process_monitor_w_output, \
    LockedInteger, LockedBoolean
from bin.anova_process import AnovaProcess

logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    # global arguments
    parser.add_argument(
        '-infile', '-in', '-i', '-input',
        dest='infile',
        type=str,
        required=True)

    parser.add_argument(
        "-sample_groups",
        help='if you know how which samples belong to which groups, feed in a file that has the samples '
             'in the first column, and their group number (index starting at 0), in the second column. The '
             'IDs must be in the same order as in the infile too.',
        dest='sample_groups',
        type=str)

    parser.add_argument(
        "-classes",
        help='if there are classes to compare, put the annotation table in this argument',
        dest='class_file',
        type=str)

    parser.add_argument(
        "-within_group",
        help='if you know how which samples belong to which groups, feed in a file that has the samples in '
             'the first column, and their group number (index starting at 0), in the second column. The IDs '
             'must be in the same order as in the infile too.',
        dest='within_group',
        type=int)

    parser.add_argument(
        "-process_count",
        help='multiprocessing sections of the app can spin up multiple processes to accellerate processing '
             'time - set this to the number of free CPUs available.',
        dest='process_count',
        type=int,
        default=1)

    parser.add_argument(
        "-out_dir", "-o", "-out",
        help='if you know how which samples belong to which groups, feed in a file that has the samples in '
             'the first column, and their group number (index starting at 0), in the second column. The IDs '
             'must be in the same order as in the infile too.',
        dest='out_dir',
        type=str)

    parser.add_argument(
        "-species", "-s",
        help='what species is this? Must be gProfiler compatible.',
        dest='species',
        type=str,
        default=HSAPIENS)

    parser.add_argument(
        '-no_gProfile',
        help='should we do the automated gprofiler results?',
        default=False,
        action='store_true')

    parser.add_argument(
        "-FDR", "-fdr", "-fdr_cutoff",
        help='The desired Benjamini-Hochberg False Discovery Rate (FDR) for multiple comparisons '
             'correction (default = 0.05)',
        dest='FDR_cutoff',
        type=float,
        default=0.05)

    parser.add_argument(
        "-Zscore", "-Z_score_cutoff", "-Z", "-zscore", "-z",
        help='The desired False Discovery Rate (FDR) for multiple comparisons correction (default = 0.05)',
        dest='Zscore',
        type=float,
        default=2.0)

    parser.add_argument(
        '-hdf5',
        help='The input file is an HDF5 file',
        default=False,
        action='store_true')

    parser.add_argument(
        "-ID_list", "-ids",
        help='If we are using an hdf5 file, give the row-wise IDs in this new line delimeted file',
        type=str)

    parser.add_argument(
        "-columns", "-cols",
        help='If we are using an hdf5 file, give the column-wise IDs in this new line delimeted file',
        type=str)

    parser.add_argument(
        '-rows',
        help='if the samples are in rows, and variables are in columns',
        default=False,
        action='store_true')

    parser.add_argument(
        "-log", '-log2', '-log_transform',
        help='do a log transformation prior to clustering',
        action='store_true',
        default=False)

    parser.add_argument(
        '-lin_norm',
        help='should we normalize the rows before doing the stats?',
        default=False,
        action='store_true')

    args = parser.parse_args()
    return args


def lin_norm_rows(in_mat):
    in_mat = np.transpose(np.array(in_mat))
    in_mat = in_mat - np.min(in_mat, axis=0)
    in_mat = in_mat / np.max(in_mat, axis=0)
    in_mat[np.isnan(in_mat)] = 0
    return np.transpose(in_mat)


def correct_pvalues_for_multiple_testing(pvalues, correction_type="Benjamini-Hochberg"):
    """

    this function was adopted from emre's stackoverflow answer found here:
    https://stackoverflow.com/questions/7450957/how-to-implement-rs-p-adjust-in-python

    consistent with R - print
    correct_pvalues_for_multiple_testing([0.0, 0.01, 0.029, 0.03, 0.031, 0.05, 0.069, 0.07, 0.071, 0.09, 0.1])
    """

    pvalues = array(pvalues)
    n = int(pvalues.shape[0])
    new_pvalues = empty(n)
    if correction_type == "Bonferroni":
        new_pvalues = n * pvalues
    elif correction_type == "Bonferroni-Holm":
        values = [(pvalue, i) for i, pvalue in enumerate(pvalues)]
        values.sort()
        for rank, vals in enumerate(values):
            pvalue, i = vals
            new_pvalues[i] = (n - rank) * pvalue
    elif correction_type == "Benjamini-Hochberg":
        values = [(pvalue, i) for i, pvalue in enumerate(pvalues)]
        values.sort()
        values.reverse()
        new_values = []
        for i, vals in enumerate(values):
            rank = n - i
            pvalue, index = vals
            new_values.append((n / rank) * pvalue)
        for i in range(0, int(n) - 1):
            if new_values[i] < new_values[i + 1]:
                new_values[i + 1] = new_values[i]
        for i, vals in enumerate(values):
            pvalue, index = vals
            new_pvalues[index] = new_values[i]
    return new_pvalues


def main():
    args = parse_arguments()

    get_stats(infile=args.infile,
              sample_dir=args.out_dir,
              sample_groups=args.sample_groups,
              class_file=args.class_file,
              hdf5=args.hdf5,
              within_group=args.within_group,
              id_list=args.ID_list,
              columns=args.columns,
              rows=args.rows,
              log=args.log,
              lin_norm=args.lin_norm,
              fdr_cutoff=args.FDR_cutoff,
              zscore=args.Zscore,
              no_g_profile=args.no_gProfile,
              species=args.species,
              process_count=args.process_count)


def get_stats(infile,
              sample_groups,
              sample_dir,
              id_list,
              columns,
              within_group=None,
              class_file=None,
              hdf5=False,
              rows=False,
              log=False,
              lin_norm=False,
              fdr_cutoff=0.05,
              zscore=2.0,
              no_g_profile=False,
              species=HSAPIENS,
              process_count=1):
    alias_dict = None
    subset_col_idxs = None

    infile = os.path.realpath(infile)
    if sample_dir is None:
        sample_dir = get_file_path(infile) + 'sample_clustering_and_summary/'
    if not os.path.isdir(sample_dir):
        os.mkdir(sample_dir)
    # read in the dataset and sample groups
    if within_group is None:
        sample_group_table = read_table(sample_groups)

        sample_group_table_np = np.array(sample_group_table)
        sample_group_order = np.transpose(sample_group_table_np[:, 0])
        sorted_list_of_ids = list(sample_group_order)

        grouping_vector = list(np.transpose(sample_group_table_np[:, 1]))
    else:
        if class_file is None:
            sys.exit("to run within class analysis, we need the classes annotation file: -classes")
        orig_sample_group_table = read_table(sample_groups)
        orig_sample_group_table_np = np.array(orig_sample_group_table)
        orig_grouping_vector = list(np.transpose(orig_sample_group_table_np[:, 1]))

        # we're going to switch around the normal sample groups with the classes that are read in
        class_table_np = np.array(read_table(class_file))
        class_labels = list(set(class_table_np[:, 1].tolist()))
        class_labels = sorted(class_labels)
        class_hash = {key: value for value, key in enumerate(class_labels)}
        alias_dict = {key: value for key, value in enumerate(class_labels)}

        # make a table to define which class is which group
        class_index_table = []
        for c in class_labels:
            class_index_table.append([c, class_hash[c]])

        # figure out which indices to keep
        subset_col_idxs = []
        for idx, g_vect in enumerate(orig_grouping_vector):
            if str(g_vect) == str(float(within_group)):
                subset_col_idxs.append(idx)
        sample_group_table_np = class_table_np[subset_col_idxs]

        # go through the annotation table and make
        for i in range(len(subset_col_idxs)):
            sample_group_table_np[i, 1] = class_hash[sample_group_table_np[i, 1]]

        sample_group_table = sample_group_table_np.tolist()
        for i in range(len(sample_group_table)):
            sample_group_table[i][1] = float(sample_group_table[i][1])
        sample_group_table_np = np.array(sample_group_table)
        sample_group_order = np.transpose(sample_group_table_np[:, 0])
        sorted_list_of_ids = list(sample_group_order)
        grouping_vector = list(np.transpose(sample_group_table_np[:, 1]))
        subset_col_idxs = np.array(subset_col_idxs, dtype=int)
    if len(list(set(grouping_vector))) == 1:
        sys.exit('only one sample group, nothing to calculate')
    else:
        one_group = False
    sample_cluster_ids = []
    for sample_g in sample_group_table:
        # THIS IS IMPORTANT
        # here we assume that the samples are all listed in the same order as in '-infile'
        # we also assume that the group indexing starts at 0
        sample_cluster_ids.append(int(sample_g[1]))
    sample_k_lists = [[] for _ in range(max(sample_cluster_ids) + 1)]
    # now populate the list of lists
    for i, sample_c in enumerate(sample_cluster_ids):
        # this appends the sample index to
        sample_k_lists[sample_c].append(i)
    # check for formatting
    if not hdf5:
        full_expression_str = read_table(infile)
        title = full_expression_str[0]
        full_expression_np = np.array(full_expression_str)
        row_names = full_expression_np[1:, 0]
        full_expression = np.array(full_expression_np[1:, 1:], dtype=float)
    else:
        row_names = read_file(id_list, 'lines')
        title = read_file(columns, 'lines')
        logger.info('making a maliable hdf5 file to preserve the original data')
        run_cmd(['cp', infile, infile + '_copy'])

        logger.inf('reading in hdf5 file')
        infile_path = infile + '_copy'
        h5f = h5py.File(infile_path, 'r+')
        full_expression = h5f["infile"]

    # if we're doing the subset analysis, subset which columns to include
    if within_group is not None:
        full_expression = full_expression[:, subset_col_idxs]
        temp_title = np.array(title[1:])
        title = [title[0]] + temp_title[subset_col_idxs].tolist()
        logger.debug(row_names)

    if rows:
        full_expression = np.transpose(full_expression)
        temp_col = ["variables"] + row_names.tolist()
        title = temp_col[:]
    if title[1:] != sorted_list_of_ids:
        logger.info('length of columns {}'.format(len(title[1:])))
        logger.info('length of sample_groups {}'.format(sorted_list_of_ids))
        for i in range(1, len(title)):
            if title[i] != sorted_list_of_ids[i - 1]:
                logger.debug('on line {} ID_list {} {}'.format(i, title[i], sorted_list_of_ids[i - 1]))
        sys.exit("the sample group ids have to be in the same order as in the input matrix")
    # make all of the nans equal to zero
    full_expression[np.isnan(full_expression)] = 0.0
    if log:
        full_expression = np.log2(full_expression - np.min(full_expression) + 1)
    if lin_norm:
        full_expression = lin_norm_rows(full_expression)
    id_list = list(row_names)

    # calculate the sample level Z-scores
    # first normalize the expression matrix linearly between 1 and 2
    # sample_var_enrichment = lin_norm_rows(full_expression) + 1
    if not hdf5:
        sample_enrichment_zscores(id_list, full_expression, sample_dir, title)

    # go through each group and generate mean and sd summaries, then write to file
    list_of_k_sample_indices = sample_k_lists
    sample_k_group_enrichment_numeric = None
    if not one_group:
        list_of_k_sample_indices, row_names, sample_groups, sample_k_group_enrichment_numeric = k_group_enrichment(
            id_list, alias_dict, full_expression, list_of_k_sample_indices, sample_dir)

    # do a one way anova between groups to test for variables that are significantly different between them ######
    all_sig_enriched_files = []
    all_sig_enriched_gprof_files = []
    fdr_cutoff = fdr_cutoff
    zscore_cutoff = zscore
    if not one_group:
        annova(id_list, all_sig_enriched_files, all_sig_enriched_gprof_files, fdr_cutoff, full_expression,
               list_of_k_sample_indices, sample_dir, row_names, sample_groups, sample_k_group_enrichment_numeric,
               zscore_cutoff, process_count=process_count)

    background = sample_dir + 'significance/background_ids.txt'
    make_file('\n'.join(id_list), background)
    if not no_g_profile:
        if not os.path.isdir(sample_dir + 'significance/gprofiler/'):
            os.mkdir(sample_dir + 'significance/gprofiler/')

        for file_num, enriched_file in enumerate(all_sig_enriched_files):
            pyminer_gprofile(infile=enriched_file,
                             outfile=all_sig_enriched_gprof_files[file_num],
                             species=species,
                             background=background)


def sample_enrichment_zscores(id_list, full_expression, sample_dir, title):
    sample_var_enrichment = full_expression
    # then calculate the mean for each variable
    norm_row_means = np.transpose(np.array([np.mean(sample_var_enrichment, axis=1)]))
    logger.debug(norm_row_means)
    # then calculate the sd for each variable
    norm_row_sd = np.transpose(np.array([np.std(sample_var_enrichment, axis=1)]))
    logger.debug(norm_row_sd)
    # then calculate the delta for each variable
    norm_row_delta = sample_var_enrichment - norm_row_means
    # calculate the z-score (ie: how many standard deviations away from the mean is each sample)
    sample_var_enrichment_numeric = norm_row_delta / norm_row_sd
    logger.debug(sample_var_enrichment_numeric)
    # add the titles
    sample_var_enrichment = np.array([title[1:]] + sample_var_enrichment_numeric.tolist()).tolist()
    temp_ids = ['variables'] + id_list
    for i in range(0, len(sample_var_enrichment)):
        sample_var_enrichment[i] = [temp_ids[i]] + sample_var_enrichment[i]
    # sample_var_enrichment = np.hstack((row_titles,sample_var_enrichment))
    write_table(sample_var_enrichment, sample_dir + 'sample_var_enrichment_Zscores.txt')


def k_group_enrichment(id_list, alias_dict, full_expression, list_of_k_sample_indices, sample_dir):
    list_of_k_sample_indices = np.array(list_of_k_sample_indices)
    # initialize mean and sd output tables
    # calculate the means accross rows
    all_sample_mean = np.transpose(np.array([np.mean(full_expression, axis=1)]))
    all_sample_sd = np.transpose(np.array([np.std(full_expression, axis=1)]))
    k_group_means = np.transpose(np.array([np.mean(full_expression[:, list_of_k_sample_indices[0]], axis=1)]))
    k_group_sd = np.transpose(np.array([np.std(full_expression[:, list_of_k_sample_indices[0]], axis=1)]))
    for k in range(1, len(list_of_k_sample_indices)):
        # calc row means
        new_mean_col = np.transpose(np.array([np.mean(full_expression[:, list_of_k_sample_indices[k]], axis=1)]))
        k_group_means = np.hstack((k_group_means, new_mean_col))

        # calc sd
        new_sd_col = np.transpose(np.array([np.std(full_expression[:, list_of_k_sample_indices[k]], axis=1)]))
        k_group_sd = np.hstack((k_group_sd, new_sd_col))
    # use the means of the k-means groups to calculate the enrichment
    # calculate the sample group z-scores ((Xbar-Mu)/sigma) * sqrt(n), or (Xbar-Mu)/(sigma/sqrt(n))
    logger.info("calculating the sample group vs all sample means")
    logger.info("np.shape(k_group_means): {}".format(np.shape(k_group_means)))
    logger.info("np.shape(all_sample_mean) {}".format(np.shape(all_sample_mean)))
    row_delta = np.array(k_group_means.transpose() - all_sample_mean.transpose()).transpose()
    logger.debug(np.shape(row_delta))
    for k in range(0, len(list_of_k_sample_indices)):
        row_delta[:, k] = np.array(row_delta[:, k].transpose() / all_sample_sd[:, 0].transpose()).transpose()
        row_delta[:, k] = row_delta[:, k] * np.sqrt(len(list_of_k_sample_indices[k]))
    sample_k_group_enrichment = row_delta
    sample_k_group_enrichment_numeric = sample_k_group_enrichment
    logger.debug(np.shape(sample_k_group_enrichment))
    logger.info('sample_k_group_enrichment {}'.format(sample_k_group_enrichment))
    sample_groups = []
    for k in range(0, len(list_of_k_sample_indices)):
        if alias_dict:
            temp_sample_group_name = alias_dict[k]
        else:
            temp_sample_group_name = 'sample_group_' + str(k)
        sample_groups.append(temp_sample_group_name)
    sample_groups = np.array(sample_groups)
    logger.debug(np.shape(sample_groups))
    k_group_means = np.vstack((sample_groups, k_group_means))
    k_group_sd = np.vstack((sample_groups, k_group_sd))
    sample_k_group_enrichment = np.vstack((sample_groups, sample_k_group_enrichment))
    # prepare the k-means enrichment for writing to file, then write it
    # these are the names of the variables, making the row labels for the output table
    row_names = np.transpose(np.array([['var_names'] + id_list]))
    k_group_means = np.hstack((row_names, k_group_means))
    k_group_sd = np.hstack((row_names, k_group_sd))
    sample_k_group_enrichment = np.hstack((row_names, sample_k_group_enrichment))
    write_table(k_group_means, sample_dir + 'k_group_means.txt')
    write_table(k_group_sd, sample_dir + 'k_group_sd.txt')
    write_table(sample_k_group_enrichment, sample_dir + 'k_group_enrichment.txt')
    logger.debug('sample_k_group_enrichment')
    logger.debug(sample_k_group_enrichment)
    logger.debug(sample_k_group_enrichment[1:, :][:, 1:])
    return list_of_k_sample_indices, row_names, sample_groups, sample_k_group_enrichment_numeric


def multiprocess_annova(id_list, full_expression, list_of_k_sample_indices,  process_count=1):

    # go through each variable and get the uncorrected anova stats

    input_queue = Queue(len(id_list) + process_count + 1)
    output_queue = Queue(100)
    error_queue = Queue(100)
    terminator_obj = LockedInteger()
    death_obj = LockedBoolean()
    process_list = []

    logger.info("spawning {} processes to process Anova".format(process_count))

    for process_idx in range(process_count):
        g_process = AnovaProcess(
            input_queue=input_queue,
            output_queue=output_queue,
            error_queue=error_queue,
            terminator_obj=terminator_obj,
            death_obj=death_obj,
            terminators_expected=process_count,
            process_name="generic process",
            full_expression=full_expression,
            list_of_k_sample_indices=list_of_k_sample_indices

        )
        proc = Process(target=g_process.run)
        proc.daemon = True
        proc.start()

        process_list.append(proc)

    for i in range(0, len(id_list)):
        input_queue.put(i)

    for _ in range(process_count):
        input_queue.put(TERMINATOR)

    logger.info("starting Process Monitor.")
    output = process_monitor_w_output(error_queue=error_queue, process_list=process_list, output_queue=output_queue)
    logger.info("multiprocessing completed.")

    anova_output = [0.0] * len(id_list)   # list of floats
    for row in output:
        logger.info("row = {}".format(row))
        p = row[1][-1]
        index = int(row[0])
        try:
            anova_output[index] = float(p)
        except ValueError:
            anova_output[index] = 1
    return anova_output


def annova(id_list, all_sig_enriched_files, all_sig_enriched_gprof_files, fdr_cutoff, full_expression,
           list_of_k_sample_indices, out_dir, row_names, sample_groups, sample_k_group_enrichment_numeric,
           zscore_cutoff, process_count=1):
    logger.info('doing the 1-way ANOVAs:')
    logger.debug("Runninng annova with array shape".format(full_expression.shape))

    if process_count > 1:
        anova_output = multiprocess_annova(id_list, full_expression, list_of_k_sample_indices,
                                           process_count=process_count)
        # replace nans with 1s
        logger.debug("first 10 anova values, uncorrected")
        logger.debug(anova_output[:10])
        # correct the p-values with Benjamini-Hochberg
        bh_corrected_aov = correct_pvalues_for_multiple_testing(anova_output)
        logger.debug("first 10 anova values, corrected")
        logger.debug(bh_corrected_aov[:10])
        indices_less_than_fdr_cutoff = []
        anova_to_write = []
        for i, id_val in enumerate(id_list):
            anova_to_write.append([id_val, anova_output[i], bh_corrected_aov[i], -1 * np.log10(bh_corrected_aov[i])])
            if bh_corrected_aov[i] <= fdr_cutoff:
                indices_less_than_fdr_cutoff.append(i)

        if not os.path.isdir(out_dir + 'significance'):
            os.mkdir(out_dir + 'significance')

        anova_header = [['Variable', 'uncorrected_p-val', 'BH_corrected_p-val', '-log10(BH_cor_p-val)']]
        write_table(anova_header + anova_to_write, out_dir + 'significance/groups_1way_anova_results.txt')
    else:
        anova_output = [['Variable', 'F', 'uncorrected_p-val', 'BH_corrected_p-val', '-log10(BH_cor_p-val)']]
        aov_uncorrected_p_val_list = []

        # go through each variable and get the uncorrected anova stats
        for i, id_val in enumerate(id_list):
            if i % 500 == 0:
                logger.info('\t{}'.format(i))
            list_of_group_values = []
            for group in list_of_k_sample_indices:
                list_of_group_values.append(full_expression[i, group])
            temp_aov = list(aov(*list_of_group_values))
            anova_output.append([id_val]+temp_aov)
            aov_uncorrected_p_val_list.append(anova_output[-1][-1])

        # replace nans with 1s
        aov_uncorrected_p_val_list = np.array(aov_uncorrected_p_val_list)
        aov_uncorrected_p_val_list[np.isnan(aov_uncorrected_p_val_list)] = 1

        logger.debug(aov_uncorrected_p_val_list[:10])
        logger.debug(correct_pvalues_for_multiple_testing(aov_uncorrected_p_val_list[:10]))

        # correct the p-values with Benjamini-Hochberg
        bh_corrected_aov = correct_pvalues_for_multiple_testing(aov_uncorrected_p_val_list)
        logger.debug(bh_corrected_aov[:10])

        indices_less_than_fdr_cutoff = []
        for i, _ in enumerate(id_list):
            anova_output[i + 1] += [bh_corrected_aov[i], -1 * np.log10(bh_corrected_aov[i])]
            if bh_corrected_aov[i] <= fdr_cutoff:
                indices_less_than_fdr_cutoff.append(i)

        if not os.path.isdir(out_dir + 'significance'):
            os.mkdir(out_dir + 'significance')
        write_table(anova_output, out_dir + 'significance/groups_1way_anova_results.txt')
    # after this point the two anova processes should yeild the same results

    # Find the significantly different & enriched variables for each group
    # we start off with the variables that are significantly different between groups
    # this is a list of lists for each
    group_sig_enriched_bool = []
    for i, _ in enumerate(id_list):
        # check to see if this variable was significant in the BH corrected 1-way anova
        # if it's significant, we want to go in and check which, if any sample groups
        # show an elevated z-score enrichment (greater than the zscore_cutoff variable)
        if i in indices_less_than_fdr_cutoff:
            temp_sig_enriched_bool_list = list(sample_k_group_enrichment_numeric[i, ] >= zscore_cutoff)
        else:
            temp_sig_enriched_bool_list = [False] * len(list_of_k_sample_indices)

        group_sig_enriched_bool.append(temp_sig_enriched_bool_list)
    bool_sample_groups = []
    for s in sample_groups:
        bool_sample_groups.append('bool_' + s)
    bool_sample_groups = np.array(bool_sample_groups)
    group_sig_enriched_bool = np.array(group_sig_enriched_bool)
    group_sig_enriched = np.vstack((bool_sample_groups, group_sig_enriched_bool))
    group_sig_enriched = np.hstack((row_names, group_sig_enriched))
    write_table(group_sig_enriched, out_dir + 'significance/significant_and_enriched_boolean_table.txt')
    # go through each group, and make a list of the variables that are significant and enriched
    num_sig_enriched = []
    for group in range(0, len(list_of_k_sample_indices)):
        sig_and_enriched_for_this_group = []
        for var in range(0, len(id_list)):
            if group_sig_enriched_bool[var, group]:
                sig_and_enriched_for_this_group.append(id_list[var])
        num_sig_enriched.append(len(sig_and_enriched_for_this_group))
        temp_file = out_dir + 'significance/' + sample_groups[group] + '_significant_enriched.txt'
        all_sig_enriched_files.append(temp_file)
        all_sig_enriched_gprof_files.append(
            out_dir + 'significance/gprofiler/' + sample_groups[group] + '_significant_enriched_gprofiler.txt')
        make_file('\n'.join(sig_and_enriched_for_this_group), temp_file)
    num_sig_enriched = np.transpose(np.vstack((np.array([sample_groups]), np.array([num_sig_enriched]))))
    num_sig_enriched = np.vstack(
        (np.array([['sample_group', 'number_of_significant_and_enriched_vars']]), num_sig_enriched))
    write_table(num_sig_enriched,
                out_dir + 'significance/number_of_variables_significant_and_enriched_for_each_group.txt')


if __name__ == "__main__":
    main()
