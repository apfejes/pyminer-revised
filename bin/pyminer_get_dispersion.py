import logging
import argparse
import h5py
import os
from matplotlib import use

use('Agg')

import matplotlib.pyplot as plt
import numpy as np
from sklearn.neighbors import RadiusNeighborsRegressor as neighbors
from statsmodels.nonparametric.smoothers_lowess import lowess
from scipy.interpolate import interp1d

from modules.common_functions import read_file, read_table, write_table

# pylint: disable=C0413

logger = logging.getLogger(__name__)

MAX_SAMPLE_SIZE = 50000


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-infile", "-i",
                        help="the input expression matrix",
                        type=str)
    parser.add_argument("-out_dir", '-out', '-o',
                        help="the directory for output files",
                        type=str)
    parser.add_argument("-log",
                        help="log transform the means",
                        action='store_true',
                        default=False)
    parser.add_argument("-hdf5",
                        help="the input file is an hdf5 file",
                        action='store_true',
                        default=False)
    parser.add_argument("-ids", '-ID_list', '-id_list',
                        help="if an hdf5 file is used, we'll need the list of IDs",
                        type=str)
    parser.add_argument("-z_cutoff", "-z",
                        help="the cutoff for how many standard deviations over the mean "
                             "residual should be considered overdispersed. Default = 0.5",
                        type=float,
                        default=.5)
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    pyminer_get_dispersion(infile=args.infile,
                           id_file=args.ids,
                           out_dir=args.out_dir,
                           z_cutoff=args.z_cutoff,
                           hdf5=args.hdf5)


def pyminer_get_dispersion(infile, id_file, out_dir, z_cutoff=.5, hdf5=False, log=False):
    if not out_dir.endswith('/'):
        out_dir += '/'
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    # read in the input file
    if not hdf5:
        in_mat_str = np.array(read_table(infile))
        in_mat = np.array(in_mat_str[1:, 1:], dtype=float)
        id_list = in_mat_str[1:, 0].tolist()
        del in_mat_str
    else:
        # read in the hdf5 file
        h5f = h5py.File(infile, 'r')
        in_mat = h5f["infile"]
        id_list = read_file(id_file, 'lines')

    if log:
        if np.min(in_mat) < 0.0:
            logger.error("can't log transform with negative values... Bringing the matrix up to min = 1")
            in_mat += min(in_mat)
        in_mat = np.log2(1 + in_mat)

    # calculate the means
    means = np.mean(in_mat, axis=1)
    # calculate the coefficient of variance
    variance = np.std(in_mat, axis=1)  # /(means+1)
    # square the coefficient of variance
    variance *= variance
    # remove the nans
    logger.info("nans found: {}".format(np.sum(np.isnan(variance))))
    variance[np.isnan(variance)] = 0
    logger.info("nans after removal {}".format(np.sum(np.isnan(variance))))
    linear_dim = np.shape(means)
    # plot (a random sample of) the relationship between the coefficient of variance and means

    sample_size = min([MAX_SAMPLE_SIZE, linear_dim[0]])
    if sample_size < MAX_SAMPLE_SIZE:
        logger.info('sampling the distribution for {} points'.format(sample_size))
        index_vect = np.arange(linear_dim[0])
        np.random.shuffle(index_vect)
        sample = index_vect[:sample_size].tolist()
        # always include the min and max values for interpolation
        x_max_index = np.where(np.array(means) == np.max(means))[0][0]
        x_min_index = np.where(np.array(means) == np.min(means))[0][0]
        if x_max_index not in sample:
            sample.append(x_max_index)
        if x_min_index not in sample:
            sample.append(x_min_index)
    else:
        sample = np.arange(linear_dim[0], dtype=int).tolist()
    sample = np.sort(sample)
    # remove the indices where the variance is zero
    keep_sample = np.where(variance[sample] > 0)[0]
    sample = sample[keep_sample]
    plt.clf()
    plt.scatter(means[sample], variance[sample], c='black', s=0.5)  # ,xlab = 'mean',ylab = 'CV')
    # do the lowess fit. This returns the expected values of the variance
    logger.info('calculating the best fit curve')
    neigh = neighbors(radius=1.0, weights='uniform', leaf_size=30)
    mean_max = max(means)
    mean_min = min(means)
    sds_min = min(variance)
    # first calculate the lowess curve on the sample
    lowess_estimates_sample = lowess(variance[sample] - sds_min + 1, means[sample] - mean_min + 1,
                                     delta=0.01 * mean_max) - 1
    # lowess_estimates_sample[:,0] = lowess_estimates_sample[:,0] + mean_min
    # lowess_estimates_sample[:,1] = lowess_estimates_sample[:,1] + sds_min
    # remove the nans
    lowess_estimates_sample[np.isnan(lowess_estimates_sample)] = 0
    logger.info("lowess_estimates_sample {}".format(lowess_estimates_sample))
    # remove x ties from the lowess as this messes up the interpolation
    values, indices = np.unique(lowess_estimates_sample[:, 0], return_index=True)
    lowess_estimates_sample = np.array(lowess_estimates_sample[indices, :])
    logger.info("lowess_estimates_sample {}".format(lowess_estimates_sample))
    logger.debug("lowess_estimates_sample - type {}".format(type(lowess_estimates_sample)))
    # then interpolate the curve to smooth it out and provide the nearest neighbor regression with the needed coverage
    logger.debug("np.shape(lowess_estimates_sample[:, 0]) {}".format(np.shape(lowess_estimates_sample[:, 0])))
    logger.debug("np.shape(lowess_estimates_sample[:, 1]) {}".format(np.shape(lowess_estimates_sample[:, 1])))
    # cross the spread
    logger.debug("original_min_max: {} {}".format(min(lowess_estimates_sample[:, 0]), max(lowess_estimates_sample[:, 0])))
    epsilon = (max(lowess_estimates_sample[:, 0]) - min(lowess_estimates_sample[:, 0])) * 0.01
    logger.info('epsilon: {}'.format(epsilon))
    # make the new x values evenly spaced a
    temp_min = min(lowess_estimates_sample[:, 0])  # - epsilon
    temp_max = max(lowess_estimates_sample[:, 0])  # + epsilon
    new_x = np.linspace(temp_min, temp_max, num=sample_size, endpoint=True)
    interpolation = interp1d(lowess_estimates_sample[:, 0], lowess_estimates_sample[:, 1], kind='cubic')
    logger.info("new_min_max: {} {}".format(min(new_x), max(new_x)))
    logger.debug("new x {}".format(new_x))
    # interpolate the lowess function
    new_y = interpolation(new_x)
    logger.info("new x, new y  {} {}".format(new_x, new_y))
    # get the interplated lowess ready for nearest neighbor regression
    train_x = np.array([[x] for x in lowess_estimates_sample[:, 0].tolist()])
    train_y = lowess_estimates_sample[:, 1]
    full_x = np.array([[x] for x in means.tolist()])
    # train_x = full_x[sample]
    # train_y = variance[sample]
    logger.debug("train_x {}".format(train_x))
    logger.debug("train_y {}".format(train_y))
    # neigh.fit(train_x,train_y)
    logger.debug(new_x)
    logger.debug(new_y)
    new_x = np.array([[x] for x in new_x.tolist()])
    plt.scatter(new_x, new_y, c='red', s=0.75)
    plt.savefig(out_dir + 'local_fit.png', dpi=600, bbox_inches='tight')
    neigh.fit(new_x, new_y)
    # now calculate the values for everything else
    bin_size = 100000
    total_vars = len(means)
    bins = []
    cur_bin = 0
    while cur_bin < total_vars:
        bins.append(min(cur_bin, total_vars))
        cur_bin += bin_size
    bins.append(total_vars)
    logger.info("bins {}".format(bins))
    lowess_estimates = np.zeros((len(means),))
    for i in range(0, total_vars):
        x_dif_vect = np.abs(full_x[i] - new_x)
        closest_idx = int(np.argmin(x_dif_vect))
        lowess_estimates[i] = new_y[closest_idx]
    # calculate the residuals
    logger.info('getting the residuals')
    residuals = variance - lowess_estimates
    logger.info('sample_means {}'.format(means[:10]))
    logger.info('sample_estimates {}'.format(lowess_estimates[:10]))
    logger.info('sample_CVs {}'.format(variance[:10]))
    logger.info('sample_residuals {}'.format(residuals[:10]))
    # reshape it back into the table
    resid_table = residuals
    logger.info("sum of nans {}".format(np.sum(np.isnan(residuals))))
    nan_idx = np.where(np.isnan(residuals))[0]
    logger.info('nan means {}'.format(means[nan_idx]))
    logger.info('nan sds {}'.format(variance[nan_idx]))
    logger.info('nan estimates {}'.format(lowess_estimates[nan_idx]))
    logger.info('nan resid {}'.format(residuals[nan_idx]))
    st_dev_resid = np.std(residuals)
    logger.info('sd residuals: {}'.format(st_dev_resid))
    st_dev_cutoff = st_dev_resid * z_cutoff
    logger.info('residual cutoff for overdispersion: {}'.format(st_dev_cutoff))
    # plot the residuals
    plt.clf()
    plt.scatter(means[sample], residuals[sample], c='black', s=0.5)  # ,xlab = 'mean',ylab = 'CV')
    plt.plot([min(means), max(means)], [0, 0], c='red')
    # find the significant residuals
    resid_sample = residuals[sample]
    mean_sample = means[sample]
    sig_indices = np.where(resid_sample >= st_dev_cutoff)[0]
    plt.scatter(mean_sample[sig_indices], resid_sample[sig_indices], c='blue', s=2)
    plt.savefig(out_dir + 'residuals.png', dpi=600, bbox_inches='tight')
    # determine which genes are locally overdispersed
    logger.info('calculating local overdispersion')
    local_overdispersion_bool_table = resid_table >= st_dev_cutoff
    logger.info('number of overdispersed genes:')
    number_overdispersed_per_group = np.sum(local_overdispersion_bool_table, axis=0)
    logger.info(number_overdispersed_per_group)
    # get the boolean overdispersed table ready for writing to file
    local_overdispersion_bool_table = np.array(local_overdispersion_bool_table, dtype=str)
    local_overdispersion_bool_table = local_overdispersion_bool_table.tolist()
    logger.debug("{} {}".format(local_overdispersion_bool_table[:5], len(local_overdispersion_bool_table)))
    logger.info("means shape {}".format(np.shape(means)[0]))
    # add the gene names
    gene_names = id_list
    for i in range(0, (np.shape(means)[0])):
        local_overdispersion_bool_table[i] = [gene_names[i]] + [local_overdispersion_bool_table[i]]
    local_overdispersion_bool_table = np.array(local_overdispersion_bool_table, dtype=str)
    logger.debug(local_overdispersion_bool_table)
    logger.debug(np.shape(local_overdispersion_bool_table))
    write_table(local_overdispersion_bool_table, out_file=out_dir + '/locally_overdispersed_boolean_table.txt')


if __name__ == "__main__":
    main()
