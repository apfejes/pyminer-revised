import os
import sys
import argparse
import fileinput
import logging
import random
import numpy as np

from bin.cell_signals import cell_signals
from bin.clustering import clustering
from bin.combine_gprofiler_results import combine_gprofiler_results
from bin.get_degrees_of_separation_from_adjacency_list import get_degrees_of_separation
from bin.get_high_marker_genes import get_high_marker_genes
from bin.get_stats import get_stats
from bin.make_website import MakeWebpage
from bin.mat_to_adj_list import mat_to_adj_list
from bin.plot_gene_subset import plot_gene_subset
from bin.plot_network import plot_network
from bin.pyminer_get_dispersion import pyminer_get_dispersion
from bin.pyminer_gprofiler_converter import pyminer_gprofiler_converter
from modules.common_functions import read_file, make_file, strip_split, get_file_path, write_table
from modules.references import HSAPIENS

logger = logging.getLogger()
LOG_FORMAT = '%(levelname)s\t%(name)s\t%(message)s'


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument(
        '-infile', '-in', '-i', '-input',
        dest='infile',
        type=str, required=True)
    parser.add_argument(
        "-hdf5", '-h5py',
        help='if the "-infile" is an hdf5 file whose main array name is "infile". '
             'This argument also requires "-ID_list" as a separate argument.',
        dest='h5py',
        action='store_true',
        default=False)
    parser.add_argument(
        '-ID_list', '-IDs', '-ids',
        help="A new line delimited file with no title line that contains the IDs "
             "associated with each row, in the correct order",
        dest='ID_list',
        type=str)
    parser.add_argument(
        '-columns', '-cols',
        help="A new line delimited file with no title line that contains the sample "
             "IDs associated with each column, in the correct order",
        dest='columns',
        type=str)
    parser.add_argument('-launch',
                        help="Use this flag if you want to launch firefox to view the output files",
                        action='store_true', default=False)

    parser.add_argument("-rand_seed", type=int, default=12345)

    # define which steps to do (ie: sample clustering, build networks, etc)
    parser.add_argument(
        "-networks_only", '-net_only',
        help='only find the networks and write adjacency list(s)',
        dest='mineOnly',
        action='store_true',
        default=False)

    parser.add_argument(
        "-sample_cluster_only",
        help='only do PyMINEr clustering',
        dest='sample_cluster_only',
        action='store_true',
        default=False)

    parser.add_argument(
        "-stringdb_dir",
        help='the directory for stringdb',
        default='lib/')

    parser.add_argument(
        "-no_sample_cluster", "-no_sample_clust",
        help='skip the sample clustering',
        dest='do_sample_clustering',
        action='store_false',
        default=True)

    parser.add_argument(
        "-cluster_on_all",
        help='cluster on all variables, without looking at dispersion',
        dest='do_dispersion',
        action='store_false',
        default=True)

    parser.add_argument(
        "-cluster_on_genes", "-clust_on_genes", "-clust_genes",
        help='If there is a specific set of genes you want to cluster on, supply the gene ids in this text file.'
    )

    parser.add_argument(
        "-leave_mito_ribo",
        help='If we should leave the mitochondrial and ribosomal genes for clustering',
        action='store_true',
        default=False)

    parser.add_argument(
        "-beta_test",
        help='set the paramaters for beta_testing',
        dest="sc_clust",
        action='store_true',
        default=False)

    # sample clustering options

    parser.add_argument(
        "-sample_k_clusters_known", '-sample_k_known',
        help='if you know how many groups there should be',
        dest='pre_determined_sample_k',
        type=int)

    parser.add_argument(
        "-sample_cluster_iter", "-clust_iter",
        help='How many iterations of clustering should we do. This can take some time, but higher '
             'iterations will give better results. Default = 10',
        dest='sample_cluster_iter',
        type=int,
        default=10)
    parser.add_argument(
        "-var_norm_for_clust", "-var_norm",
        help='normalize the variables for sample clustering',
        dest='var_norm_for_clust',
        action='store_true',
        default=True)
    parser.add_argument(
        "-no_var_norm",
        help='do not normalize the variables for sample clustering',
        dest='var_norm_for_clust',
        action='store_false',
        default=True)
    parser.add_argument(
        "-neg_cor_clust",
        help='cluster based on variables with negative correlations',
        action='store_true',
        default=False)
    parser.add_argument(
        "-no_spearman_clust",
        help='do not use the spearman similarity matrix to do the clustering. The default is '
             'to use the symetric Spearman correlation matrix of all samples against each other.',
        dest='spearman_clust',
        action='store_false',
        default=True)
    parser.add_argument(
        "-ap_clust", '-ap',
        help='if you want to use affinity propagation clustering',
        action='store_true',
        default=False)
    parser.add_argument(
        "-merge",
        help='if you want to perform cluster merger analysis',
        action='store_true',
        default=False)

    parser.add_argument(
        "-manual_sample_groups",
        help='if you know how which samples belong to which groups, feed in a file that has '
             'the samples in the first column, and their group number (index starting at 0), '
             'in the second column. The IDs must be in the same order as in the infile too.',
        dest='manual_sample_groups',
        type=str)

    parser.add_argument(
        "-species",
        help='species to use for gProfiler analysis',
        dest='species',
        default=HSAPIENS,
        type=str)

    # enrichment options
    parser.add_argument(
        "-anova_FDR_cutoff", "-FDR_cutoff",
        help='The Benjamini Hochberg corrected FDR value cutoff for calling a variable '
             'significantly different between groups.',
        dest='FDR_cutoff',
        type=float,
        default=0.05)

    parser.add_argument(
        "-zscore_cutoff",
        help='The Z-score cutoff for calling a variable enriched in a given group.',
        dest='zscore_cutoff',
        type=float,
        default=2.0)

    parser.add_argument(
        "-perplexity",
        help='Perplexity value to use for tSNE',
        default=None)

    # network detection options
    parser.add_argument("-spearman_only",
                        dest="spearman_only",
                        action='store_true',
                        default=False)

    parser.add_argument("-rho_cutoff", '-rho',
                        dest="rho_cutoff",
                        help="Suggested to leave this blank, but you can provide your own cutoff "
                             "instead of allowing PyMINEr to set it for you. This is the absolute "
                             "value of the spearman rho to use for a cutoff. If using scRNAseq, "
                             "try something in the range of 0.25-0.35. For bulk RNAseq, try something "
                             "closer to 0.70 - 0.90.",
                        type=float)

    parser.add_argument("-block_size",
                        help='how many variables will be used at once for correlation analyzis; '
                             'this helps keep memory requirements down and prevents a segmentation '
                             'fault memory error',
                        type=int,
                        default=5000)

    # for getting degree of separation from genes of iterest
    parser.add_argument(
        "-genes_of_interest", '-nodes_of_interest', '-goi', '-noi',
        help="if you have guesses about genes of interest a priori, we'll get the degree of "
             "separation from those genes in the primary analysis. This can either be a file "
             "with genes each on a new line, or it can be a comma separated list of genes.",
        dest="noi",
        type=str)

    # for getting degree of separation from genes of iterest
    parser.add_argument(
        "-no_cell_signals",
        help="if you don't want to do the autocrine paracrine signaling network prediction",
        action='store_true',
        default=False)

    # aesthetics options
    parser.add_argument(
        "-dpi",
        help='dots per inch for the output figures',
        dest="dpi",
        type=int,
        default=360)

    parser.add_argument(
        "-verbose",
        help='prints out some extra lines; this is primarily for troubleshooting',
        dest='verbose',
        action='store_true',
        default=False)

    parser.add_argument(
        "-process_count",
        help='multiprocessing sections of the app can spin up multiple processes to accellerate processing '
             'time - set this to the number of free CPUs available.',
        dest='process_count',
        type=int,
        default=1)

    args = parser.parse_args()
    return args


def process_noi(nodes_of_interest):
    noi_table = []
    for nodes in nodes_of_interest:
        if '\t' in nodes:
            noi_table.append(nodes.split('\t'))
        elif '::' in nodes:
            noi_table.append(nodes.split('::'))
        else:
            noi_table.append([nodes, nodes])

    # also make the ailias look-up dictionary
    alias_dict = {}
    # make the string that would be passed through to command line if needed
    pass_through_arg = ""
    # and the final list of nodes in the dataset
    final_nodes = []
    for noi in noi_table:
        alias_dict[noi[0]] = noi[1]
        pass_through_arg += "," + noi[0] + '::' + noi[1]
        final_nodes.append(noi[0])
    # remove the leading comma
    pass_through_arg = pass_through_arg[1:]

    return alias_dict, pass_through_arg, final_nodes


def do_the_clustering(args, out_adj_pos, working_directory, rho_cutoff, cor_stats_file=None, neg_cor_cutoff=1):
    logger.info('performing clustering')
    # if we're doing the clustering build up the string for the command line

    if args.do_dispersion:
        pyminer_get_dispersion(infile=args.infile,
                               id_file=args.ID_list,
                               hdf5=args.h5py,
                               out_dir=working_directory + '/sample_clustering_and_summary/dispersion/')
    clustering(infile=args.infile,
               neg_cor_count_clust=working_directory + '/sum_neg_cor.txt' if args.sc_clust else None,
               species=args.species,
               sc_clust=bool(args.sc_clust),
               sample_k_known=args.pre_determined_sample_k if args.pre_determined_sample_k is not None else 0,
               sample_cluster_iter=args.sample_cluster_iter,
               var_norm=bool(args.var_norm_for_clust),
               neg_cor_clust=cor_stats_file,
               neg_cor_cutoff=neg_cor_cutoff,
               first_neg_neighbor=True,
               pos_adj_list=out_adj_pos,
               spearman_dist=bool(args.spearman_clust or args.sc_clust),
               ap_clust=bool(args.ap_clust or args.sc_clust),
               do_hdf5=args.h5py,
               id_list=args.ID_list,
               columns=args.columns,
               do_merger=bool(args.merge or args.sc_clust),
               clust_on_genes=working_directory + '/sample_clustering_and_summary/dispersion/'
                                                  'locally_overdispersed_boolean_table.txt' if
               args.do_dispersion else args.cluster_on_genes,

               leave_mito_ribo=args.leave_mito_ribo,
               rand_seed=args.rand_seed,
               manual_sample_groups=args.manual_sample_groups if args.manual_sample_groups else None,
               out_dir=working_directory,
               perplexity=int(args.perplexity) if args.perplexity else None,
               dpi=args.dpi,
               rho_cutoff=rho_cutoff)


def main():
    args = parse_arguments()

    working_directory = get_file_path(args.infile)

    logging.basicConfig(level=logging.DEBUG,
                        filemode='w',
                        format=LOG_FORMAT)
    fh = logging.FileHandler(working_directory + 'pyminer.log')
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter(LOG_FORMAT)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    random.seed(args.rand_seed)
    np.random.seed(args.rand_seed)

    if args.cluster_on_genes:
        args.leave_mito_ribo = True
        args.do_dispersion = False

    if args.neg_cor_clust or args.sc_clust:
        args.do_dispersion = False

    args.infile = os.path.realpath(args.infile)

    logger.info(' '.join(sys.argv))

    if args.h5py and args.ID_list is None:
        logger.error("when using the hdf5 format, you must supply the -ID_list as another argument")
        return

    if args.ID_list is not None:
        id_list_path = args.ID_list
        id_list = read_file(id_list_path, 'lines')

    else:
        id_list = []
        first = True
        for line in fileinput.input(args.infile):
            if not first:
                temp_line = line.split('\t')
                id_list.append(temp_line[0])
            else:
                first = False

    id_file_name = working_directory + 'ID_list.txt'
    make_file('\n'.join(id_list), id_file_name)
    ID_hash = {gene: i for i, gene in enumerate(id_list)}
    #                                           call all of the other scripts #

    # get gene annotations

    annotation_prefix = working_directory + 'annotations'
    pyminer_gprofiler_converter(id_list_file=id_file_name,
                                species=args.species,
                                out=annotation_prefix,
                                annotations=True)

    # get the human orthologues if we don't have human ids
    if args.species != HSAPIENS:
        orthologue_prefix = working_directory + 'human_orthologues'
        pyminer_gprofiler_converter(id_list_file=id_file_name,
                                    out=orthologue_prefix,
                                    species=args.species,
                                    annotations=False)

    # 3: build the network(s)
    # if we're doing a single network:
    if args.rho_cutoff is not None:
        if args.rho_cutoff > 1.0 or args.rho_cutoff < 0:
            logger.error(
                'please use a rho cutoff between zero and one. We recommend not using '
                'this argument at all to allow for the built in False Positive Rate (FPR) '
                'algorithm to take over. If using scRNAseq, try something in the range of '
                '0.25-0.35. For bulk RNAseq, try something closer to 0.70 - 0.90.')
            return

    out_adj = working_directory + 'adj_list_rho.tsv'
    mat_to_adj_list(rand_seed=args.rand_seed,
                    infile_path=args.infile,
                    adj_list_out=out_adj,
                    id_list=working_directory + 'ID_list.txt',
                    rho_cutoff=args.rho_cutoff if args.rho_cutoff else None,
                    block_size=args.block_size,
                    sc_clust=bool(args.sc_clust),
                    col_ids=args.columns if args.columns else None,
                    hdf5=bool(args.h5py))

    # 3.5: subset the positive/negative correlations

    pos_neg_count = np.zeros((len(id_list), 2))

    out_adj_pos = out_adj[:-4] + '_dedup_pos.tsv'
    if os.path.isfile(out_adj_pos):
        os.remove(out_adj_pos)
    pos_f_out = open(out_adj_pos, 'w')

    out_adj_neg = out_adj[:-4] + '_dedup_neg.tsv'
    if os.path.isfile(out_adj_neg):
        os.remove(out_adj_neg)
    neg_f_out = open(out_adj_neg, 'w')

    counter = 0
    first = True
    for line in fileinput.input(out_adj[:-4] + '_dedup.tsv'):
        if first:
            logger.info('reading {}_dedup.tsv'.format(out_adj[:-4]))
            pos_f_out.write(line)
            neg_f_out.write(line)
            first = False
        else:
            temp_line = strip_split(line)
            try:
                float(temp_line[-1])
            except ValueError:
                logger.error('error at line: {}'.format(counter))
                logger.error('\t{}'.format(line))
            if float(temp_line[-1]) > 0:
                pos_f_out.write(line)
                pos_neg_count[ID_hash[temp_line[0]]][0] += 1
                pos_neg_count[ID_hash[temp_line[1]]][0] += 1
            else:
                neg_f_out.write(line)
                pos_neg_count[ID_hash[temp_line[0]]][1] += 1
                pos_neg_count[ID_hash[temp_line[1]]][1] += 1
        counter += 1
    pos_f_out.close()
    neg_f_out.close()

    # log some stats about how many positive and negative relationships there are
    pos_neg_count = pos_neg_count.tolist()
    for i in range(0, len(pos_neg_count)):
        pos_neg_count[i] = [id_list[i]] + pos_neg_count[i]

    pos_neg_count = [['ID', 'num_pos_cor', 'num_neg_cor']] + pos_neg_count

    write_table(pos_neg_count, working_directory + 'positive_negative_cor_counts.txt')

    # clustering
    sample_dir = working_directory + 'sample_clustering_and_summary/'
    if not os.path.isdir(sample_dir):
        os.mkdir(sample_dir)
    manual_sample_groups_file = sample_dir + '/sample_k_means_groups.txt'

    if args.manual_sample_groups is not None:
        do_the_clustering(args, out_adj_pos, working_directory, rho_cutoff=args.rho_cutoff)
    elif args.neg_cor_clust:
        do_the_clustering(args, out_adj_pos, working_directory,
                          cor_stats_file=working_directory + 'positive_negative_cor_counts.txt',
                          neg_cor_cutoff=args.neg_cor_cutoff, rho_cutoff=args.rho_cutoff)
    else:
        # do the clustering if it is needed
        # if not args.h5py or not args.neg_cor_clust:
        if args.do_sample_clustering:
            do_the_clustering(args, out_adj_pos, working_directory, rho_cutoff=args.rho_cutoff)
        elif args.manual_sample_groups is None:
            # if we aren't going to be doing the clustering, we'll need to pretend
            # that these are all from the same group
            # to do this we'll write the sample_k_means_groups_file
            for temp_line in fileinput.input(args.infile):
                title = np.array((temp_line.strip('\n')).split('\t'), dtype='U32')
                break
            fileinput.close()
            sample_names = title[1:]
            sample_k_table = list(zip(sample_names, [0] * len(sample_names)))

            # make the manual sample groups true, and make the file
            out_sample_group_table = np.transpose(np.array([sample_k_table]))
            write_table(out_sample_group_table, sample_dir + '/sample_k_means_groups.txt')

            # after we finish the clustering (or not), re-direct the manual sample groups to the
            # output sample group identity file

        else:
            logger.error("Couldn't find manual_sample_groups_file: {}".format(manual_sample_groups_file))
            args.manual_sample_groups = False

    if args.sample_cluster_only:
        logger.info("done with clustering")
        return

    # : enrichment & significance

    get_stats_args = {'infile': args.infile,
                      'sample_groups': manual_sample_groups_file,
                      'fdr_cutoff': args.FDR_cutoff,
                      'zscore': args.zscore_cutoff,
                      'hdf5': True if args.h5py else False,
                      'sample_dir': sample_dir,
                      'id_list': args.ID_list,
                      'columns': args.columns,
                      'process_count': args.process_count
               }
    if args.species:
        get_stats_args['species'] = args.species

    logger.info("calling get_stats.")
    logger.info(get_stats_args)
    get_stats(**get_stats_args)

    # combine the gprofiler results
    if os.path.isdir(sample_dir + '/significance/gprofiler'):
        gprofiler_args = {'in_dir': sample_dir + 'significance/gprofiler/',
                          'out': sample_dir + 'significance/'}
        logger.info("calling combine_gprofiler_results")
        logger.info(gprofiler_args)
        combine_gprofiler_results(**gprofiler_args)

    # get and plot highly expressed marker genes
    group_means_file = sample_dir + "k_group_means.txt"
    aov_file = sample_dir + 'significance/groups_1way_anova_results.txt'
    annotation_dict = working_directory + "annotations.pkl"
    high_marker_out_dir = sample_dir + 'significance/high_markers/'
    cluster_pkl = sample_dir + 'clustering_plots.pkl'

    if os.path.isdir(sample_dir + 'significance/'):
        if not os.path.isdir(high_marker_out_dir):
            os.mkdir(high_marker_out_dir)

        high_marker_args = {'means_file': group_means_file,
                            'sig': aov_file,
                            'out': high_marker_out_dir,
                            'symbol_def_dict': annotation_dict}
        logger.info('calling get_high_marker_genes')
        logger.info(high_marker_args)
        get_high_marker_genes(**high_marker_args)

        plot_subset_file = high_marker_out_dir + "subset_input.txt"
        logger.info("calling plot_gene_subset")

        gene_subset_args = {'infile': args.infile,
                            'nodes_of_interest_data': plot_subset_file,
                            'only_heatmap': True,
                            'id_list_file': id_file_name,
                            'out_dir': high_marker_out_dir,
                            'cluster_pkl': cluster_pkl,
                            'hdf5': True if args.h5py else False}
        logger.info(gene_subset_args)
        plot_gene_subset(**gene_subset_args)

    # 4: network analysis
    # check if the enrichment file exists
    if not os.path.isdir(working_directory + 'pos_cor_graphs'):
        os.mkdir(working_directory + 'pos_cor_graphs')

    k_enrich_file_exists = os.path.isfile(sample_dir + 'k_group_enrichment.txt')

    plot_network_args = {'id_list': working_directory + 'ID_list.txt',
                         'adj_list': out_adj_pos,
                         'plot_all': True,
                         'output': working_directory + 'pos_cor_graphs',
                         'node_attribute_lists': sample_dir + 'k_group_enrichment.txt' if k_enrich_file_exists else None,
                         'float_color_range': "{},-{}".format(args.zscore_cutoff, args.zscore_cutoff),
                         'verbose': args.verbose}
    logger.info('calling plot_network')
    logger.info(plot_network_args)
    plot_network(**plot_network_args)

    # analyze genes of interest

    if args.noi is not None:
        # first check if it's a path
        noi_list = read_file(args.noi, 'lines') if os.path.isfile(args.noi) else None

        alias_dict, pass_through_arg, temp_nodes_of_interest = process_noi(noi_list)

        logger.debug(temp_nodes_of_interest)

        final_noi_list = []
        for noi in temp_nodes_of_interest:
            if noi in id_list:
                final_noi_list.append(str(noi))
            else:
                logger.error("couldn't find {} in the IDs...".format(str(noi)))
        logger.info('getting degrees of separation for:')
        for noi in final_noi_list:
            logger.debug('\t{}'.format(noi))

        # now run the call
        if not os.path.isdir(working_directory + 'genes_of_interest'):
            os.mkdir(working_directory + 'genes_of_interest')
        get_degrees_of_separation_args = {'adjacency_list': out_adj_pos,
                                          'id_list': working_directory + "ID_list.txt",
                                          'nodes_of_interest': ','.join(final_noi_list),
                                          'out_dir': working_directory + 'genes_of_interest'}
        logger.info('calling get_degrees_of_separation')
        logger.info(get_degrees_of_separation_args)
        get_degrees_of_separation(**get_degrees_of_separation_args)

        # if the plots file is there plot the color over the positions
        plot_gene_subset_args = {'infile': args.infile,
                                 'out_dir': high_marker_out_dir,
                                 'nodes_of_interest_data': pass_through_arg,
                                 'id_list_file': working_directory + 'ID_list.txt',
                                 'cluster_pkl': sample_dir + 'clustering_plots.pkl',
                                 'hdf5': True if args.h5py else False}
        logger.info('calling plot_gene_subset')
        logger.info(plot_gene_subset_args)
        plot_gene_subset(**plot_gene_subset_args)

    if not args.no_cell_signals:

        # check that the the string db dir is correct
        if not args.stringdb_dir.endswith(os.sep):
            args.stringdb_dir += os.sep

        if not os.path.isfile(args.stringdb_dir+'9606.protein.actions.v10.5.txt.gz'):
            # If we can't find the stringdb file for humans in the given strindb dir
            # then check if the lib dir is in adjacent to the current pyminer bin dir.
            # This will be the case when running pyminer locally
            possible_alternative_stringdb_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'lib/'))
            logger.info('looking for: {}'.format(possible_alternative_stringdb_dir))
            if os.path.isfile(possible_alternative_stringdb_dir+'/9606.protein.actions.v10.5.txt.gz'):
                args.stringdb_dir = possible_alternative_stringdb_dir
                logger.info("we found the right stringDB directory:")
                logger.info('\t{}'.format(possible_alternative_stringdb_dir))
            else:
                possible_alternative_stringdb_dir = os.path.abspath('/usr/local/lib/cell_signals/')+'/'
                if os.path.isfile(possible_alternative_stringdb_dir+'9606.protein.actions.v10.5.txt.gz'):
                    args.stringdb_dir = possible_alternative_stringdb_dir
                    logger.info("we found the right stringDB directory:")
                    logger.info('\t{}'.format(possible_alternative_stringdb_dir))
                else:
                    raise Exception("we couldn't find the appropriate cell signals directory")

        if not os.path.isdir(args.stringdb_dir):
            raise Exception("couldn't find the directory given in for the stringdb file")

        if not args.stringdb_dir.endswith(os.sep):
            args.stringdb_dir += os.sep

        signaling_dir = working_directory + 'autocrine_paracrine_signaling/'
        if not os.path.isdir(signaling_dir):
            os.mkdir(signaling_dir)

        cell_signals_args = {'out_dir': signaling_dir,
                             'organism': args.species,
                             'gene_table': sample_dir + '/significance/significant_and_enriched_boolean_table.txt',
                             'stringdb_dir': args.stringdb_dir}
        logger.info('calling cell_signals')
        logger.info(cell_signals_args)
        cell_signals(**cell_signals_args)

        if os.path.isfile(signaling_dir + '/all_cell_type_specific_interactions_gprofiler.txt'):
            combine_gprofiler_results_args = {
                'in_dir': signaling_dir + '/all_cell_type_specific_interactions_gprofiler.txt',
                'out': signaling_dir}
            logger.info('calling combine_gprofiler_results')
            logger.info(combine_gprofiler_results_args)
            combine_gprofiler_results(**combine_gprofiler_results_args)

    # summarize the results via the website generator
    logger.info(f'calling web page generator with working directory {working_directory}')
    mw = MakeWebpage(working_directory, launch=False)
    mw.make_webpage()
    logger.info("Pyminer Complete - Success.")


if __name__ == "__main__":
    main()
