import logging
import numpy as np
from sklearn.cluster import AffinityPropagation as ap
from sklearn.metrics.pairwise import euclidean_distances

logger = logging.getLogger(__name__)


def new_affinity_propagation(S, block_size, preference=None, convergence_iter=15, max_iter=200,
                             damping=0.5, return_n_iter=False, temp_ap_hdf5=None):
    """Perform Affinity Propagation Clustering of data

    Read more in the :ref:`User Guide <affinity_propagation>`.

    Parameters
    ----------

    S : array-like, shape (n_samples, n_samples)
        Matrix of similarities between points

    preference : array-like, shape (n_samples,) or float, optional
        Preferences for each point - points with larger values of
        preferences are more likely to be chosen as exemplars. The number of
        exemplars, i.e. of clusters, is influenced by the input preferences
        value. If the preferences are not passed as arguments, they will be
        set to the median of the input similarities (resulting in a moderate
        number of clusters). For a smaller amount of clusters, this can be set
        to the minimum value of the similarities.

    convergence_iter : int, optional, default: 15
        Number of iterations with no change in the number
        of estimated clusters that stops the convergence.

    max_iter : int, optional, default: 200
        Maximum number of iterations

    damping : float, optional, default: 0.5
        Damping factor between 0.5 and 1.

    return_n_iter : bool, default False
        Whether or not to return the number of iterations.

    Returns
    -------

    cluster_centers_indices : array, shape (n_clusters,)
        index of clusters centers

    labels : array, shape (n_samples,)
        cluster labels for each point

    n_iter : int
        number of iterations run. Returned only if `return_n_iter` is
        set to True.

    Notes
    -----
    See examples/cluster/plot_affinity_propagation.py for an example.

    References
    ----------
    Brendan J. Frey and Delbert Dueck, "Clustering by Passing Messages
    Between Data Points", Science Feb. 2007
    """
    # S = as_float_array(S, copy=copy)

    n_samples = S.shape[0]

    # ind = np.arange(n_samples)

    logger.info("S:", S[:5, :5])

    bins = []
    cur_bin = 0
    bin_size = block_size
    while cur_bin < n_samples:
        bins.append(min(cur_bin, n_samples))
        cur_bin += bin_size

    bins.append(n_samples)

    if S.shape[0] != S.shape[1]:
        raise ValueError("S must be a square array (shape=%s)" % repr(S.shape))

    if preference is None:
        try:
            preference = np.min(S, axis=1)
        except Exception:
            preference = np.zeros((np.shape(S)[0],))
            for i in range(0, np.shape(S)[0]):
                preference[i] = np.min(S[i, :])

    if damping < 0.5 or damping >= 1:
        raise ValueError('damping must be >= 0.5 and < 1')

    random_state = np.random.RandomState(0)

    # Place preference on the diagonal of S
    logger.info("setting preference to {}".format(preference))
    for d in range(0, np.shape(S)[0]):
        S[d, d] = preference

    if 'A' in temp_ap_hdf5:
        del temp_ap_hdf5['A']

    if 'S2' in temp_ap_hdf5:
        del temp_ap_hdf5['S2']

    if 'R' in temp_ap_hdf5:
        del temp_ap_hdf5['R']

    if 'tmp' in temp_ap_hdf5:
        del temp_ap_hdf5['tmp']

    if 'rand_mat' in temp_ap_hdf5:
        del temp_ap_hdf5['rand_mat']

    a = temp_ap_hdf5.create_dataset('A', (n_samples, n_samples), dtype=np.float16)
    r = temp_ap_hdf5.create_dataset('R', (n_samples, n_samples), dtype=np.float16)
    # A = np.zeros((n_samples, n_samples),dtype = 'f0')
    # R = np.zeros((n_samples, n_samples),dtype = 'f0')  # Initialize messages

    # Intermediate results
    tmp = temp_ap_hdf5.create_dataset('tmp', (n_samples, n_samples), dtype=np.float16)

    logger.info('removing degeneracies')
    for i in range(0, len(bins) - 1):
        nrow = np.shape(S[bins[i]:bins[i + 1], :])[0]
        temp_rand = S[bins[i]:bins[i + 1], :] * np.finfo(np.float16).eps
        temp_rand = temp_rand + (np.finfo(np.float16).tiny * 100)
        temp_rand = temp_rand * random_state.randn(nrow, n_samples)
        S[bins[i]:bins[i + 1], :] = S[bins[i]:bins[i + 1], :] + temp_rand

    # Execute parallel affinity propagation updates
    e = np.zeros((n_samples, convergence_iter))

    logger.debug('S {}'.format(S[:5, :5]))
    logger.debug('A {}'.format(a[:5, :5]))
    logger.debug('R {}'.format(r[:5, :5]))
    logger.debug('tmp {}'.format(tmp[:5, :5]))
    logger.debug('e {}'.format(e[:5, :5]))

    last_it = 0
    for it in range(max_iter):
        last_it = it

        for i in range(0, len(bins) - 1):
            tmp[bins[i]:bins[i + 1], :] = a[bins[i]:bins[i + 1], :] + S[bins[i]:bins[i + 1], :]
            # + tmp#; compute responsibilities
        i_r = np.zeros((n_samples,))
        for i in range(0, len(bins) - 1):
            i_r[bins[i]:bins[i + 1]] = np.argmax(tmp[bins[i]:bins[i + 1], :], axis=1)

        for i in range(0, n_samples):
            i_r[i] = np.argmax(tmp[i, :])

        y = np.zeros((n_samples,))
        for i in range(0, n_samples):
            y[i] = tmp[i, i_r[i]]  # np.max(A + S, axis=1)

        for i in range(0, n_samples):
            tmp[i, i_r[i]] = -np.inf

        y2 = np.zeros((n_samples,))
        for i in range(0, len(bins) - 1):
            y2[bins[i]:bins[i + 1]] = np.max(tmp[bins[i]:bins[i + 1], :], axis=1)

        # tmp = Rnew
        for i in range(0, len(bins) - 1):
            tmp[bins[i]:bins[i + 1], :] = S[bins[i]:bins[i + 1], :] - y[bins[i]:bins[i + 1], None]
        for i in range(0, n_samples):
            tmp[i, i_r[i]] = S[i, i_r[i]] - y2[i]

        # Damping
        for i in range(0, len(bins) - 1):
            tmp[bins[i]:bins[i + 1], :] = tmp[bins[i]:bins[i + 1], :] * (1 - damping)
        for i in range(0, len(bins) - 1):
            r[bins[i]:bins[i + 1], :] = r[bins[i]:bins[i + 1], :] * damping
            r[bins[i]:bins[i + 1], :] = r[bins[i]:bins[i + 1], :] + tmp[bins[i]:bins[i + 1], :]

        logger.debug('after dampening')
        logger.debug('{} S {}'.format(it, S[:5, :5]))
        logger.debug('{} A {}'.format(it, a[:5, :5]))
        logger.debug('{} R {}'.format(it, r[:5, :5]))
        logger.debug('{} tmp {}'.format(it, tmp[:5, :5]))
        logger.debug('{} e {}'.format(it, e[:5, :5]))
        logger.debug('{} Y {}'.format(it, y[:5, ]))
        logger.debug('{} Y2 {}'.format(it, y2[:5, ]))

        # tmp = Rp; compute availabilities
        for i in range(0, len(bins) - 1):
            tmp[bins[i]:bins[i + 1], :] = np.maximum(r[bins[i]:bins[i + 1], :], 0)  # , tmp)
        # tmp.flat[::n_samples + 1] = R.flat[::n_samples + 1]
        for d in range(0, n_samples):
            tmp[d, d] = r[d, d]

        logger.debug('after computing availabilities')
        logger.debug('{} S {}'.format(it, S[:5, :5]))
        logger.debug('{} A {}'.format(it, a[:5, :5]))
        logger.debug('{} R {}'.format(it, r[:5, :5]))
        logger.debug('{} tmp {}'.format(it, tmp[:5, :5]))
        logger.debug('{} e {}'.format(it, e[:5, :5]))
        logger.debug('{} Y {}'.format(it, y[:5, ]))
        logger.debug('{} Y2 {}'.format(it, y2[:5, ]))

        # tmp = -Anew
        for i in range(0, len(bins) - 1):
            tmp[:, bins[i]:bins[i + 1]] -= np.sum(tmp[:, bins[i]:bins[i + 1]], axis=0)

        d_a = np.zeros((n_samples,))
        for d in range(0, n_samples):
            d_a[d] = tmp[d, d]
        # dA = np.diag(tmp[:,:]).copy()
        for i in range(0, len(bins) - 1):
            tmp[bins[i]:bins[i + 1], :] = np.clip(tmp[bins[i]:bins[i + 1], :], 0, np.inf)
        for d in range(0, n_samples):
            tmp[d, d] = d_a[d]

        logger.debug('after -Anew')
        logger.debug('{} S {}'.format(it, S[:5, :5]))
        logger.debug('{} A {}'.format(it, a[:5, :5]))
        logger.debug('{} R {}'.format(it, r[:5, :5]))
        logger.debug('{} tmp {}'.format(it, tmp[:5, :5]))
        logger.debug('{} e {}'.format(it, e[:5, :5]))
        logger.debug('{} Y {}'.format(it, y[:5, ]))
        logger.debug('{} Y2 {}'.format(it, y2[:5, ]))

        # Damping
        for i in range(0, len(bins) - 1):
            tmp[bins[i]:bins[i + 1], :] = tmp[bins[i]:bins[i + 1], :] * (1 - damping)
            a[bins[i]:bins[i + 1], :] = a[bins[i]:bins[i + 1], :] * damping
            a[bins[i]:bins[i + 1], :] = a[bins[i]:bins[i + 1], :] - tmp[bins[i]:bins[i + 1], :]
        # for i in range(0,np.shape(R)[0]):
        #     A[i,:] = A[i,:] * damping
        #     A[i,:] = A[i,:] - tmp[i,:]

        # Check for convergence
        e = np.array(np.zeros(n_samples, ), dtype=bool)
        for d in range(0, n_samples):
            e[d] = (a[d, d] + r[d, d]) > 0
        # E = (np.diag(A) + np.diag(R)) > 0
        e[:, it % convergence_iter] = e
        k = np.sum(e, axis=0)

        if it >= convergence_iter:
            se = np.sum(e, axis=1)
            unconverged = (np.sum((se == convergence_iter) + (se == 0))
                           != n_samples)
            if (not unconverged and (k > 0)) or (it == max_iter):
                logger.info("Converged after %d iterations." % it)
                break
    else:
        logger.info("Did not converge")

    d_a = np.zeros((n_samples,))
    d_r = np.zeros((n_samples,))
    for d in range(0, n_samples):
        d_a[d] = a[d, d]
        d_r[d] = r[d, d]
    i_r = np.where((d_a + d_r) > 0)[0]
    k = i_r.size  # Identify exemplars

    logger.debug('{} dAR {}'.format(last_it, d_a + d_r))
    logger.debug('{} I {}'.format(last_it, i_r))
    logger.debug('{} K {}'.format(last_it, k))

    if k > 0:
        c = np.argmax(S[:, i_r], axis=1)
        c[i_r] = np.arange(k)  # Identify clusters
        # Refine the final set of exemplars and clusters and return results
        for k in range(k):
            ii = np.where(c == k)[0]
            temp_j_calc_mat = np.zeros((np.shape(ii)[0], np.shape(ii)[0]))
            for exemp in range(0, np.shape(ii)[0]):
                exemp_ind = ii[exemp]
                temp_s = S[exemp_ind, :]
                temp_j_calc_mat[exemp, :] = temp_s[ii]
            j = np.argmax(np.sum(temp_j_calc_mat, axis=0))
            i_r[k] = ii[j]

        c = np.argmax(S[:, i_r], axis=1)
        c[i_r] = np.arange(k)
        labels = i_r[c]
        # Reduce labels to a sorted, gapless, list
        cluster_centers_indices = np.unique(labels)
        labels = np.searchsorted(cluster_centers_indices, labels)
    else:
        labels = np.empty((n_samples, 1))
        cluster_centers_indices = None
        labels.fill(np.nan)

    if return_n_iter:
        return cluster_centers_indices, labels, last_it + 1
    else:
        temp_ap_hdf5.close()
        return cluster_centers_indices, labels


class Hdf5AffinityPropagation(ap):

    def __init__(self, block_size, damping=.5, max_iter=200, convergence_iter=15,
                 copy=True, preference=None, affinity='euclidean',
                 temp_ap_hdf5=None, verbose=False, ):

        ap.__init__(self)

        self.block_size = block_size
        self.damping = damping
        self.max_iter = max_iter
        self.convergence_iter = convergence_iter
        self.copy = copy
        self.verbose = verbose
        self.preference = preference
        self.affinity = affinity
        self.temp_ap_hdf5 = temp_ap_hdf5

        self.cluster_centers_ = None
        self.cluster_centers_indices_ = None
        self.affinity_matrix_ = None
        self.labels_ = None
        self.n_iter_ = None

    def fit(self, affinity_matrix, y=None):
        """ Create affinity matrix from negative euclidean distances, then
        apply affinity propagation clustering.

        Parameters
        ----------

        affinity_matrix : array-like, shape (n_samples, n_features) or (n_samples, n_samples)
            Data matrix or, if affinity is ``precomputed``, matrix of
            similarities / affinities.
        y : unused parameter, but required for signature.
        """
        # X = check_array(X, accept_sparse='csr')
        if self.affinity == "precomputed":
            self.affinity_matrix_ = affinity_matrix
        elif self.affinity == "euclidean":
            self.affinity_matrix_ = -euclidean_distances(affinity_matrix, squared=True)
        else:
            raise ValueError("Affinity must be 'precomputed' or "
                             "'euclidean'. Got %s instead"
                             % str(self.affinity))

        self.cluster_centers_indices_, self.labels_, self.n_iter_ = \
            new_affinity_propagation(
                self.affinity_matrix_, self.block_size, self.preference, max_iter=self.max_iter,
                convergence_iter=self.convergence_iter, damping=self.damping,
                return_n_iter=True, temp_ap_hdf5=self.temp_ap_hdf5)

        if self.affinity != "precomputed":
            self.cluster_centers_ = affinity_matrix[self.cluster_centers_indices_].copy()

        return self
