import argparse
import numpy as np

from modules.common_functions import read_table, write_table, import_dict
import logging

# hyperparameters
SIG_CUTOFF = 0.05
Q_CUTOFF = 0.5
# the percent of the largest distance to second to consider for marker genes
# this gives you highly expressed differentially expressed genes
PERCENTILE = 0.9
logger = logging.getLogger(__name__)


def get_group(i, sig_q_and_dist, group_names, k_group_max, k_group_means):
    if not sig_q_and_dist[i]:
        return "None"
    else:
        index = int(np.where(k_group_means[i] == k_group_max[i])[0])
        return group_names[index]


def process_symbol(in_symbol_list, ensg):
    if not in_symbol_list:
        return ensg
    else:
        return '_'.join(in_symbol_list)


def process_def(in_def):
    return "NA" if not in_def else in_def[0]


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    # global arguments
    parser.add_argument('-means', '-m', '-mean', dest='means', type=str)
    parser.add_argument('-significance', '-sig', '-anovas', '-anova', '-aov', dest='sig', type=str)
    parser.add_argument('-out', '-o', help="output directory", dest='out', type=str)
    parser.add_argument('-annotation_dict', '-ad', dest='symbol_def_dict', type=str)
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    get_high_marker_genes(args.symmbol_def_dict, args.means, args.sig, args.out)


def get_high_marker_genes(symbol_def_dict, means_file, sig, out):
    # import the annotation dict
    symbol_dict, def_dict = import_dict(symbol_def_dict)
    k_group_means_file = np.array(read_table(means_file))
    id_list = k_group_means_file[1:, 0].tolist()
    id_list_len = len(id_list)
    group_names = k_group_means_file[0, 1:].tolist()
    if len(group_names) < 3:
        raise Exception('need at least 3 groups')
    # get the means array
    k_group_means = np.array(k_group_means_file[1:, 1:], dtype=float)
    k_group_min = np.min(k_group_means, axis=1)
    k_group_max = np.max(k_group_means, axis=1)
    k_group_range = k_group_max - k_group_min

    # get the distance from max to second max
    dist_max_to_second = np.zeros(id_list_len)
    q_value_vect = np.zeros(id_list_len)

    for i in range(id_list_len):
        temp_sorted = np.sort(k_group_means[i, :])[::-1]
        dist_max_to_second[i] = temp_sorted[0] - temp_sorted[1]
        q_value_vect[i] = dist_max_to_second[i] / k_group_range[i]

    sorted_dist_to_max = np.sort(dist_max_to_second[:], kind="mergesort")
    index_for_cutoff = int(id_list_len * PERCENTILE)
    dist_max_to_second_cutoff = sorted_dist_to_max[index_for_cutoff]
    bh_corrected_aov_file = np.array(read_table(sig))
    logger.debug("bh_corrected_aov_file {}".format(bh_corrected_aov_file[:10]))
    bh_corrected_aov = np.array(bh_corrected_aov_file[1:, 2], dtype=float)

    # only consider significant genes
    sig_bool = bh_corrected_aov < SIG_CUTOFF
    q_bool = q_value_vect > Q_CUTOFF
    dist_to_second_max_bool = dist_max_to_second > dist_max_to_second_cutoff
    logger.info('sig_bool {} {} '.format(sig_bool, sum(sig_bool)))
    logger.debug('q_bool {} {}'.format(q_bool, sum(q_bool)))
    logger.debug('dist_to_second_max_bool {} {}'.format(dist_to_second_max_bool, sum(dist_to_second_max_bool)))
    sig_q_and_dist = (np.array(sig_bool, dtype=int) + np.array(q_bool, dtype=int) + np.array(dist_to_second_max_bool,
                                                                                             dtype=int)) == 3
    logger.info('combined {}'.format(sig_q_and_dist))
    logger.info('found {} highly expressed, somewhat "exclusive" marker genes'.format(sum(sig_q_and_dist)))
    logger.info('making the output table...')
    out_file = [['gene', 'gene_symbol', 'gene_def', 'aov_BH_p', 'range',
                 'dist_from_max_to_second_max', 'q_value', 'group']]
    for i, ident in enumerate(id_list):
        # in the case of entrez IDs
        try:
            temp_id = "{:.0f}".format(ident)
        except ValueError:
            temp_id = ident

        temp_line = [temp_id,
                     process_symbol(symbol_dict[temp_id], temp_id),
                     "NA" if not def_dict[temp_id] else def_dict[temp_id][0],
                     bh_corrected_aov[i],
                     k_group_range[i],
                     dist_max_to_second[i],
                     q_value_vect[i],
                     get_group(i, sig_q_and_dist, group_names, k_group_max, k_group_means)]
        out_file.append(temp_line)

    # get the input for plot_subset...
    plot_subset_input = []
    for i, file_name in enumerate(out_file):
        if file_name[-1] != "None":
            plot_subset_input.append((file_name[0], file_name[1]))
    write_table(plot_subset_input, out + "/subset_input.txt")
    write_table(out_file, out + '/marker_gene_annotations.tsv')


if __name__ == "__main__":
    main()
