import os
import argparse
import fileinput
import logging
import h5py
import numpy as np

from modules.common_functions import make_file

logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument('infile', type=str,
                        help="this must be a tab delimited file which will be converted to the hdf5 "
                             "format (output will be created with the same filename with the .hdf5 extention)")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    tab_to_h5(args.infile)


def tab_to_h5(infile):
    # first read through the input tab delim
    # file to see the dimentions of the input matrix
    logger.info('reading in the file, please wait')
    cols = None
    col_names = None
    id_list = []  # the variable names (the length here will also be the number of rows in the .h5 matrix)
    for line_count, line in enumerate(fileinput.input(infile)):
        line = line.strip()
        line = line.split('\t')
        if line_count % 1000 == 0:
            if line_count == 0:
                # note that the number of columns for the
                # hdf5 file will be cols-1 because the first
                # column in the input file will be the
                cols = len(line)
                col_names = line
            else:
                logger.debug('\tline {}'.format(line_count))
        else:
            if cols != len(line):
                raise Exception('there is a line (' + str(line_count) +
                                ') has an inconsistent number of rows with previous rows')
            id_list.append(line[0])

    fileinput.close()
    temp = str(infile).split('/')
    temp = '/'.join(temp[:-1])
    make_file('\n'.join(col_names), temp + '/column_IDs.txt')
    make_file('\n'.join(id_list), temp + '/ID_list.txt')
    logger.info('we detected {} data rows'.format(len(id_list)))
    logger.info('   and  {} data cols' .format(cols - 1))
    logger.info("now we will make the hdf5 file and populate it with the values from your input dataset")
    outfile = os.path.splitext(infile)[0] + '.hdf5'
    file_handle = h5py.File(outfile, "w")
    # set up the data matrix (this assumes float32)
    dset = file_handle.create_dataset("infile", (len(id_list), cols - 1), dtype=np.float32)
    # populate dset with the values from the input dataset
    line_count = 0
    for line in fileinput.input(infile):
        line = line.strip()
        line = line.split('\t')
        if line_count % 1000 == 0:
            logger.debug('\tline {}'.format(line_count))
        if line_count > 0:
            dset[(line_count - 1), :] = np.array(np.float32(i) for i in line[1:])

        line_count += 1
    # close the input file
    fileinput.close()
    # close the hdf5 file
    file_handle.close()


if __name__ == "__main__":
    main()
