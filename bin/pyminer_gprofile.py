import argparse
import os
import random
from gprofiler import GProfiler
import logging

from modules.common_functions import read_file, write_table
from modules.references import HSAPIENS

logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-infile", '-i', '-input',
                        help="a file containing gene ids separated on new lines",
                        type=str)

    parser.add_argument("-out", '-outfile', '-o',
                        help="the file that we'll write the results to",
                        type=str)

    parser.add_argument("-species", '-s',
                        help="a gProfiler accepted species code. Dafault = '{}'".format(HSAPIENS),
                        type=str,
                        default=HSAPIENS)

    parser.add_argument("-background", '-b',
                        help="If you are going to use a custom background provide either a comma "
                             "separated list (no spaces), or a file with genes on a new line each.",
                        type=str,
                        default=None)

    parser.add_argument("-verbose", "-v", action="store_true")

    args = parser.parse_args()
    return args


# fix for entrez...
def process_genes(in_genes):
    out_genes = []
    for gene in in_genes:
        try:
            float(gene)
        except ValueError:
            out_genes.append(gene)
        else:
            temp_gene = "{:.0f}".format(gene)
            out_genes.append(temp_gene)
    return out_genes


def main():
    args = parse_arguments()

    pyminer_gprofile(infile=args.infile,
                     outfile=args.out,
                     species=args.species,
                     background=args.background)


def pyminer_gprofile(infile, outfile, species=HSAPIENS, background=None):
    # setup the api
    gp = GProfiler('PyMINEr_' + str(random.randint(0, int(1e6))), want_header=True)
    # import the gene names
    genes = read_file(infile, lines_or_raw='lines')
    logger.debug("genes: {}".format(genes))
    if background is not None:
        if os.path.isfile(background):
            temp_bg = read_file(background, lines_or_raw='lines')
        else:
            temp_bg = str(background)
            temp_bg = temp_bg.split(',')
        logger.debug("Background: {}".format(temp_bg))
        result = gp.gprofile(process_genes(genes), custom_bg=temp_bg, organism=species)
    else:
        result = gp.gprofile(process_genes(genes), organism=species)
    logger.debug(result)
    for line in result:
        logger.debug(line)
    if outfile is None:
        outfile = infile[:-4] + '_gProfiler.txt'
    write_table(result, outfile)


if __name__ == "__main__":
    main()
