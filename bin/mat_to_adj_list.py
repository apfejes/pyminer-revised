from __future__ import division, print_function, absolute_import

import sys
from time import time

import logging
import argparse
import glob
import h5py
import os
import random
import warnings
from matplotlib import use

# Scipy imports.
use('Agg')

import matplotlib.pyplot as plt
import numpy as np

from numpy import ma
from scipy.stats import gaussian_kde
from scipy.stats import poisson, mstats_basic
from scipy.stats import rankdata
from sklearn import metrics

from modules.common_functions import read_file, read_table, write_table, import_dict, save_dict

from copy import deepcopy

logger = logging.getLogger(__name__)
# constants
FPR = 1000  # 1 in 1000 false positive rate for negative correlation clustering
FPR_MULTIPLE = 15  # the number of times the expected number of false positives to be included in clustering
POS_FPR = int(1e3)


def _chk_asarray(a, axis):
    if axis is None:
        a = np.ravel(a)
        outaxis = 0
    else:
        a = np.asarray(a)
        outaxis = axis
    if a.ndim == 0:
        a = np.atleast_1d(a)
    return a, outaxis


def _contains_nan(a, nan_policy='propagate'):
    policies = ['propagate', 'raise', 'omit']
    if nan_policy not in policies:
        raise ValueError("nan_policy must be one of {%s}" %
                         ', '.join("'%s'" % s for s in policies))
    try:
        # Calling np.sum to avoid creating a huge array into memory
        # e.g. np.isnan(a).any()
        with np.errstate(invalid='ignore'):
            contains_nan = np.isnan(np.sum(a))
    except TypeError:
        # If the check cannot be properly performed we fallback to omiting
        # nan values and raising a warning. This can happen when attempting to
        # sum things that are not numbers (e.g. as in the function `mode`).
        contains_nan = False
        nan_policy = 'omit'
        warnings.warn("The input array could not be properly checked for nan "
                      "values. nan values will be ignored.", RuntimeWarning)
    if contains_nan and nan_policy == 'raise':
        raise ValueError("The input contains nan values")
    return contains_nan, nan_policy


def no_p_spear(a, b=None, axis=0, nan_policy='propagate'):
    a, axisout = _chk_asarray(a, axis)

    contains_nan, nan_policy = _contains_nan(a, nan_policy)

    if contains_nan and nan_policy == 'omit':
        a = ma.masked_invalid(a)
        b = ma.masked_invalid(b)
        return mstats_basic.spearmanr(a, b, axis)

    if a.size <= 1:
        return SpearmanrResult(np.nan, np.nan)
    ar = np.apply_along_axis(rankdata, axisout, a)

    br = None
    if b is not None:
        b, axisout = _chk_asarray(b, axis)

        contains_nan, nan_policy = _contains_nan(b, nan_policy)

        if contains_nan and nan_policy == 'omit':
            b = ma.masked_invalid(b)
            return mstats_basic.spearmanr(a, b, axis)

        br = np.apply_along_axis(rankdata, axisout, b)
    # n = a.shape[axisout]
    rs = np.corrcoef(ar, br, rowvar=axisout)

    olderr = np.seterr(divide='ignore')  # rs can have elements equal to 1

    # try:
    # clip the small negative values possibly caused by rounding
    # errors before taking the square root
    # t = rs * np.sqrt(((n - 2) / ((rs + 1.0) * (1.0 - rs))).clip(0))
    # finally:

    np.seterr(**olderr)

    if rs.shape == (2, 2):
        return rs[1, 0]
    else:
        return rs


# parse the name of the file to get the
def parse_dict_name(dict_path, id_list):
    dict_path = dict_path.split('/')
    dict_path = dict_path[-1]
    dict_path = dict_path.strip('rho_')
    dict_path = dict_path.strip('.pkl')
    dict_path = dict_path.split('_vs_')
    dict_path[0] = dict_path[0].split('_to_')
    dict_path[1] = dict_path[1].split('_to_')
    dict_path[0][0] = int(dict_path[0][0])
    dict_path[0][1] = int(dict_path[0][1])
    dict_path[1][0] = int(dict_path[1][0])
    dict_path[1][1] = int(dict_path[1][1])
    temp_id_indices = np.concatenate(
        (np.arange(dict_path[0][0], dict_path[0][1]), np.arange(dict_path[1][0], dict_path[1][1])))
    temp_ids = [id_list[i] for i in temp_id_indices]
    return temp_id_indices, temp_ids


def get_density(in_vect):
    in_vect = np.array(in_vect)
    in_vect = in_vect[np.array((np.isnan(in_vect) * -1) + 1, dtype=bool)]  # remove nans
    in_vect = in_vect[np.array((np.isinf(in_vect) * -1) + 1, dtype=bool)]  # remove infs
    offset = min(in_vect)
    in_vect = in_vect - offset
    density = gaussian_kde(in_vect)
    xs = np.arange(0, max(in_vect), max(in_vect) / 150)
    y = density(xs)
    y = y / sum(y)
    return xs + offset, y


def plot_one_density(neg_cor_density_x, neg_cor_density_y, out_file,
                     label='negative correlation Rhos from bootstrap shuffled negative control'):
    plt.clf()
    plt.plot(neg_cor_density_x, neg_cor_density_y,
             label=label,
             color='blue',
             linewidth=3)
    plt.legend()
    plt.savefig(out_file,
                dpi=600,
                bbox_inches='tight')


def plot_one_density_x_cutoff(neg_cor_density_x, neg_cor_density_y, cutoff, out_file, poisson_mu=None,
                              label="density"):
    x_min = np.min(neg_cor_density_x)
    y_min = np.min(neg_cor_density_y)
    x_max = np.max(neg_cor_density_x)
    y_max = np.max(neg_cor_density_y)

    plt.clf()
    plt.plot(neg_cor_density_x, neg_cor_density_y,
             label=label,
             color='blue',
             linewidth=3)
    plt.plot([cutoff, cutoff], [y_min, y_max],
             color='k',
             lw=2,
             label='cutoff')

    # calculate the poisson at the given mu=cutoff
    if poisson_mu is not None:
        temp_x = np.arange(x_max - x_min) + x_min
        dist = poisson(poisson_mu)
        plt.plot(np.log2(temp_x + 1), np.log2(dist.pmf(temp_x) + 1), ls='--', color='red',
                 label='poisson', linestyle='steps-mid')
    plt.legend()

    plt.savefig(out_file,
                dpi=600,
                bbox_inches='tight')


def density_scatter(x, y, out_file, log_density=0, expected_x=[], expected_y=[]):
    xy = np.vstack([x, y])
    z = gaussian_kde(xy)(xy)
    logger.debug(z)
    if log_density:
        while log_density:
            log_density -= 1
            z = np.log2(z + 1)
            z = z - min(z)
            z = z / max(z)
            logger.debug(z)
    plt.clf()
    fig, ax = plt.subplots()
    ax.scatter(x, y, c=z, s=5, edgecolor='', cmap=plt.cm.jet)
    if expected_x != [] and expected_y != []:
        ax.plot(expected_x, expected_y, linestyle='-', c='black')
        plt.xlim((-1, max(x) + 1))
        plt.ylim((0, max(y) + 1))

    plt.savefig(out_file,
                dpi=600,
                bbox_inches='tight'
                )


def calculate_mad(in_vect, dev_num=3, mean=False):
    med = np.median(in_vect)
    if mean:
        dev = np.mean(np.abs(in_vect - med))
    else:
        dev = np.median(np.abs(in_vect - med))
    logger.debug('median: {}'.format(med))
    logger.debug('median absolute deviation: {}'.format(dev))
    cutoff = med + (dev_num * dev)
    logger.debug('cutoff ='.format(cutoff))
    return med, dev, cutoff


def get_Z_cutoff(vect,z=4.5,positive = False):
    # this is for a half distribution, so mult by -1 to make it 'normal'
    vect_original = np.array(deepcopy(vect))
    vect = list(vect)
    vect += list(np.array(vect)*-1)
    vect = np.array(vect)
    # get the mean, & SD
    v_std = np.std(vect)
    cutoff = v_std * z
    if not positive:
        cutoff *= -1
        cutoff = max([-0.9, cutoff])
    else:
        cutoff = min([0.9, cutoff])
    logger.debug('cutoff: {}'.format(cutoff))
    # TODO edit expected false positive
    if not positive:
        # FPR = 1/scipy.stats.norm.sf(z)
        num_sig = np.sum(np.array(vect_original<cutoff,dtype = bool))
        total_num = vect_original.shape[0]
        logger.debug("{}/{} significant".format(num_sig, total_num))
        FPR = total_num/num_sig
        return cutoff, FPR
    else:
        return cutoff


def get_FPR_cutoff(negative_control_negative_rho_vect, FPR, positive=False):
    # figure out the cutoff to use for a given false positive rate
    negative_control_negative_rho_vect = np.sort(negative_control_negative_rho_vect, kind="mergesort")
    if positive:
        negative_control_negative_rho_vect = negative_control_negative_rho_vect[::-1]
    logger.info("sorting list:")
    logger.debug('{}...{}'.format(negative_control_negative_rho_vect[:5], negative_control_negative_rho_vect[-5:]))
    number_of_total_negative_correlations_in_boot = np.shape(negative_control_negative_rho_vect)[0]
    logger.info("number_of_total_negative_correlations_in_boot")
    logger.info(number_of_total_negative_correlations_in_boot)
    logger.info("FPR:", FPR)

    # this is an emperical cutoff of 1/FPR randomly negative rhos
    neg_cutoff_index = int(max([1, number_of_total_negative_correlations_in_boot / FPR]) - 1)
    logger.debug("cutoff_index")
    logger.info(neg_cutoff_index)

    neg_cutoff = float(negative_control_negative_rho_vect[neg_cutoff_index])
    # print(negative_control_negative_rho_vect[:neg_cutoff_index])
    logger.debug('Rho cutoff: {}'.format(neg_cutoff))
    return neg_cutoff


def pass_cutoff(above, total, num_pos_cor, num_pos_cor_cutoff = 10):
    """append whether oqr not they passed the cutoff to the summary matrix"""
    if above == 0:
        return "False"
    elif (total / above) < (FPR / FPR_MULTIPLE) and num_pos_cor >= num_pos_cor_cutoff:
        return "True"
    else:
        return "False"

# def pass_cutoff(above, total):
#     """append whether or not they passed the cutoff to the summary matrix"""
#     if above == 0:
#         return "False"
#     elif total / above < (FPR / FPR_MULTIPLE):
#         return "True"
#     else:
#         return "False"


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-infile_path", '-i',
                        help="the input dataset for making the adjacency list",
                        required=True)
    parser.add_argument("-adj_list_out", '-o',
                        help="the ouptut adj list",
                        required=True)
    parser.add_argument("-id_list", '-ids',
                        help="the file containing the IDs in the appropriate order",
                        required=True)
    parser.add_argument("-col_ids",
                        help="the file containing column IDs",
                        required=True)
    parser.add_argument("-rho_cutoff", '-rho',
                        help="the cutoff to use for rho values",
                        type=float, require=True)
    parser.add_argument("-hdf5",
                        action='store_true',
                        default=False,
                        help="if the input file is in the hdf5 format")
    parser.add_argument("-block_size",
                        help='how many variables will be used at once for correlation analyzis; this helps keep '
                             'memory requirements down and prevents a segmentation fault memory error',
                        type=int,
                        default=5000)
    parser.add_argument("-time",
                        action='store_true',
                        default=False,
                        help="if you only want to test the speed of relationship detection")
    parser.add_argument("-sc_clust",
                        action='store_true',
                        default=False,
                        help="if scRNAseq clustering is going to be done")
    parser.add_argument("-transpose",
                        action='store_true',
                        default=False,
                        help="if we're going to make an adjacency list on the columns rather than the rows")
    parser.add_argument(
        "-rand_seed", '-seed',
        help='the random number input for random number seed, default = 12345',
        dest='rand_seed',
        type=int,
        default=12345)

    parser.add_argument("-hdf5_out",
                        action='store_true',
                        default=False,
                        help="if we want to make an hdf5 as the output instead of the rho dicts")
    parser.add_argument("-euclidean_dist",
                        action='store_true',
                        default=False,
                        help="if we are going to also calculate the euclidean distance of all points "
                             "against each other and store as an hdf5.")
    parser.add_argument("-row_subset",
                        help="a file with the row indices to include if we only want to do this on a "
                             "subset of the rows. Note that this operation is performed before the "
                             "transpose if need be.")
    parser.add_argument(
        "-rho_dict_dir",
        help='the destination directory', required=True)

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    mat_to_adj_list(rand_seed=args.rand_seed,
                    infile_path=args.infile_path,
                    hdf5=args.hdf5,
                    id_list=args.id_list,
                    row_subset=args.row_subset,
                    block_size=args.block_size,
                    rho_dict_dir=args.rho_dict_dir,
                    adj_list_out=args.adj_list_out,
                    transpose=args.transpose,
                    hdf5_out=args.hdf5_out,
                    euclidean_dist=args.euclidean_dist,
                    rho_cutoff=args.rho_cutoff,
                    sc_clust=args.sc_clust,
                    col_ids=args.col_ids)


def mat_to_adj_list(
        infile_path,
        adj_list_out,
        id_list,
        col_ids,
        rho_cutoff,
        rho_dict_dir=None,
        hdf5=False,
        block_size=5000,
        time_it=False,
        sc_clust=False,
        transpose=False,
        rand_seed=12345,
        euclidean_dist=False,
        row_subset=None,
        hdf5_out=False,
):
    np.random.seed(rand_seed)
    random.seed(rand_seed)
    infile_path = os.path.realpath(infile_path)
    # run spearman analysis
    if hdf5:
        h5f = h5py.File(infile_path, 'r')
        infile = h5f["infile"]
    else:
        infile = np.array(read_table(infile_path))
    if id_list is None and not hdf5:
        id_list = list(infile[1:, 0])
        col_ids = list(infile[0, 1:])
        infile = np.array(infile[1:, 1:], dtype=float)
    elif id_list is not None and not hdf5:
        infile = np.array(infile[1:, 1:], dtype=float)
        id_list_file = id_list
        id_list = read_file(id_list_file, 'lines')
    else:
        id_list_file = id_list
        id_list = read_file(id_list_file, 'lines')
    # subset for the pertinent rows
    if row_subset is not None:
        row_indices = read_file(row_subset, 'lines')
        row_indices = [int(i) for i in row_indices]
        infile = infile[row_indices, :]
    # here's where we'll transpose the input
    if transpose:
        if hdf5:
            infile = np.transpose(infile)
            id_list = read_file(col_ids, 'lines')[1:]  # start at 1 because of the leader column
        else:
            infile = np.transpose(infile)
            id_list = col_ids
    num_vars = len(id_list)
    # sanity check that the ID list length is equal to the row num
    if num_vars != np.shape(infile)[0]:
        logger.warning('the length of the ID list does not equal the number of rows in the input dataset')
        logger.warning("infile: {}".format(np.shape(infile)[0]))
        logger.warning("id_list: {} {}".format(num_vars, id_list[0:5]))
        sys.exit()
    temp_dir = str(infile_path).split('/')
    temp_dir = '/'.join(temp_dir[:-1])
    bin_size = block_size
    bin_size = min([bin_size, len(id_list)])  # this prevents the program from hitting index errors for small datasets
    logger.info('finding the spearman correlations for very big file')
    logger.info('this may take a while if your dataset has lots of variables...')
    if rho_dict_dir is None:
        rho_dict_dir = temp_dir + '/rho_dicts'
    if not os.path.isdir(rho_dict_dir):
        os.mkdir(rho_dict_dir)
    total_vars = len(id_list)
    bins = []
    cur_bin = 0
    while cur_bin < total_vars:
        bins.append(min(cur_bin, total_vars))
        cur_bin += bin_size
    bins.append(total_vars)
    spear_out_hdf5 = None
    if hdf5_out:
        logger.info('making the hdf5 spearman output file')
        # make the hdf5 output file
        hdf5_spear_out_file = rho_dict_dir + "/spearman.hdf5"
        logger.info(hdf5_spear_out_file)
        spear_f = h5py.File(hdf5_spear_out_file, "w")
        try:
            spear_out_hdf5 = spear_f.create_dataset("infile", (total_vars, total_vars), dtype=np.float16)
        except Exception:
            spear_out_hdf5 = spear_f["infile"]
    logger.debug(np.shape(infile))
    logger.debug(infile)
    logger.debug(bins)
    start = time()
    logger.info("finding block...")
    for i in range(0, (len(bins) - 1)):
        for j in range(i, (len(bins) - 1)):
            if (i != j) or (len(bins) == 2):  # True:
                out_rho_dict = rho_dict_dir + "/rho_" + str(bins[i]) + "_to_" + str(bins[i + 1]) + "_vs_" + str(
                    bins[j]) + "_to_" + str(bins[j + 1]) + ".pkl"

                # if we're writing to an hdf5 file, check if the values are already entered
                if hdf5_out:
                    do_top_left = np.all(spear_out_hdf5[bins[i]:bins[i + 1], bins[i]:bins[i + 1]] == 0)
                    do_top_right = np.all(spear_out_hdf5[bins[i]:bins[i + 1], bins[j]:bins[j + 1]] == 0)
                    do_bottom_left = np.all(spear_out_hdf5[bins[j]:bins[j + 1], bins[i]:bins[i + 1]] == 0)
                    do_bottom_right = np.all(spear_out_hdf5[bins[j]:bins[j + 1], bins[j]:bins[j + 1]] == 0)
                    do_one = (do_top_left or do_top_right or do_bottom_left or do_bottom_right)
                    logger.debug('{} {} {} {}'.format(do_top_left, do_top_right, do_bottom_left, do_bottom_right))
                    logger.debug('{} {}'.format(spear_out_hdf5[bins[j]:bins[j + 1], bins[j]:bins[j + 1]]))
                else:
                    do_one = False

                if (not os.path.exists(out_rho_dict) and not hdf5_out) or (hdf5_out and do_one):
                    # find the spearman correlations

                    logger.info('working on {} {} vs {} {}'.format( bins[i], bins[i + 1], bins[j], bins[j + 1]))
                    r = no_p_spear(infile[bins[i]:bins[i + 1], ], infile[bins[j]:bins[j + 1], ], axis=1)

                    # if we're not making the spearman correlation matrix into an hdf5 file - save the dictionary:
                    if not hdf5_out:
                        save_dict(r, out_rho_dict)

                    # if we are making the spearman correlation matrix an hdf5 file, we've got some work to do:
                    else:
                        logger.debug(np.shape(r))
                        if np.shape(r)[0] == 2 * bin_size:
                            # print('is symmetric:',r[bin_size:,bin_size:] == r[:bin_size,:bin_size])
                            if len(bins) == 2:  # False#np.shape(r)[0]<=bin_size:
                                # there is a special case, where the number of variables is less than the
                                # bin size. In this case, the returned matrix will be symmetric in all 4
                                # quadrants, so we'll reduce it down to just the one
                                r = r[bin_size:, bin_size:]
                        logger.debug(np.shape(r))

                        # because h5py's fancy indexing doesn't work very well with slicing in 2D...

                        # first set the top left corner bins
                        if np.all(spear_out_hdf5[bins[i]:bins[i + 1], bins[i]:bins[i + 1]] == 0):
                            logger.info("setting top left")
                            spear_out_hdf5[bins[i]:bins[i + 1], bins[i]:bins[i + 1]] = np.array(r[:bin_size, :bin_size],
                                                                                                dtype=np.float16)

                        # top right
                        if len(bins) != 2:
                            try:
                                if np.all(spear_out_hdf5[bins[i]:bins[i + 1], bins[j]:bins[j + 1]] == 0):
                                    logger.info("setting top right")
                                    spear_out_hdf5[bins[i]:bins[i + 1], bins[j]:bins[j + 1]] = np.array(
                                        r[:bin_size, bin_size:], dtype=np.float16)
                            except Exception:
                                raise Exception('error setting the top right')

                        # bottom left
                        if len(bins) != 2:
                            try:
                                if np.all(spear_out_hdf5[bins[j]:bins[j + 1], bins[i]:bins[i + 1]] == 0):
                                    logger.info('setting bottom left')
                                    spear_out_hdf5[bins[j]:bins[j + 1], bins[i]:bins[i + 1], ] = np.array(
                                        r[bin_size:, :bin_size], dtype=np.float16)
                            except Exception:
                                raise Exception('error setting the bottom left')

                        # bottom right
                        if len(bins) != 2:
                            try:
                                if np.all(spear_out_hdf5[bins[j]:bins[j + 1], bins[j]:bins[j + 1]] == 0):
                                    logger.info('setting bottom right')
                                    spear_out_hdf5[bins[j]:bins[j + 1], bins[j]:bins[j + 1]] = np.array(
                                        r[bin_size:, bin_size:], dtype=np.float16)
                            except Exception:
                                raise Exception('error setting the bottom right')
                else:
                    logger.info('already finished block {} {} vs {} {}'
                                .format(bins[i], bins[i + 1], bins[j], bins[j + 1]))
    end = time()
    if euclidean_dist:

        euclidean_distances = metrics.pairwise.euclidean_distances
        logger.info('making the negative euclidean distance matrix')
        # make the euclidean distance output matrix
        # make the hdf5 output file
        hdf5_euc_out_file = rho_dict_dir + "/neg_euc_dist.hdf5"
        logger.debug(hdf5_euc_out_file)
        euc_f = h5py.File(hdf5_euc_out_file, "w")
        # set up the data matrix (this assumes float32)
        try:
            # neg_euc_out_hdf5 = euc_f.create_dataset("infile", (total_vars,total_vars), dtype=np.float32)
            float_type = np.float16
            neg_euc_out_hdf5 = euc_f.create_dataset("infile", (total_vars, total_vars), dtype=float_type)
        except Exception:
            neg_euc_out_hdf5 = euc_f["infile"]
        # else:
        #     neg_euc_out_hdf5 = euc_f.create_dataset("infile", (total_vars,total_vars), dtype=np.float32)
        # # go through and calculate the negative euclidean distances
        for i in range(0, (len(bins) - 1)):
            for j in range(i, (len(bins) - 1)):
                if (np.all(neg_euc_out_hdf5[bins[i]:bins[i + 1], bins[j]:bins[j + 1]] == 0) and np.all(
                        neg_euc_out_hdf5[bins[j]:bins[j + 1], bins[i]:bins[i + 1]] == 0)):
                    logger.info('calculating negative euclidean distance for {} {} vs {} {}'
                                .format(bins[i], bins[i + 1], bins[j], bins[j + 1]))
                    # temp_neg_euc = -euclidean_distances(np.array(spear_out_hdf5[bins[i]:bins[i+1],:],
                    # dtype=np.float32),np.array(spear_out_hdf5[bins[j]:bins[j+1],:],dtype=np.float32),squared=True)
                    temp_neg_euc = -euclidean_distances(np.array(spear_out_hdf5[bins[i]:bins[i + 1], :],
                                                                 dtype=float_type),
                                                        np.array(spear_out_hdf5[bins[j]:bins[j + 1], :],
                                                                 dtype=float_type),
                                                        squared=True) / np.log2(total_vars)

                    neg_euc_out_hdf5[bins[i]:bins[i + 1], bins[j]:bins[j + 1]] = temp_neg_euc
                    neg_euc_out_hdf5[bins[j]:bins[j + 1], bins[i]:bins[i + 1]] = np.transpose(temp_neg_euc)
                else:
                    logger.info('already finished {} {} vs {} {}'
                                .format(bins[i], bins[i + 1], bins[j], bins[j + 1]))
        for i in range(0, np.shape(neg_euc_out_hdf5)[0]):
            neg_euc_out_hdf5[i, i] = -0.0

        euc_f.close()
    if hdf5_out:
        # close the hdf5 file
        spear_f.close()
    if time_it:
        logger.info('{} minutes spent finding correlations'.format((end - start) / 60))
        sys.exit()
    # read in the ID list
    logger.info('logging all the IDs')
    ID_hash = {}
    for idx, id_val in enumerate(id_list):
        ID_hash[id_val] = idx
    # get the rho dictionaries
    rho_properties_dict = {}
    rho_files = []
    for g in glob.glob(rho_dict_dir + '/*.pkl'):
        rho_files.append(g)
        rho_properties_dict[g] = parse_dict_name(g, id_list)
    # make the adjacency list
    logger.info('making the adjacency list')
    if os.path.isfile(adj_list_out):
        os.remove(adj_list_out)
    if os.path.isfile(adj_list_out[:-4] + "_dedup.tsv"):
        os.remove(adj_list_out[:-4] + "_dedup.tsv")

    out_file = open(adj_list_out[:-4] + "_dedup.tsv", "w")
    # out_file.write('\t'.join(['var_1', 'var_2', 'NA', 'NA','NA','NA','rho'])+'\n')
    out_file.write('\t'.join(['var_1', 'var_2', 'rho']) + '\n')
    # look at the sum of all negative correlations
    sum_neg = []
    all_vars_adj_dict = {}
    neg_vars_adj_dict = {}
    total_neg_vars_adj_dict = {}
    for i in range(0, len(id_list)):
        # [ID, sum_negative_rho, count_below_threshold, total_number_negative,
        # (count_below_threshold+1)/(count_above_threshold +1)]
        sum_neg.append([id_list[i], 0, 0, 0, 0])
        all_vars_adj_dict[id_list[i]] = np.zeros(len(id_list, ), dtype=bool)
        neg_vars_adj_dict[id_list[i]] = np.zeros(len(id_list, ), dtype=bool)
        total_neg_vars_adj_dict[id_list[i]] = np.zeros(len(id_list, ), dtype=bool)
    do_sum_neg = sc_clust
    neg_cutoff = None
    if do_sum_neg or rho_cutoff is None:
        # run the bootstrap negative control for finding sum negative relationships
        # first get a bin_size random set of variables to do the negative control on
        neg_control_sample_idxs = np.arange(len(id_list))
        # remove any genes that are zero in all samples
        non_zero_idxs = np.where(np.sum(infile, axis=1) > 0)[0]
        neg_control_sample_idxs = neg_control_sample_idxs[non_zero_idxs]
        np.random.shuffle(neg_control_sample_idxs)
        bin_size = min([bin_size, np.shape(neg_control_sample_idxs)[0]])
        neg_control_sample_idxs = neg_control_sample_idxs[:bin_size]
        neg_control_sample_idxs = neg_control_sample_idxs.tolist()
        neg_control_sample_idxs.sort()

        # subset these IDs
        neg_control_subset_mat = np.array(infile[neg_control_sample_idxs, :])
        logger.debug('making negative control bootstrap shuffled matrix {}'.format(np.shape(neg_control_subset_mat)))

        # go through each row and shuffle them within variables
        for i in range(0, np.shape(neg_control_subset_mat)[0]):
            temp_row_shuffle_order = np.arange(np.shape(neg_control_subset_mat)[1])
            np.random.shuffle(temp_row_shuffle_order)
            temp_row_shuffle_order = temp_row_shuffle_order.tolist()

            neg_control_subset_mat[i, :] = neg_control_subset_mat[i, temp_row_shuffle_order]

        # get the spearman_rhos for the shuffled matrix
        r = no_p_spear(neg_control_subset_mat, neg_control_subset_mat, axis=1)
        r = r[:bin_size, :bin_size]
        logger.debug("rho shape {}".format(np.shape(r)))
        # remove self correlations
        upper_tiangle_indices = np.triu_indices(bin_size, k=1)

        # subset the negative ones
        linear_rho = r[upper_tiangle_indices]
        # calculate the median absolute deviation
        # calculate_mad(linear_rho)

        negative_control_negative_rho_vect = linear_rho[np.where(linear_rho < 0)[0]]
        negative_control_positive_rho_vect = linear_rho[np.where(linear_rho > 0)[0]]
        # calculate_mad(negative_control_negative_rho_vect)

        # plot the average Rho per negative correlation
        neg_cor_density_x, neg_cor_density_y = get_density(negative_control_negative_rho_vect)
        pos_cor_density_x, pos_cor_density_y = get_density(negative_control_positive_rho_vect)

        sum_neg_plot = os.path.dirname(adj_list_out) + '/boostrap_neg_cor_rhos.png'
        plot_one_density(neg_cor_density_x, neg_cor_density_y, sum_neg_plot,
                         label="density of negative correlation null distribution")
        pos_rho_plot = os.path.dirname(adj_list_out) + '/boostrap_pos_cor_rhos.png'
        plot_one_density(pos_cor_density_x, pos_cor_density_y, pos_rho_plot,
                         label="density of positive correlation null distribution")

        # plot the distribution of all bootstrap negative control Rhos
        neg_cor_density_x, neg_cor_density_y = get_density(linear_rho)
        sum_neg_plot = os.path.dirname(adj_list_out) + '/boostrap_cor_rhos.png'
        plot_one_density(neg_cor_density_x, neg_cor_density_y, sum_neg_plot,
                         label="density of all correlation null distribution")

        logger.info("calculating negative correlation FPR cutoff from null distribution:")
        # neg_cutoff = get_FPR_cutoff(negative_control_negative_rho_vect,FPR)
        neg_cutoff, FPR = get_Z_cutoff(negative_control_negative_rho_vect)
        logger.info('Empiric FPR: {}'.format(FPR))
        logger.info("calculating positive correlation FPR cutoff from null distribution:")
        # pos_cutoff = get_FPR_cutoff(negative_control_positive_rho_vect,pos_FPR, positive = True)
        pos_cutoff = get_Z_cutoff(negative_control_positive_rho_vect, z=5.5, positive=True)
        rho_cutoff = pos_cutoff

    for rho in rho_files:
        # keep track of how many positive correlations each var has
        num_pos_cor_dict = {str(temp_id):0 for idx, temp_id in enumerate(id_list)}
        # go through all the rho dict files
        logger.info('reading and processing {}'.format(rho))
        temp_rho_mat = import_dict(rho)
        temp_indices, temp_IDs = rho_properties_dict[rho]
        for i in range(0, np.shape(temp_rho_mat)[0]):
            # go through each line in the rho matrix and log the pertinent indices
            cur_i_variable = temp_IDs[i]
            sig_indices = np.where(np.absolute(temp_rho_mat[i, :]) > rho_cutoff)[0]
            sig_IDs = [temp_IDs[j] for j in sig_indices]
            sig_rhos = temp_rho_mat[i, sig_indices]

            # go through the significant indicies
            for q, sig_id in enumerate(sig_IDs):
                if sig_id != cur_i_variable:
                    if ID_hash[cur_i_variable] > ID_hash[sig_id]:
                        # check if variable pair has been done already
                        var2_idx = ID_hash[sig_id]
                        # check if it has been done (ie: currently set to zero.)
                        if all_vars_adj_dict[cur_i_variable][var2_idx] == 0:
                            # this is the indicator that we've done it already
                            all_vars_adj_dict[cur_i_variable][var2_idx] += 1
                            out_file.write("{}\t{}\t{}\n".format(cur_i_variable, sig_id, sig_rhos[q]))
                            # also log the positive correlations
                            num_pos_cor_dict[str(cur_i_variable)] = 1 + num_pos_cor_dict[str(cur_i_variable)]
                            num_pos_cor_dict[str(sig_IDs[q])] = 1 + num_pos_cor_dict[str(sig_IDs[q])]

            if do_sum_neg:
                # do the same for all of the negative correlations, regardless of magnitude
                total_negative_indices = np.where(temp_rho_mat[i, :] < 0)[0]
                neg_IDs = [temp_IDs[j] for j in total_negative_indices]
                neg_rhos = temp_rho_mat[i, total_negative_indices]
                for q, neg_rho in enumerate(neg_rhos):
                    if ID_hash[cur_i_variable] > ID_hash[neg_IDs[q]]:

                        # check if variable pair has been done already
                        var2_idx = ID_hash[neg_IDs[q]]
                        if total_neg_vars_adj_dict[cur_i_variable][var2_idx] == 0:
                            # total number of Rho <0
                            total_neg_vars_adj_dict[cur_i_variable][var2_idx] += 1
                            sum_neg[ID_hash[cur_i_variable]][3] += 1
                            sum_neg[var2_idx][3] += 1

                            # then if it's actually less than the simulation determined cutoff, log it for that as well
                            if neg_rho < neg_cutoff:  # and neg_rhos[q]>-1 :
                                sum_neg[ID_hash[cur_i_variable]][1] += neg_rho
                                sum_neg[var2_idx][1] += neg_rho

                                sum_neg[ID_hash[cur_i_variable]][2] += 1
                                sum_neg[var2_idx][2] += 1
    out_file.close()
    if do_sum_neg:
        # sum up the negative correlations for the bootstrap shuffled negative control
        negative_control_sum_neg = []
        negative_control_count_neg = []
        for i in range(0, np.shape(r)[0]):
            neg_indices = np.where(r[i, :] < 0)[0]
            count_neg = np.shape(neg_indices)[0]
            negative_control_count_neg.append(count_neg)
            sum_neg_rhos = sum(r[i, neg_indices])
            negative_control_sum_neg.append(sum_neg_rhos)

        # calculate the average per comparison for each gene, then multiply it by the number of comparisons
        # in the full dataset. This extrapolates the subset to the size of the full dataset.
        # this will then be used to dynamicaly set the cutoff of what counts as a negative correlation
        # at the desired FDR (default FDR = 0.05)
        negative_control_sum_neg = (np.array(negative_control_sum_neg) / bin_size) * len(id_list)
        max_neg_ctr = max(negative_control_sum_neg)
        logger.info("max of the negative control = {}".format(max_neg_ctr))

        # process the sum negative table into a vector so that we can do interesting things with it
        logger.debug(sum_neg[:10])
        sum_neg_vect = np.zeros(len(sum_neg), dtype=float)
        sum_neg_count = np.zeros(len(sum_neg), dtype=float)
        sum_neg_count_total = np.zeros(len(sum_neg), dtype=float)
        non_sig_neg_count = np.zeros(len(sum_neg), dtype=float)
        for i in range(0, len(sum_neg)):
            sum_neg_vect[i] = sum_neg[i][1]
            sum_neg_count[i] = sum_neg[i][2]
            sum_neg_count_total[i] = sum_neg[i][3]
            non_sig_neg_count[i] = sum_neg[i][3] - sum_neg[i][2]

        sum_neg_vect = np.array(sum_neg_vect)
        sum_neg_count = np.array(sum_neg_count)
        sum_neg_count_total = np.array(sum_neg_count_total)
        count_above_threshold = sum_neg_count_total - sum_neg_count
        count_above_below_ratio = (sum_neg_count + 1) / (count_above_threshold + 1)

        # log the ratios in the output file
        for i in range(0, len(sum_neg)):
            sum_neg[i][4] = count_above_below_ratio[i]

        logger.debug(sum_neg_vect[:10])

        # given the total number of observed negative correlations (expected_y)
        logger.debug("max of sum_neg_count_total {}".format(np.max(sum_neg_count_total)))
        expected_y = np.arange(np.max(sum_neg_count_total))
        expected_x = expected_y / (FPR / FPR_MULTIPLE)
        expected_x = np.log2(expected_x)
        expected_x = expected_x[1:]
        expected_y = expected_y[1:]
        logger.debug(expected_y)
        logger.debug(expected_x)
        sum_neg_count = np.log2(sum_neg_count + 1)

        for i in range(0, len(sum_neg)):
            # format of sum_neg:
            # [ID, sum_negative_rho, count_below_threshold, total_number_negative,
            # (count_below_threshold+1)/(count_above_threshold +1)]
            sum_neg[i].append(pass_cutoff(sum_neg[i][2], sum_neg[i][3], num_pos_cor_dict[sum_neg[i][0]]))

        # plot the ratio of negative correlations below and above the bootstrap shuffled determined cutoff
        logger.debug('count_above_below_ratio {}'.format(count_above_below_ratio))
        neg_cor_density_x, neg_cor_density_y = get_density(count_above_below_ratio)

        sum_neg_plot = os.path.dirname(adj_list_out) + '/ratio_of_neg_cor_above_below_cutoff.png'
        plot_one_density(neg_cor_density_x, neg_cor_density_y, sum_neg_plot)

        # plot the counts of negative correlations against
        neg_cor_density_x, neg_cor_density_y = get_density(count_above_below_ratio)

        sum_neg_plot = os.path.dirname(adj_list_out) + '/ratio_of_neg_cor_above_below_cutoff.png'
        plot_one_density_x_cutoff(neg_cor_density_x, neg_cor_density_y, 1 / 1000, sum_neg_plot)

        # plot the counts of significant vs non-significant
        count_sig_vs_non_sig = os.path.dirname(adj_list_out) + '/sig_neg_count_vs_total_neg_count.png'
        density_scatter(sum_neg_count, sum_neg_count_total, count_sig_vs_non_sig, log_density=4, expected_x=expected_x,
                        expected_y=expected_y)

        # write the results to file
        sum_neg_out = os.path.dirname(adj_list_out) + '/sum_neg_cor.txt'
        write_table(sum_neg, sum_neg_out)


if __name__ == "__main__":
    main()
