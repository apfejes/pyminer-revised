import sys
from time import time, sleep

import argparse
import logging
import gzip
import numpy as np
import os
import random
from gprofiler import GProfiler

from modules.common_functions import read_file, strip_split, read_table, write_table
from modules.references import SPECIES_CODES, HSAPIENS

all_calls = []

logger = logging.getLogger(__name__)


def check_infile(infile):
    if os.path.isfile(infile):
        return
    else:
        raise Exception('could not find {}' .format(infile))


def outfile_exists(outfile):
    if not os.path.isfile(outfile):
        return False
    statinfo = os.stat(outfile)
    return True if statinfo.st_size else False


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument(
        "-gene_list_files",
        help='a comma separated list of files that contain the enriched genes for each cell type.',
        type=str)

    parser.add_argument(
        "-gene_table", '-bool_table', '-in_table',
        help='A boolean True False table indicating whether a gene should be included in each cell types list.',
        type=str)

    parser.add_argument(
        "-organism", '-species', '-s',
        help='which organism are we using. (Use the -species_codes to print out a list of all supported '
             'species and their codes).',
        type=str,
        default=HSAPIENS)

    parser.add_argument(
        "-species_codes",
        help='print out a list of all supported species and their codes.',
        action='store_true',
        default=False)

    parser.add_argument(
        "-convert_to_human",
        help='if using a non-mouse/human species, it may be useful to convert to human, as these interactions '
             'will be more complete. Note that you will still have to provide an -organism argument so that '
             'we know what species to convert from.',
        action='store_true',
        default=False)

    parser.add_argument(
        "-out_dir", '-out', '-o',
        help='The output directory to use. If it does not exist, it will be created.',
        type=str)

    parser.add_argument(
        "-stringdb_dir", '-sdb',
        help='The directory containing the StringDB action files',
        type=str,
        default='/usr/local/lib/cell_signals/')

    args = parser.parse_args()

    return args


def get_name_from_path(in_path):
    temp_name = in_path.split('/')
    temp_name = temp_name[-1]
    temp_name = temp_name[:-4]
    return temp_name


def get_gene_lists_from_files(all_gene_list_files):
    if not isinstance(all_gene_list_files, list):
        all_gene_list_files = all_gene_list_files.split(',')

    cell_type_ids = []
    for gene_list in all_gene_list_files:
        cell_type_ids.append(get_name_from_path(gene_list))

    # get the gene lists for each cell type
    gene_lists = []
    gene_ids = []
    for cell in all_gene_list_files:
        temp_genes = read_file(cell)
        gene_lists.append(temp_genes)
        gene_ids += temp_genes
    gene_ids = list(set(gene_ids))
    return cell_type_ids, gene_ids, gene_lists


def get_gene_lists_from_bool_table(in_file):
    """
    here, we take in the boolean table with columns corresponding
    to the cell type names, with the body of the table consisting of
    True or False. This will be directly translated into a boolean numpy array
    so syntax is important in this array.
    """
    bool_table = read_table(in_file)
    cell_type_ids = bool_table[0][1:]
    bool_table = np.array(bool_table)
    gene_ids = list(set(bool_table[1:, 0].tolist()))
    gene_lists = [[] for _ in range(1, np.shape(bool_table)[1])]
    # populate
    for i in range(1, np.shape(bool_table)[0]):
        # go through each row in the table (barring the )
        for j in range(1, np.shape(bool_table)[1]):
            if bool_table[i, j] == 'True':
                gene_lists[j - 1].append(bool_table[i, 0])

    # make them all upper case
    for i in range(0, len(gene_ids)):
        gene_ids[i] = str(gene_ids[i]).upper()

    for i in range(0, len(gene_lists)):
        for j in range(0, len(gene_lists[i])):
            gene_lists[i][j] = str(gene_lists[i][j]).upper()

    return cell_type_ids, gene_ids, gene_lists


def get_unique_ids(i, out_table):
    gene_1 = out_table[i][2]
    cell_type_1 = out_table[i][1]
    gene_2 = out_table[i][6]
    cell_type_2 = out_table[i][5]
    inuqe_id_1 = cell_type_1 + "|" + gene_1 + ":" + cell_type_2 + "|" + gene_2
    inuqe_id_2 = cell_type_2 + "|" + gene_2 + ":" + cell_type_1 + "|" + gene_1
    return inuqe_id_1, inuqe_id_2


def get_subset(out_file, term_a, term_b, interaction_title_line, out_table):
    temp_file_str = out_file
    if os.path.isfile(temp_file_str):
        os.remove(temp_file_str)
    extra_cell_file = open(temp_file_str, 'w')
    temp_title_line = '\t'.join(interaction_title_line[0])
    extra_cell_file.write(temp_title_line + '\n')
    # extracellular_extracellular_interactions = interaction_title_line[:]
    for i in range(1, len(out_table)):
        if (out_table[i][4] == term_a and out_table[i][8] == term_b) or (
                out_table[i][4] == term_b and out_table[i][8] == term_a):
            temp_line = out_table[i][:]
            temp_line = '\t'.join(temp_line)
            if i != len(out_table) - 1:
                temp_line += '\n'
            extra_cell_file.write(temp_line)
    extra_cell_file.close()


def annotate_gprofiler_output(output_gprofiler, cell_type_idx_dict, gene_symbol_list_dicts, symbol_interaction_dict):
    logger.info('annotating the gprofiler results - This might take a minute...')
    new_output_gprofiler = [output_gprofiler[0] + [output_gprofiler[0][-1]]]
    new_output_gprofiler[0][-2] = 'filtered_annotated_gene_list'
    first = True
    count = 0
    total_length = len(output_gprofiler)
    for line in output_gprofiler:
        count += 1
        if count % 500 == 0:
            logger.info('line: {}\tpercent:{}%'.format(count, 100 * count / total_length))
        if first:
            first = False
        else:
            # put the cell types and annotations into the line, then append it to the new output
            # but only add it for either all autocrine interactions, or for paracrine interactions
            # only put it into the output if there are
            temp_line = line[:]
            temp_line.append(temp_line[-1])
            temp_line[-2] = filter_and_annoatate_genes(temp_line[-1], temp_line[0], temp_line[1],
                                                       symbol_interaction_dict, cell_type_idx_dict,
                                                       gene_symbol_list_dicts)
            if temp_line[-2] != "None":
                new_output_gprofiler.append(temp_line)
    return new_output_gprofiler


def filter_and_annoatate_genes(in_gene_list, cell_type_1, cell_type_2, symbol_interaction_dict,
                               cell_type_idx_dict, gene_symbol_list_dicts, test_time=False):
    if cell_type_1 != cell_type_2:
        paracrine = True
    else:
        paracrine = False
    in_gene_list = in_gene_list.split(',')
    if len(in_gene_list) == 1:
        return "None"
    cell_type_1_idx = cell_type_idx_dict[cell_type_1]
    cell_type_2_idx = cell_type_idx_dict[cell_type_2]
    # cell_type_enriched_ids_1=gene_symbol_lists[cell_type_1_idx]
    # cell_type_enriched_ids_2=gene_symbol_lists[cell_type_2_idx]
    cell_type_enriched_dict_1 = gene_symbol_list_dicts[cell_type_1_idx]
    cell_type_enriched_dict_2 = gene_symbol_list_dicts[cell_type_2_idx]

    # first make all fully crossed interaction_pairs
    start = None
    if test_time:
        logger.info('making pairs')
        start = time()
    fully_crossed_possible_pairs = []
    for i in range(0, len(in_gene_list)):
        for j in range(i, len(in_gene_list)):
            # if in_gene_list[j]+":"+in_gene_list[i] not in fully_crossed_possible_pairs:
            fully_crossed_possible_pairs.append(in_gene_list[i] + ":" + in_gene_list[j])
            # fully_crossed_possible_pairs.append(in_gene_list[j]+":"+in_gene_list[i])
    fully_crossed_possible_pairs = list(set(fully_crossed_possible_pairs))
    filtered_pairs = []
    if test_time:
        logger.info('time_taken {}'.format(time() - start))
        logger.info('looking at fully_crossed_possible_pairs in interaction dict')
        start = time()
    for i in range(0, len(fully_crossed_possible_pairs)):
        cont = True
        try:
            symbol_interaction_dict[fully_crossed_possible_pairs[i]]
        except Exception:
            cont = False
        if cont:  # fully_crossed_possible_pairs[i] in symbol_interaction_dict:
            filtered_pairs.append(fully_crossed_possible_pairs[i])
    if test_time:
        logger.info('time_taken {}'.format(time() - start))
        logger.warning('annotating source')
        start = time()
    final_output_list = []
    for i in range(0, len(filtered_pairs)):
        # format of this will be cell_type_1:gene1;cell_type2:gene2,... for each
        # interaction contained within the genes in this pathway
        temp_interaction = filtered_pairs[i]
        temp_interaction = temp_interaction.split(':')
        gene_source_1 = ''
        gene_source_2 = ''
        # figure out which cell type is producing each within the pair
        if temp_interaction[0] in cell_type_enriched_dict_1:
            gene_source_1 += cell_type_1
        if temp_interaction[0] in cell_type_enriched_dict_2:
            # temp_interaction[0] in cell_type_enriched_ids_2:
            if gene_source_1 != '':
                gene_source_1 += '&'
            gene_source_1 += cell_type_2
        if temp_interaction[1] in cell_type_enriched_dict_1:
            # if temp_interaction[1] in cell_type_enriched_ids_1:
            gene_source_2 += cell_type_1
        if temp_interaction[1] in cell_type_enriched_dict_2:
            # if temp_interaction[1] in cell_type_enriched_ids_2:
            if gene_source_2 != '':
                gene_source_2 += '&'
            gene_source_2 += cell_type_2
        # double check that there was a gene source found
        if gene_source_1 == '':
            logger.warning("couldn't find gene_source_1")
        if gene_source_2 == '':
            logger.warning("couldn't find gene_source_2")
        # format the outputs for this line
        if paracrine:
            temp_full_interaction_description = gene_source_1 + ":" + temp_interaction[0] + ";" + \
                                                gene_source_2 + ":" + temp_interaction[1]
        else:
            temp_full_interaction_description = cell_type_1 + ":" + temp_interaction[0] + ";" + cell_type_2 + ":" + \
                                                temp_interaction[1]
        if paracrine and gene_source_1 != gene_source_2:
            final_output_list.append(temp_full_interaction_description)
        elif not paracrine:
            final_output_list.append(temp_full_interaction_description)
    if test_time:
        logger.info('time_taken {}'.format(time() - start))
    # and return the final line
    if not final_output_list:
        return 'None'
    return ','.join(final_output_list)


def get_gprofiler_on_interaction_lists(interaction_lists, cell_type_ids, organism, gp, bg=None):
    logger.info('getting the pathway analysis for each cell type interactions...')
    output_gprofiler = [['cell_type_1', 'cell_type_2']]
    for i in range(0, len(interaction_lists)):
        # wait for a random amount of time so that we don't overload the server
        random_time_lag = np.random.randint(low=25, high=100) / 100
        sleep(random_time_lag)
        for j in range(i, len(interaction_lists)):
            logger.debug("{}\t{}: {} genes".format(cell_type_ids[i], cell_type_ids[j], len(interaction_lists[i][j])))
            if bg is None:
                temp_gprofile_results = gp.gprofile(interaction_lists[i][j], organism=organism)
            else:
                temp_gprofile_results = gp.gprofile(interaction_lists[i][j], custom_bg=bg, organism=organism)
            if i == 0 and j == 0 and temp_gprofile_results != []:
                output_gprofiler[0] += temp_gprofile_results[0]
            for q in range(1, len(temp_gprofile_results)):
                output_gprofiler.append([cell_type_ids[i], cell_type_ids[j]] + temp_gprofile_results[q])
    return output_gprofiler


def convert_to_ensp(in_genes, gene_lists, organism, gp):
    temp_lookup = {}
    gene_symbol_dict = {}
    for g in in_genes:
        temp_lookup[g] = []

    results = gp.gconvert(in_genes, organism=organism, target='ENSP')

    for r in range(1, len(results)):
        # the original ID
        g = results[r][1]
        # the current orthologue list
        try:
            temp_lookup[g]
        except Exception:
            pass
        else:
            if results[r][4] is not None:
                gene_symbol_dict[results[r][3]] = results[r][4]
            if results[r][3] is not None:
                temp_list = temp_lookup[g]
                temp_list += [results[r][3]]
                temp_lookup[g] = temp_list

    gene_lists = convert_list_of_lists_by_dict(gene_lists, temp_lookup)
    new_gene_ids = []
    for k in list(temp_lookup.keys()):
        new_gene_ids += temp_lookup[k]
    new_gene_ids = list(set(new_gene_ids))

    for gene in new_gene_ids:
        if gene not in gene_symbol_dict:
            gene_symbol_dict[gene] = "None"

    return new_gene_ids, gene_lists, gene_symbol_dict


def get_orthologue_dicts(in_genes, source_organism, target_organism, gp):
    temp_org_to_human = {}

    # each gene will get a list for its orthologues
    for g in in_genes:
        temp_org_to_human[g] = []

    results = gp.gorth(in_genes,
                       source_organism=source_organism,
                       target_organism=target_organism,
                       numeric_ns=True)

    for r in range(1, len(results)):

        # the original ID
        g = results[r][1]
        # the current orthologue list
        try:
            temp_org_to_human[g]
        except Exception:
            pass
        else:
            if results[r][4] is not None:
                temp_list = temp_org_to_human[g]
                temp_list += [results[r][4]]
                temp_org_to_human[g] = temp_list

    human_to_temp_org = {}
    return temp_org_to_human, human_to_temp_org


def convert_list_of_lists_by_dict(gene_lists, temp_org_to_human):
    # set up the new gene list
    new_gene_lists = [[] for _ in range(len(gene_lists))]
    # convert the original gene lists to the IDs
    for i in range(len(gene_lists)):
        for j in range(len(gene_lists[i])):
            new_gene_lists[i] += temp_org_to_human[gene_lists[i][j]]
    return new_gene_lists


def update_converted_genes(temp_org_to_human, gene_lists):
    new_gene_ids = []
    for k in list(temp_org_to_human.keys()):
        new_gene_ids += temp_org_to_human[k]
    new_gene_ids = list(set(new_gene_ids))
    if None in new_gene_ids:
        new_gene_ids.pop()

    new_gene_lists = convert_list_of_lists_by_dict(gene_lists, temp_org_to_human)

    return new_gene_ids, new_gene_lists


def get_receptors_and_secreted(gene_ids, organism, gp):
    secreted_go = ['GO:0005615', 'GO:0005576', 'GO:0044421']
    receptor_go = ["GO:0009897", "GO:0031232", "GO:0031233", "GO:0071575", " GO:0098591", "GO:0031362", "GO:0098567",
                   "GO:0009986", "GO:0005886", "GO:0042923", "GO:0016021"]  # ,'GO:0005904']

    results = gp.gconvert(gene_ids, organism=organism, target='GO')

    all_go_terms = []
    receptor_list = []
    secreted_list = []
    for i in range(1, len(results)):
        if results[i][3] not in all_go_terms:
            all_go_terms.append(results[i][3])
        if results[i][3] in receptor_go:
            receptor_list.append(results[i][1])
        if results[i][3] in secreted_go:
            secreted_list.append(results[i][1])
    receptor_list = list(set(receptor_list))
    secreted_list = list(set(secreted_list))
    return receptor_list, secreted_list


def filter_enriched_lists_for_subcellular_localization(receptor_list, secreted_list, gene_lists):
    receptor_gene_lists = [[] for _ in range(len(gene_lists))]
    secreted_gene_lists = [[] for _ in range(len(gene_lists))]

    for i, gene_list in enumerate(gene_lists):
        for j, gene in enumerate(gene_list):
            if gene in receptor_list:
                receptor_gene_lists[i].append(gene)
            if gene in secreted_list:
                secreted_gene_lists[i].append(gene)
    return receptor_gene_lists, secreted_gene_lists


def clean_names(in_vect):
    name1 = in_vect[0].split('.')
    name2 = in_vect[1].split('.')
    name1 = name1[-1]
    name2 = name2[-1]
    in_vect[0] = name1
    in_vect[1] = name2
    return in_vect


def subset_action_table(receptor_or_secreted_list, db_file):
    logger.info('this may take a minute....')
    # construct a dictionary for the receptor or secreted list
    receptor_or_secreted_dict = {j: i for i, j in enumerate(receptor_or_secreted_list)}
    logger.info('{} receptor_or_secreted_dict'.format(len(list(receptor_or_secreted_dict.keys()))))
    all_ids = []
    out_table = []
    activation_dict = {}
    inhibition_dict = {}
    other_dict = {}
    first = True

    if db_file.endswith('.gz'):
        open_cmd = gzip.open
        gz = True
    else:
        open_cmd = open
        gz = False

    with open_cmd(db_file, 'r') as db_fh:
        for line_count, line in enumerate(db_fh):
            if gz:
                line = line.decode('utf-8')

            if line_count % 1e5 == 0:
                logger.info('\t', line_count, '\t', len(out_table))
            temp_line = strip_split(line)
            if first:
                first = False
                # out_table.append(temp_line)
            else:
                temp_line = clean_names(temp_line)
                cont1 = True
                try:
                    receptor_or_secreted_dict[temp_line[0]]
                except Exception:
                    cont1 = False
                if cont1:
                    all_ids.append(temp_line[0])

                cont2 = True
                try:
                    receptor_or_secreted_dict[temp_line[1]]
                except Exception:
                    cont2 = False
                if cont2:
                    all_ids.append(temp_line[1])

                if cont1 and cont2:
                    # if temp_line[0] in receptor_or_secreted_list and temp_line[1] in receptor_or_secreted_list:
                    temp_id = temp_line[0] + ":" + temp_line[1]
                    if temp_line[2] == 'binding':
                        out_table.append(temp_line)
                    elif temp_line[2] == 'activation':
                        activation_dict[temp_id] = True
                    elif temp_line[2] == 'inhibition':
                        inhibition_dict[temp_id] = True
                    else:
                        if temp_id not in other_dict:
                            other_dict[temp_id] = temp_line[2]
                        else:
                            temp_thingy = None
                            if other_dict[temp_id] != 'None':
                                temp_thingy = other_dict[temp_id]
                                temp_thingy = ',' + temp_line[2]
                            if temp_thingy[0] == ',':
                                temp_thingy = temp_thingy[1:]
                            other_dict[temp_id] = temp_thingy

    all_ids = list(set(all_ids))
    return out_table, activation_dict, inhibition_dict, other_dict, all_ids


def annotate_interactions_for_activation_and_inhibition(action_table, activation_dict, inhibition_dict, other_dict):
    logger.info('annotating the interactions for directionality and type of action....')
    action_table[0] = action_table[0][:11] + ['activation/inhibition', 'other_action'] + action_table[0][11:]
    for i in range(1, len(action_table)):
        temp_id = str(action_table[i][9]) + ":" + str(action_table[i][10])
        activation_term = 'None'
        other_action_term = 'None'
        if temp_id in activation_dict:
            activation_term = 'activation'
        if temp_id in inhibition_dict:
            activation_term = 'inhibition'
        if temp_id in other_dict:
            other_action_term = other_dict[temp_id]
        action_table[i] = action_table[i][:11] + [activation_term, other_action_term] + action_table[i][11:]
    return action_table


def gene_lists_to_symbol_lists(gene_lists, gene_symbol_dict):
    gene_symbol_lists = []
    for c in range(len(gene_lists)):
        gene_symbol_lists.append([])
        for gene in gene_lists[c]:
            temp_symbol = gene_symbol_dict[gene]
            if temp_symbol not in gene_symbol_lists[c]:
                gene_symbol_lists[c].append(temp_symbol)
    return gene_symbol_lists


def get_empty_interaction_lists(gene_lists):
    interaction_lists = []
    for i in range(0, len(gene_lists)):
        interaction_lists.append([])
        for j in range(0, len(gene_lists)):
            interaction_lists[i].append([])
    return interaction_lists


def gene_symbol_lists_to_dicts(gene_symbol_lists):
    gene_symbol_list_dicts = []
    for gene_symbol_list in gene_symbol_lists:
        gene_symbol_list_dicts.append({j: w for w, j in enumerate(gene_symbol_list)})
    return gene_symbol_list_dicts


def get_cell_types_gene_enriched_in(gene, gene_list_dict, cell_type_ids):
    gene_cell_types = []
    for i, ct_id in enumerate(cell_type_ids):
        cur_cell_type = True
        try:
            gene_list_dict[i][gene]
        except Exception:
            cur_cell_type = False
        if cur_cell_type:
            gene_cell_types.append(ct_id)
    return gene_cell_types


def main():
    args = parse_arguments()

    cell_signals(organism=args.organism,
                 species_codes=args.species_codes,
                 gene_table=args.gene_table,
                 stringdb_dir=args.stringdb_dir,
                 out_dir=args.out_dir,
                 gene_list_files=args.gene_list_files)


def cell_signals(out_dir,
                 gene_table,
                 organism=HSAPIENS,
                 species_codes=False,
                 stringdb_dir='/usr/local/lib/cell_signals/',
                 gene_list_files=None,
                 ):
    organism_action_files = {
        HSAPIENS: '9606.protein.actions.v10.5.txt.gz',
        'mmusculus': '10090.protein.actions.v10.5.txt.gz'
    }

    if organism not in organism_action_files:
        convert_to_human = True
    else:
        convert_to_human = False
    gp = GProfiler('PyMINEr_' + str(random.randint(0, int(1e6))), want_header=True)
    if species_codes:
        logger.critical(SPECIES_CODES)
        sys.exit()
    gene_lists = None
    gene_ids = None
    cell_type_ids = None
    db_file = None
    if gene_table is not None:
        cell_type_ids, gene_ids, gene_lists = get_gene_lists_from_bool_table(gene_table)
    if gene_list_files is not None:
        cell_type_ids, gene_ids, gene_lists = get_gene_lists_from_files(gene_list_files)

    # print out some basic summaries on the input data
    logger.info('{} cell types found'.format(len(cell_type_ids)))
    logger.debug("{}...".format(cell_type_ids[:min([4, len(cell_type_ids)])]))
    logger.info('genes found'.format(len(gene_ids)))
    logger.debug("{}...".format(gene_ids[:min([4, len(gene_ids)])]))
    # make a hash table to look up the index from a cell type name
    cell_type_idx_dict = {key: value for value, key in enumerate(cell_type_ids)}
    # if we need to convert over to human ids, this is the first thing we'll need to do
    if convert_to_human:
        logger.info('converting to human')
        temp_org_to_human, human_to_temp_org = get_orthologue_dicts(gene_ids, source_organism=organism,
                                                                    target_organism=HSAPIENS, gp=gp)
        # get_orthologue_dicts(gene_ids[:10], organism, HSAPIENS)

        # update the gene_ids and gene_lists to reflect the conversion to human
        gene_ids, gene_lists = update_converted_genes(temp_org_to_human, gene_lists)
        organism = HSAPIENS
        logger.info('finished converting IDs')

        logger.debug('gene IDs now look like:')
        logger.debug("{}...".format(gene_ids[:4]))
        logger.debug('enrichment lists now look like:')
        logger.debug(cell_type_ids[0])
        logger.debug(gene_lists[0][:3])
        logger.debug(cell_type_ids[1])
        logger.debug(gene_lists[1][:3])
    logger.info('Converting IDs to ENSP format.')
    logger.info('This is needed for cross-referencing the StringDB database later')
    gene_ids, gene_lists, gene_symbol_dict = convert_to_ensp(gene_ids, gene_lists, gp=gp, organism=organism)
    gene_symbol_lists = gene_lists_to_symbol_lists(gene_lists, gene_symbol_dict)
    gene_symbol_list_dicts = gene_symbol_lists_to_dicts(gene_symbol_lists)
    logger.info('finished converting to ENSPs')
    logger.debug('gene IDs now look like:')
    logger.debug(gene_ids[:4])
    logger.debug('enrichment lists now look like:')
    logger.debug(cell_type_ids[0])
    logger.debug(gene_lists[0][:3])
    logger.debug(cell_type_ids[1])
    logger.debug(gene_lists[1][:3])
    symbols = list(gene_symbol_dict.keys())
    logger.debug(len(symbols))
    for i in range(0, 5):
        logger.debug('{} {}'.format(symbols[i], gene_symbol_dict[symbols[i]]))
    # go term filter for subcellular localization   #
    logger.info('getting subcellular localizations')
    receptor_list, secreted_list = get_receptors_and_secreted(gene_ids, organism, gp)
    logger.info('found {} plasma membrane'.format(len(receptor_list)))
    logger.info('found {} extracellular'.format(len(secreted_list)))
    # if there are any extracellular genes in the plasma membrane gene lists, pop them out
    # we only want loose ligands in the secreted list
    for r in receptor_list:
        if r in secreted_list:
            logger.debug('removing {} {} from secreted, because it\'s also annotated as being in the plasma membrane'
                          .format(r, gene_symbol_dict[r]))
            secreted_list.pop(secreted_list.index(r))
    logger.info('found {} plasma membrane'.format(len(receptor_list)))
    logger.info('found {} extracellular'.format(len(secreted_list)))
    receptor_or_secreted_list = list(set(receptor_list + secreted_list))
    logger.info('{} receptors and secreted genes found'.format(len(receptor_or_secreted_list)))

    # set up a dictionary with the info needed about each gene for writing to file later
    gene_detail_dict = {}
    for g in gene_ids:
        if g in receptor_list:
            gene_detail_dict[g] = [g, gene_symbol_dict[g], 'plasma_membrane']
        if g in secreted_list:
            gene_detail_dict[g] = [g, gene_symbol_dict[g], 'extracellular']

    # subset the enriched gene lists for each subcellular localization
    receptor_gene_lists, secreted_gene_lists = \
        filter_enriched_lists_for_subcellular_localization(receptor_list, secreted_list, gene_lists)

    # print out some summaries
    logger.info('plasma membrane:')
    for i in range(0, len(cell_type_ids)):
        temp_list_len = len(receptor_gene_lists[i])
        logger.info('{}\t{}'.format(cell_type_ids[i], temp_list_len))
        if temp_list_len != 0:
            logger.debug(receptor_gene_lists[i][0:min([temp_list_len, 3])])
    logger.info('extracellular:')
    for i in range(0, len(cell_type_ids)):
        temp_list_len = len(secreted_gene_lists[i])
        logger.info('{}\t{}'.format(cell_type_ids[i], temp_list_len))
        if temp_list_len != 0:
            logger.debug(secreted_gene_lists[i][0:min([temp_list_len, 3])])

    #   cross reference all of the receptor and secreted   ############
    #      lists with the StringDB action database        ############
    if organism in organism_action_files:
        db_file = stringdb_dir + organism_action_files[organism]

    # first we'll load into memory only the interactions that stand
    # a chance of being pertinent to our dataset
    logger.info('loading in the interaction table')
    action_table, activation_dict, inhibition_dict, other_dict, all_included_bg = subset_action_table(
        receptor_or_secreted_list, db_file)
    logger.info('interactions among the genes remaining'.format(len(action_table) - 1))

    # convert that to an action dictionary so that we can look up the details quickly later
    # first make a unique key for the interaction lists
    action_dict = {}
    symbol_interaction_dict = {}
    for line in action_table:
        temp_id = line[0] + ":" + line[1]
        temp_symbol_id = gene_symbol_dict[line[0]] + ":" + gene_symbol_dict[line[1]]
        if temp_symbol_id not in symbol_interaction_dict:
            symbol_interaction_dict[temp_symbol_id] = True
        if temp_id not in action_dict:
            action_dict[temp_id] = [line]
        else:
            temp_list = action_dict[temp_id]
            temp_list.append(line)
            action_dict[temp_id] = temp_list
    interaction_lists = get_empty_interaction_lists(gene_lists)
    interaction_title_line = [["autocrine_paracrine",
                               "cell_type_1", "gene_1", "gene_1_symbol", "gene_1_location",
                               "cell_type_2", "gene_2", "gene_2_symbol", "gene_2_location",
                               "gene1", "gene2", "action", "inhibition", "directional", "is_direction", "score"]]
    out_summary_table = [["cell_type_1", "cell_type_2", "count"]]
    out_table = interaction_title_line[:]

    # make the cell type enrichment dicts
    gene_list_dict = []
    for i in range(0, len(gene_lists)):
        temp_gene_dict = {j: i for i, j in enumerate(gene_lists[i])}
        gene_list_dict.append(temp_gene_dict)
    # go through the action_dict & find what cell types are enriched for each of the genes
    cell_type_interaction_count = np.zeros((len(cell_type_ids), len(cell_type_ids)))
    all_interactions = list(action_dict.keys())
    logger.info('cataloging which cell types have the interactions')
    for interaction_idx in range(0, len(all_interactions)):
        if interaction_idx % 500 == 0:
            logger.debug('\t', interaction_idx, '\t', interaction_idx / len(all_interactions))
        interaction = all_interactions[interaction_idx]
        genes = interaction.split(':')
        gene1 = genes[0]
        gene2 = genes[1]
        gene1_symbol = gene_symbol_dict[gene1]
        gene2_symbol = gene_symbol_dict[gene2]
        # go through all of the cell types and find which cell types are enriched for the genes
        gene1_cell_types = get_cell_types_gene_enriched_in(gene1, gene_list_dict=gene_list_dict,
                                                           cell_type_ids=cell_type_ids)
        gene2_cell_types = get_cell_types_gene_enriched_in(gene2, gene_list_dict=gene_list_dict,
                                                           cell_type_ids=cell_type_ids)

        # write the output for each of the combinations of cell types
        for cell_type_1 in gene1_cell_types:
            for cell_type_2 in gene2_cell_types:
                if cell_type_1 == cell_type_2:
                    out_line = ['autocrine']
                else:
                    out_line = ['paracrine']
                cell_type_1_idx = cell_type_idx_dict[cell_type_1]
                cell_type_2_idx = cell_type_idx_dict[cell_type_2]
                interaction_lists[cell_type_1_idx][cell_type_2_idx].append(gene1_symbol)
                # if gene2_symbol not in interaction_lists[i][j]:
                interaction_lists[cell_type_1_idx][cell_type_2_idx].append(gene2_symbol)
                out_line += [cell_type_1] + gene_detail_dict[gene1]
                out_line += [cell_type_2] + gene_detail_dict[gene2]
                for line in action_dict[gene1 + ":" + gene2]:
                    out_table.append(out_line + line)

                cell_type_interaction_count[cell_type_1_idx, cell_type_2_idx] += 1
    for i in range(0, len(cell_type_ids)):
        cell_type_1 = cell_type_ids[i]
        for j in range(i, len(cell_type_ids)):
            cell_type_2 = cell_type_ids[j]

            temp_count = cell_type_interaction_count[i, j]

            out_summary_table.append([cell_type_1, cell_type_2, temp_count])
    # will need to catalogue the number remaining
    # out_summary_table.append([cell_type_1,cell_type_2,temp_count])
    # annotate the action table for the mechanism of interaction
    out_table = annotate_interactions_for_activation_and_inhibition(out_table, activation_dict, inhibition_dict,
                                                                    other_dict)
    # remove the duplicates in the interaction_lists:
    for i in range(0, len(interaction_lists)):
        for j in range(0, len(interaction_lists[i])):
            interaction_lists[i][j] = list(set(interaction_lists[i][j]))
    out_summary_table_copy = out_summary_table[:]
    out_table_copy = out_table[:]
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    write_table(out_table_copy, out_dir + '/all_cell_type_specific_interactions.txt')
    write_table(out_summary_table_copy, out_dir + '/all_cell_cell_interaction_summary.txt')
    # get gprofiler results from each of the interaction lists
    output_gprofiler = get_gprofiler_on_interaction_lists(interaction_lists, bg=all_included_bg, gp=gp,
                                                          organism=organism, cell_type_ids=cell_type_ids)
    output_gprofiler = annotate_gprofiler_output(output_gprofiler, cell_type_idx_dict,
                                                 gene_symbol_list_dicts=gene_symbol_list_dicts,
                                                 symbol_interaction_dict=symbol_interaction_dict)
    write_table(output_gprofiler, out_dir + '/all_cell_type_specific_interactions_gprofiler.txt')
    logger.info('writing output files')
    logger.info('extracellular\textracellular')
    temp_file_str = out_dir + '/extracellular_extracellular_cell_type_specific_interactions.txt'
    get_subset(temp_file_str, 'extracellular', 'extracellular', interaction_title_line, out_table=out_table)
    logger.info('plasma_membrane\tplasma_membrane')
    temp_file_str = out_dir + '/plasma_membrane_plasma_membrane_cell_type_specific_interactions.txt'
    get_subset(temp_file_str, 'plasma_membrane', 'plasma_membrane', interaction_title_line, out_table=out_table)

    # extracellular:plasma membrane interactions
    logger.info('extracellular\tplasma_membrane')
    prev_interact_dict = {}
    extracellular_pm_interactions = interaction_title_line[:]
    for i in range(1, len(out_table)):
        inuqe_id_1, inuqe_id_2 = get_unique_ids(i, out_table)

        # temp_line not in extracellular_pm_interactions:
        if inuqe_id_1 not in prev_interact_dict and inuqe_id_2 not in prev_interact_dict:
            if out_table[i][4] == 'extracellular' and out_table[i][8] == 'plasma_membrane':
                extracellular_pm_interactions.append(out_table[i])
                prev_interact_dict[inuqe_id_1] = True
                prev_interact_dict[inuqe_id_2] = True
            elif out_table[i][4] == 'plasma_membrane' and out_table[i][8] == 'extracellular':
                # make it a directional file so that things only flow from extracellular to plasma membrane
                temp_line = out_table[i][:]
                temp_cell_description = temp_line[1:5]
                temp_line[1:5] = temp_line[5:9]
                temp_line[5:9] = temp_cell_description
                extracellular_pm_interactions.append(temp_line)
                prev_interact_dict[inuqe_id_1] = True
                prev_interact_dict[inuqe_id_2] = True
    extracellular_pm_interactions_copy = extracellular_pm_interactions[:]
    write_table(extracellular_pm_interactions_copy,
                out_dir + '/extracellular_plasma_membrane_cell_type_specific_interactions.txt')


if __name__ == "__main__":
    main()
