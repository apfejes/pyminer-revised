import argparse
import fileinput
import random
from matplotlib import use
import logging

use('Agg')

import scipy
import numpy as np

from modules.common_functions import read_file, strip_split, write_table

RANDOM_SEED = 12345
logger=logging.getLogger(__name__)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-adjacency_list", "-adj", "-adj_list",
                        help="the adjacency list ",
                        required=True)
    parser.add_argument("-nodes_of_interest", "-noi", "-nodes",
                        help="the nodes that you want to subset and get their degrees of separation from")
    parser.add_argument("-ID_list", "-id_list", "-ids", "-IDs",
                        help="the list of all IDs that made the network",
                        required=True)
    parser.add_argument("-out_dir",
                        help="the directory to place the output file(s)")
    parser.add_argument("-all_distances",
                        help="if you want to write a file that has all distances as a matrix. "
                             "(note that the IDs will be in the same order as the input -ID_list)",
                        action='store_true',
                        default=False)
    parser.add_argument("-random_seed",
                        help="Seed for the random number generator (default=12345)",
                        default=RANDOM_SEED)
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()

    get_degrees_of_separation(adjacency_list=args.adjacency_list,
                              nodes_of_interest=args.nodes_of_interest,
                              id_list=args.id_list,
                              out_dir=args.out_dir,
                              all_distances=args.all_distances)


def get_degrees_of_separation(adjacency_list, nodes_of_interest, id_list, out_dir,
                              random_seed=RANDOM_SEED, all_distances=False):
    random.seed(random_seed)
    np.random.seed(random_seed)

    if nodes_of_interest is not None:
        nodes_of_interest = nodes_of_interest.split(',')

    id_hash = {}
    id_list = read_file(id_list, "lines")
    for idx, id_val in enumerate(id_list):
        id_hash[id_val] = idx

    if not out_dir:
        out_dir = adjacency_list.split('/')
        out_dir = '/'.join(out_dir[:-1]) + '//'

    logger.info('making the adjacency matrix')

    # first read in the adjacency list & make an adjacency matrix
    # make an empty adjacency matrix
    g_dense = np.zeros((len(id_list), len(id_list)), dtype=bool)

    # go through the adjacency list and populate the adjacnecy matrix
    first_line = True
    for line in fileinput.input(adjacency_list):
        if not first_line:
            temp_line = strip_split(line)
            idx1 = id_hash[temp_line[0]]
            idx2 = id_hash[temp_line[1]]
            g_dense[idx1, idx2] = 1
            g_dense[idx2, idx1] = 1
        else:
            first_line = False

    # get all shortest paths from & to all nodes          #
    logger.info('finding all of the shortest paths')
    G_sparse = scipy.sparse.csr_matrix(g_dense)
    short_path_mat = scipy.sparse.csgraph.shortest_path(G_sparse)
    if all_distances:
        temp_outfile = out_dir + '/all_shortest_paths_matrix.txt'
        write_table(short_path_mat, temp_outfile)

    # subset the distance matrix to get all distances from the nodes of interest
    header = ['genes']
    out_array = np.transpose(np.array([id_list]))
    if nodes_of_interest:
        for n in nodes_of_interest:
            if n not in id_hash:
                raise Exception('could not find {} in the adjacency matrix'.format(n))
            else:
                header.append('dist_to_' + str(n))
                # get the distances
                temp_dist_vect = short_path_mat[:, id_hash[n]]

                # make the shortest list for output
                out_array = np.hstack((out_array, np.transpose(np.array([temp_dist_vect]))))
    out_array = np.vstack((np.array([header]), out_array))
    temp_outfile = out_dir + '/' + 'genes_of_interest_shortest_path_list.txt'
    write_table(out_array, temp_outfile)


if __name__ == "__main__":
    main()
