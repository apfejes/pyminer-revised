
from time import time
from queue import Empty
from multiprocessing import Queue, Lock, Process, Value
import logging

TERMINATOR = "TERMINATOR_STRING"
CHECK_DEATH_INTERVAL = 20  # seconds

logger = logging.getLogger(__name__)


def process_monitor(error_queue, process_list):
    """
    Use this to monitor the running daemons.
    """
    error = None

    while process_list:
        try:
            error = error_queue.get(timeout=1)
        except Empty:
            pass

        if error:
            logger.error(error)

        while process_list:
            next_process = process_list[0]
            next_process.join(timeout=0.1)
            if not next_process.is_alive():
                process_list.pop()
            else:
                continue


def process_monitor_w_output(error_queue, process_list, output_queue):
    """
    Use this to monitor the running daemons.
    """
    error = None
    output_collected = []
    while process_list:
        get_output = True
        try:
            error = error_queue.get(timeout=0.5)
            logger.error("error found: {}".format(error))
        except Empty:
            pass

        while get_output:
            try:
                row = output_queue.get(timeout=0.5)
                output_collected.append(row)
                if output_collected and len(output_collected) % 100 == 0:
                    logger.debug("{} records processed".format(len(output_collected)))
            except Empty:
                get_output = False

        if error:
            logger.error(error)

        while process_list and not process_list[0].is_alive():
            try:
                process_list[0].join(timeout=0.1)
                process_list.pop()
            except Empty:
                continue

    logger.info("All processes joined.")
    return output_collected


def timed_check_death_signal(last_check_time, death_signal, close_down):
    if time() > last_check_time + CHECK_DEATH_INTERVAL:
        if death_signal.value():
            close_down()
        return time()

    return last_check_time


class LockedInteger(object):
    def __init__(self, value=0):
        self.lock = Lock()
        self.cur_val = Value("i", value)

    def increment(self, increment=1):
        with self.lock:
            self.cur_val.value += increment

    def value(self):
        with self.lock:
            return self.cur_val.value


class LockedBoolean(object):
    def __init__(self, value=False):
        self.lock = Lock()
        self.cur_val = Value("b", value)

    def true(self):
        with self.lock:
            self.cur_val.value = True

    def false(self):
        with self.lock:
            self.cur_val.value = False

    def value(self):
        with self.lock:
            return self.cur_val.value


class GenericProcessParent(object):
    def __init__(self):
        self.process_list = []
        self.death_obj = LockedBoolean()

        self.error_queue = Queue(100)
        self.terminal_queue = Queue(100)

    def launch_process(self, process, process_count=1, **kwargs):

        temp_proc_name = kwargs.get("process_name", None)
        temp_input_queue = kwargs.get("input_queue", None)

        for process_number in range(process_count):
            if "process_name" in kwargs:
                kwargs["process_name"] = temp_proc_name + " " + str(process_number)
                logger.info(f"launching {process_count} processes - {kwargs['process_name']}")
            if "input_queue" in kwargs:
                kwargs["input_queue"] = temp_input_queue[process_number]
            target = process(**kwargs)
            proc = Process(target=target.run)
            proc.daemon = True
            proc.start()

            self.process_list.append(proc)

    def close_down(self):

        # something else here
        while self.process_list:
            proc = self.process_list.pop()
            proc.join()
            logger.info(f"joined Process - {len(self.process_list)} processes left!")

    def do_something(self):
        pass

    def run_loop(self):
        last_check_time = time()
        while self.process_list:
            try:
                error = self.error_queue.get(timeout=0.5)
                logger.error(error)
                self.death_obj.value = True

            except Empty:
                pass

            last_check_time = timed_check_death_signal(last_check_time, self.death_obj, self.close_down)

            try:
                terminal = self.terminal_queue.get(timeout=0.5)
                if terminal == TERMINATOR:
                    self.close_down()
            except Empty:
                pass

            self.do_something()


class GenericProcess(object):
    def __init__(
        self, incoming_queue, outgoing_queue, error_queue, death_obj, terminator_obj, terminators_expected, process_name
    ):

        self.incoming_queue = incoming_queue
        self.outgoing_queue = outgoing_queue
        self.error_queue = error_queue
        self.death_signal = death_obj

        self.terminators_received = terminator_obj
        self.terminators_expected = terminators_expected
        self.process_name = process_name

    def process_items(self, item):
        """ You must implement this"""
        pass

    def start_up(self):
        """ You must implement this, if you want something to happen at the start."""
        pass

    def close_down(self):
        """ You must implement this"""
        while True:
            try:
                item = self.incoming_queue.get(block=True, timeout=1)
                if item == TERMINATOR:
                    logger.info(f"process {self.process_name} received a terminator")
                    self.terminators_received.increment(1)
                    continue

            except Empty:
                if self.terminators_expected == self.terminators_received.value():
                    break
        self.close_and_place_terminators()

    def close_and_place_terminators(self):
        logger.info(f"process {self.process_name} placing terminator")
        self.outgoing_queue.put(TERMINATOR)

    def run(self):

        last_check_time = time()
        self.start_up()

        while True:
            try:
                item = self.incoming_queue.get(block=True, timeout=1)

                if item == TERMINATOR:
                    logger.info(f"process {self.process_name} received a terminator")
                    self.terminators_received.increment(1)
                    logger.info(f"currently terminators received = {self.terminators_received.value()}")
                    continue

                last_check_time = timed_check_death_signal(last_check_time, self.death_signal, self.close_down)

                self.process_items(item)

            except Empty:
                if self.terminators_expected == self.terminators_received.value():
                    logger.info(
                        f"{self.terminators_received.value()}/{self.terminators_expected} "
                        f"terminators received for {self.process_name}"
                    )
                    break
                last_check_time = timed_check_death_signal(last_check_time, self.death_signal, self.close_down)

        self.close_and_place_terminators()
        logger.info(f"process {self.process_name} is done")
