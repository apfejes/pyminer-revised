HSAPIENS = 'hsapiens'

SUPPORTED_ORGANISMS = ["aaegypti", "acarolinensis", "agambiae", "amelanoleuca", "amexicanus", "aplatyrhynchos",
                       "btaurus", "celegans", "cfamiliaris", "choffmanni", "cintestinalis", "cjacchus", "cporcellus",
                       "csabaeus", "csavignyi", "dmelanogaster", "dnovemcinctus", "dordii", "drerio", "ecaballus",
                       "eeuropaeus", "etelfairi", "falbicollis", "fcatus", "gaculeatus", "ggallus", "ggorilla",
                       "gmorhua", "hsapiens", "itridecemlineatus", "lafricana", "lchalumnae", "loculatus",
                       "mdomestica", "meugenii", "mfuro", "mgallopavo", "mlucifugus", "mmulatta", "mmurinus",
                       "mmusculus", "nleucogenys", "oanatinus", "oaries", "ocuniculus", "ogarnettii", "olatipes",
                       "oniloticus", "oprinceps", "pabelii", "panubis", "pcapensis", "pformosa", "pmarinus",
                       "psinensis", "ptroglodytes", "pvampyrus", "rnorvegicus", "saraneus", "scerevisiae",
                       "sharrisii", "sscrofa", "tbelangeri", "tguttata", "tnigroviridis", "trubripes", "tsyrichta",
                       "ttruncatus", "vpacos", "xmaculatus", "xtropicalis", "aclavatus", "aflavus", "afumigatus",
                       "afumigatusa1163", "agossypii", "anidulans", "aniger", "aoryzae", "aterreus", "bcinerea",
                       "bgraminis", "cgloeosporioides", "cgraminicola", "chigginsianum", "cneoformans", "corbiculare",
                       "dseptosporum", "fculmorum", "ffujikuroi", "fgraminearum", "foxysporum", "fpseudograminearum",
                       "fsolani", "fverticillioides", "ggraminis", "kpastoris", "lmaculans", "mlaricipopulina",
                       "moryzae", "mpoae", "mviolaceum", "ncrassa", "nfischeri", "pgraminis", "pgraminisug99",
                       "pnodorum", "pstriiformis", "pteres", "ptriticina", "ptriticirepentis", "scerevisiae",
                       "scryophilus", "sjaponicus", "soctosporus", "spombe", "sreilianum", "ssclerotiorum",
                       "tmelanosporum", "treesei", "tvirens", "umaydis", "vdahliae", "vdahliaejr2", "ylipolytica",
                       "ztritici", "aaegypti", "acephalotes", "adarlingi", "agambiae", "aglabripennis", "amellifera",
                       "apisum", "aqueenslandica", "avaga", "bantarctica", "bimpatiens", "bmalayi", "bmori",
                       "cbrenneri", "cbriggsae", "celegans", "cgigas", "cjaponica", "cquinquefasciatus", "cremanei",
                       "cteleta", "dananassae", "derecta", "dgrimshawi", "dmelanogaster", "dmojavensis",
                       "dpersimilis", "dplexippus", "dponderosae", "dpseudoobscura", "dpulex", "dsechellia",
                       "dsimulans", "dvirilis", "dwillistoni", "dyakuba", "hmelpomene", "hrobusta", "iscapularis",
                       "lanatina", "lcuprina", "lgigantea", "lloa", "lsalmonis", "mcinxia", "mdestructor", "mleidyi",
                       "mscalaris", "nvectensis", "nvitripennis", "obimaculoides", "ovolvulus", "phumanus",
                       "ppacificus", "rprolixus", "sinvicta", "smansoni", "smaritima", "smimosarum", "spurpuratus",
                       "sratti", "sscabiei", "tadhaerens", "tcastaneum", "tkitauei", "tspiralis", "turticae",
                       "znevadensis", "alyrata", "atauschii", "athaliana", "atrichopoda", "bdistachyon", "bnapus",
                       "boleracea", "brapa", "bvulgaris", "ccrispus", "cmerolae", "creinhardtii", "gmax",
                       "gsulphuraria", "hvulgare", "lperrieri", "macuminata", "mtruncatula", "obarthii",
                       "obrachyantha", "oglaberrima", "oglumaepatula", "oindica", "olongistaminata", "olucimarinus",
                       "omeridionalis", "onivara", "opunctata", "orufipogon", "osativa", "ppatens", "ppersica",
                       "ptrichocarpa", "sbicolor", "sitalica", "slycopersicum", "smoellendorffii", "stuberosum",
                       "taestivum", "tcacao", "tpratense", "turartu", "vvinifera", "zmays"]

SPECIES_CODES = """
Ensembl

    aaegypti — Aedes aegypti
    acarolinensis — Anolis carolinensis
    agambiae — Anopheles gambiae
    amelanoleuca — Ailuropoda melanoleuca
    amexicanus — Astyanax mexicanus
    aplatyrhynchos — Anas platyrhynchos
    btaurus — Bos taurus
    celegans — Caenorhabditis elegans
    cfamiliaris — Canis familiaris
    choffmanni — Choloepus hoffmanni
    cintestinalis — Ciona intestinalis
    cjacchus — Callithrix jacchus
    cporcellus — Cavia porcellus
    csabaeus — Chlorocebus sabaeus
    csavignyi — Ciona savignyi
    dmelanogaster — Drosophila melanogaster
    dnovemcinctus — Dasypus novemcinctus
    dordii — Dipodomys ordii
    drerio — Danio rerio
    ecaballus — Equus caballus
    eeuropaeus — Erinaceus europaeus
    etelfairi — Echinops telfairi
    falbicollis — Ficedula albicollis
    fcatus — Felis catus
    gaculeatus — Gasterosteus aculeatus
    ggallus — Gallus gallus
    ggorilla — Gorilla gorilla
    gmorhua — Gadus morhua
    hsapiens — Homo sapiens
    itridecemlineatus — Ictidomys tridecemlineatus
    lafricana — Loxodonta africana
    lchalumnae — Latimeria chalumnae
    loculatus — Lepisosteus oculatus
    mdomestica — Monodelphis domestica
    meugenii — Macropus eugenii
    mfuro — Mustela putorius furo
    mgallopavo — Meleagris gallopavo
    mlucifugus — Myotis lucifugus
    mmulatta — Macaca mulatta
    mmurinus — Microcebus murinus
    mmusculus — Mus musculus
    nleucogenys — Nomascus leucogenys
    oanatinus — Ornithorhynchus anatinus
    oaries — Ovis aries
    ocuniculus — Oryctolagus cuniculus
    ogarnettii — Otolemur garnettii
    olatipes — Oryzias latipes
    oniloticus — Oreochromis niloticus
    oprinceps — Ochotona princeps
    pabelii — Pongo abelii
    panubis — Papio anubis
    pcapensis — Procavia capensis
    pformosa — Poecilia formosa
    pmarinus — Petromyzon marinus
    psinensis — Pelodiscus sinensis
    ptroglodytes — Pan troglodytes
    pvampyrus — Pteropus vampyrus
    rnorvegicus — Rattus norvegicus
    saraneus — Sorex araneus
    scerevisiae — Saccharomyces cerevisiae
    sharrisii — Sarcophilus harrisii
    sscrofa — Sus scrofa
    tbelangeri — Tupaia belangeri
    tguttata — Taeniopygia guttata
    tnigroviridis — Tetraodon nigroviridis
    trubripes — Takifugu rubripes
    tsyrichta — Tarsius syrichta
    ttruncatus — Tursiops truncatus
    vpacos — Vicugna pacos
    xmaculatus — Xiphophorus maculatus
    xtropicalis — Xenopus tropicalis

Ensembl Genomes Fungi

    aclavatus — Aspergillus clavatus
    aflavus — Aspergillus flavus
    afumigatus — Aspergillus fumigatus
    afumigatusa1163 — Aspergillus fumigatusa1163
    agossypii — Ashbya gossypii
    anidulans — Aspergillus nidulans
    aniger — Aspergillus niger
    aoryzae — Aspergillus oryzae
    aterreus — Aspergillus terreus
    bcinerea — Botrytis cinerea
    bgraminis — Blumeria graminis f. sp. hordei DH14
    cgloeosporioides — Colletotrichum gloeosporioides
    cgraminicola — Colletotrichum graminicola
    chigginsianum — Colletotrichum higginsianum
    cneoformans — Cryptococcus neoformans
    corbiculare — Colletotrichum orbiculare
    dseptosporum — Dothistroma septosporum
    fculmorum — Fusarium culmorum UK99
    ffujikuroi — Fusarium fujikuroi
    fgraminearum — Fusarium graminearum
    foxysporum — Fusarium oxysporum
    fpseudograminearum — Fusarium pseudograminearum
    fsolani — Fusarium solani
    fverticillioides — Fusarium verticillioides
    ggraminis — Gaeumannomyces graminis
    kpastoris — Komagataella pastoris
    lmaculans — Leptosphaeria maculans
    mlaricipopulina — Melampsora larici-populina 98AG31
    moryzae — Magnaporthe oryzae
    mpoae — Magnaporthe poae
    mviolaceum — Microbotryum violaceum p1A1 Lamole
    ncrassa — Neurospora crassa
    nfischeri — Neosartorya fischeri
    pgraminis — Puccinia graminis
    pgraminisug99 — Puccinia graminis Ug99
    pnodorum — Phaeosphaeria nodorum
    pstriiformis — Puccinia striiformis f. sp. tritici PST-130 str. Race 130
    pteres — Pyrenophora teres f. teres 0-1
    ptriticina — Puccinia triticina
    ptriticirepentis — Pyrenophora tritici-repentis Pt-1C-BFP
    scerevisiae — Saccharomyces cerevisiae
    scryophilus — Schizosaccharomyces cryophilus
    sjaponicus — Schizosaccharomyces japonicus
    soctosporus — Schizosaccharomyces octosporus
    spombe — Schizosaccharomyces pombe
    sreilianum — Sporisorium reilianum SRZ2
    ssclerotiorum — Sclerotinia sclerotiorum
    tmelanosporum — Tuber melanosporum
    treesei — Trichoderma reesei
    tvirens — Trichoderma virens
    umaydis — Ustilago maydis
    vdahliae — Verticillium dahliae
    vdahliaejr2 — Verticillium dahliae JR2
    ylipolytica — Yarrowia lipolytica
    ztritici — Zymoseptoria tritici

Ensembl Genomes Metazoa

    aaegypti — Aedes aegypti
    acephalotes — Atta cephalotes
    adarlingi — Anopheles darlingi
    agambiae — Anopheles gambiae
    aglabripennis — Anoplophora glabripennis
    amellifera — Apis mellifera
    apisum — Acyrthosiphon pisum
    aqueenslandica — Amphimedon queenslandica
    avaga — Adineta vaga
    bantarctica — Belgica antarctica
    bimpatiens — Bombus impatiens
    bmalayi — Brugia malayi
    bmori — Bombyx mori
    cbrenneri — Caenorhabditis brenneri
    cbriggsae — Caenorhabditis briggsae
    celegans — Caenorhabditis elegans
    cgigas — Crassostrea gigas
    cjaponica — Caenorhabditis japonica
    cquinquefasciatus — Culex quinquefasciatus
    cremanei — Caenorhabditis remanei
    cteleta — Capitella teleta
    dananassae — Drosophila ananassae
    derecta — Drosophila erecta
    dgrimshawi — Drosophila grimshawi
    dmelanogaster — Drosophila melanogaster
    dmojavensis — Drosophila mojavensis
    dpersimilis — Drosophila persimilis
    dplexippus — Danaus plexippus
    dponderosae — Dendroctonus ponderosae
    dpseudoobscura — Drosophila pseudoobscura
    dpulex — Daphnia pulex
    dsechellia — Drosophila sechellia
    dsimulans — Drosophila simulans
    dvirilis — Drosophila virilis
    dwillistoni — Drosophila willistoni
    dyakuba — Drosophila yakuba
    hmelpomene — Heliconius melpomene
    hrobusta — Helobdella robusta
    iscapularis — Ixodes scapularis
    lanatina — Lingula anatina
    lcuprina — Lucilia cuprina
    lgigantea — Lottia gigantea
    lloa — Loa loa
    lsalmonis — Lepeophtheirus salmonis
    mcinxia — Melitaea cinxia
    mdestructor — Mayetiola destructor
    mleidyi — Mnemiopsis leidyi
    mscalaris — Megaselia scalaris
    nvectensis — Nematostella vectensis
    nvitripennis — Nasonia vitripennis
    obimaculoides — Octopus bimaculoides
    ovolvulus — Onchocerca volvulus
    phumanus — Pediculus humanus
    ppacificus — Pristionchus pacificus
    rprolixus — Rhodnius prolixus
    sinvicta — Solenopsis invicta
    smansoni — Schistosoma mansoni
    smaritima — Strigamia maritima
    smimosarum — Stegodyphus mimosarum
    spurpuratus — Strongylocentrotus purpuratus
    sratti — Strongyloides ratti
    sscabiei — Sarcoptes scabiei
    tadhaerens — Trichoplax adhaerens
    tcastaneum — Tribolium castaneum
    tkitauei — Thelohanellus kitauei
    tspiralis — Trichinella spiralis
    turticae — Tetranychus urticae
    znevadensis — Zootermopsis nevadensis

Ensembl Genomes Plants

    alyrata — Arabidopsis lyrata
    atauschii — Aegilops tauschii
    athaliana — Arabidopsis thaliana
    atrichopoda — Amborella trichopoda
    bdistachyon — Brachypodium distachyon
    bnapus — Brassica napus
    boleracea — Brassica oleracea
    brapa — Brassica rapa
    bvulgaris — Beta vulgaris subsp. vulgaris
    ccrispus — Chondrus crispus
    cmerolae — Cyanidioschyzon merolae
    creinhardtii — Chlamydomonas reinhardtii
    gmax — Glycine max
    gsulphuraria — Galdieria sulphuraria
    hvulgare — Hordeum vulgare
    lperrieri — Leersia perrieri
    macuminata — Musa acuminata
    mtruncatula — Medicago truncatula
    obarthii — Oryza barthii
    obrachyantha — Oryza brachyantha
    oglaberrima — Oryza glaberrima
    oglumaepatula — Oryza glumaepatula
    oindica — Oryza sativa indica
    olongistaminata — Oryza longistaminata
    olucimarinus — Ostreococcus lucimarinus
    omeridionalis — Oryza meridionalis
    onivara — Oryza nivara
    opunctata — Oryza punctata
    orufipogon — Oryza rufipogon
    osativa — Oryza sativa Japonica
    ppatens — Physcomitrella patens
    ppersica — Prunus persica
    ptrichocarpa — Populus trichocarpa
    sbicolor — Sorghum bicolor
    sitalica — Setaria italica
    slycopersicum — Solanum lycopersicum
    smoellendorffii — Selaginella moellendorffii
    stuberosum — Solanum tuberosum
    taestivum — Triticum aestivum
    tcacao — Theobroma cacao
    tpratense — Trifolium pratense
    turartu — Triticum urartu
    vvinifera — Vitis vinifera
    zmays — Zea mays
    """
