"""
A library of functions that are used in the Scripts for PyMiner's auxilary scripts.  Reduced code duplication and makes
maintenance easier. """
import os
import pickle
from subprocess import check_call, Popen, PIPE
import logging

logger = logging.getLogger(__name__)


def run_cmd(in_message, com=True, stdout=None):
    msg = '\n{}\n'.format(" ".join(in_message))
    logger.info(msg)
    if stdout:
        with open(stdout, 'w') as out:
            process = Popen(in_message, stdout=PIPE)
            while True:
                line = process.stdout.readline().decode("utf-8")
                out.write(line)
                if line == '' and process.poll() is not None:
                    break
    if com:
        Popen(in_message).communicate()
    else:
        check_call(in_message)


def read_file(file_name, lines_or_raw='lines'):
    """ basic function library """
    logger.info("reading file " + file_name)
    lines = None
    with open(file_name, 'r') as file_handle:
        if lines_or_raw == 'lines':
            lines = file_handle.readlines()
            for i, line in enumerate(lines):
                lines[i] = line.strip('\n')
        elif lines_or_raw == 'raw':
            lines = file_handle.read()
    return lines


def make_file(contents, path):
    with open(path, 'w') as file_handle:
        if isinstance(contents, list):
            file_handle.writelines(contents)
        elif isinstance(contents, str):
            file_handle.write(contents)


def flatten_2D_table(table, delim):
    if str(type(table)) == "<class 'numpy.ndarray'>":
        out = []
        for i, row in enumerate(table):
            out.append([])
            for j, cell in enumerate(row):
                try:
                    str(cell)
                except ValueError:
                    logger.error(cell)
                else:
                    out[i].append(str(cell))
            out[i] = delim.join(out[i]) + '\n'
        return out
    else:
        new_table = []
        for i, row in enumerate(table):
            new_row = []
            for j, cell in enumerate(row):
                try:
                    str(cell)
                except ValueError:
                    logger.error(cell)
                else:
                    new_row.append(str(cell))
            new_table.append(delim.join(new_row) + '\n')
        return new_table


def strip_split(line, delim='\t'):
    return line.strip('\n').split(delim)


def make_table(lines, delim, range_min=0):
    for i in range(range_min, len(lines)):
        lines[i] = lines[i].strip()
        lines[i] = lines[i].split(delim)
        for j in range(range_min, len(lines[i])):
            try:
                float(lines[i][j])
            except ValueError:
                lines[i][j] = lines[i][j].replace('"', '')
            else:
                lines[i][j] = float(lines[i][j])
    return lines


def get_file_path(in_path):
    in_path = in_path.split('/')
    in_path = in_path[:-1]
    in_path = os.sep.join(in_path)
    return in_path + os.sep


def read_table(file, sep='\t'):
    return make_table(read_file(file, 'lines'), sep)


def write_table(table, out_file, sep='\t'):
    make_file(flatten_2D_table(table, sep), out_file)


def import_dict(file_handle):
    with open(file_handle, 'rb') as file_handle:
        data = pickle.load(file_handle)
    return data


def save_dict(data, path):
    with open(path, 'wb') as file_handle:
        pickle.dump(data, file_handle)



