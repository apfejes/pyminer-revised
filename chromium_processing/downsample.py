from collections import Counter

import argparse
import os
import random
from matplotlib import use
import logging
use('Agg')

import matplotlib.pyplot as plt
import numpy as np

from modules.common_functions import read_table

RANDOM_SEED = 123456789
logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    """ Define the arguments available on the command line."""
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-infile", '-i',
                        default="/media/scott/HD2/imputation/pyminer_dynamic_cutoff/real/PMID_27667667_pancreas"
                                "_refseq_rpkms_3514sc_ge150k_OK_erccRM_ensg_collated_log2_naRM_sigExpress_ENSG.txt",
                        help="the input expression matrix")

    parser.add_argument("-out", '-o',
                        default="/media/scott/HD2/imputation/pyminer_dynamic_cutoff/real/PMID_27667667_pancreas_refse"
                                "q_rpkms_3514sc_ge150k_OK_erccRM_ensg_collated_log2_naRM_sigExpress_ENSG_1e2.txt",
                        help="the input expression matrix")

    parser.add_argument('-log',
                        help="if the input is log2 transformed we'll undo that to make the original "
                             "transcriptome again",
                        action="store_true",
                        default=False)

    parser.add_argument('-fig',
                        help="make the downsampled figure",
                        action="store_true",
                        default=False)

    parser.add_argument("-num_transcripts",
                        default=1e2,
                        type=int,
                        help="the number of transcripts each cell should have")

    args = parser.parse_args()
    return args


def make_colsums_equal(expression_vect, num_transcripts):
    return expression_vect * num_transcripts / np.sum(expression_vect) * 10


def get_transcript_vect(index, expression, mult_factor=1):
    return [index] * int(expression * mult_factor)


def get_all_transcript_vect(full_expression_vect, num_genes, num_final_transcripts=int(1e6), sample_percent=0.9):
    # num_final_transcripts = int(1e6)
    full_transcript_vect = []
    # go through each gene, generating a model of the transcriptome for that cell
    for i in range(0, np.shape(full_expression_vect)[0]):
        temp_transcripts = get_transcript_vect(i, full_expression_vect[i])
        full_transcript_vect += temp_transcripts
    np.random.shuffle(full_transcript_vect)
    full_transcript_vect = np.array(full_transcript_vect)[np.arange(num_final_transcripts, dtype=int)]
    freq = Counter(full_transcript_vect)
    all_counts = np.zeros(num_genes)
    for i in range(num_genes):
        all_counts[i] = freq[i]
    return all_counts


def rewrite_get_all_transcript_vect(full_data, num_final_transcripts=int(1e6)):
    all_counts = np.zeros(shape=full_data.shape)
    for col_idx, column in enumerate(full_data.T):
        row_set = []
        for gene_idx, UMI_count in enumerate(column):
            for i in range(int(UMI_count)):
                row_set.append(gene_idx)
        random.shuffle(row_set)
        row_set = row_set[:num_final_transcripts]
        for value in row_set:
            all_counts[value][col_idx] += 1
    return all_counts


def main():
    """ Wrapper to run the code in this file from the command line."""
    args = parse_arguments()

    downsample(args.infile, args.num_transcripts, args.out, args.fig, args.log)


def downsample(original_dataset, num_transcripts, out, fig=False, log=False):
    random.seed(RANDOM_SEED)
    np.random.seed(RANDOM_SEED)

    # check what the type the input was
    if isinstance(original_dataset, str):
        # if it was a file, read it in
        if os.path.isfile(original_dataset):
            original_dataset = np.array(read_table(original_dataset))
        else:
            raise Exception("don't know what to do with this input:" + original_dataset)
    elif isinstance(original_dataset, list):
        # if it was a list, numpy-fy it
        original_dataset = np.array(original_dataset)

    logger.debug("original_dataset: {}".format(original_dataset))

    full_dataset = np.array(original_dataset[1:, 1:], dtype=float)
    gene_ids = original_dataset[1:, 0]
    cols = original_dataset[0, :]
    num_genes = np.shape(gene_ids)[0]

    # undo the log if we need to
    if log:
        logger.debug(np.sum(full_dataset, axis=0))
        full_dataset[:, :] = 2 ** full_dataset[:, :]
        full_dataset[:, :] -= 1
        logger.debug(np.sum(full_dataset, axis=0))
        logger.debug(np.shape(np.sum(full_dataset, axis=0)))

    # make the output matrix
    out_data = np.zeros((np.shape(full_dataset)[0], np.shape(full_dataset)[1]))
    logger.info("getting transcript vectors")
    out_data = rewrite_get_all_transcript_vect(full_data=full_dataset, num_final_transcripts=num_transcripts)

    if log:
        out_data = np.log2(out_data + 1)
        if fig:
            full_dataset = np.log2(full_dataset + 1)

    if fig:
        logger.info('making downsampled fig')
        plt.clf()
        plt.scatter(full_dataset, out_data)
        plt.savefig(out + ".png")
    # append the gene and column titles
    logger.info('writing the output matrix')

    if os.path.isfile(out):
        os.remove(out)

    with open(out, 'w') as out_f:
        out_f.writelines('\t'.join(cols) + '\n')
        for i, row in enumerate(out_data):
            gene_name = gene_ids[i]
            temp_line = out_data[i, :].tolist()
            temp_line = gene_name + '\t' + '\t'.join(str(i) for i in temp_line)
            if i != len(out_data) - 1:
                out_f.writelines(temp_line + '\n')
            else:
                out_f.writelines(temp_line)

    logger.info('done!')


if __name__ == "__main__":
    main()
