import argparse
import fileinput
import numpy as np

from modules.common_functions import strip_split, read_table, read_file, write_table


def parse_arguments(parser=None):
    """ Define the arguments available on the command line."""
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-input_file", "-i",
                        help="the input matrix which needs to have cols removed")
    parser.add_argument("-output_file", "-o",
                        help="the file which will contain the cleaned matrix")
    parser.add_argument("-remove", "-remove_cols", "-r",
                        help="The file containing the names of columns to remove")
    args = parser.parse_args()
    return args


def main():
    """ Warpper to run the code in this file from the command line."""
    args = parse_arguments()

    remove_columns(args.input_file, args.remove, args.output_file)


def remove_columns(input_file, remove, output_file=None):
    header = None
    for line in fileinput.input(input_file):
        header = strip_split(line)
        break
    fileinput.close()
    in_mat = read_table(input_file)
    remove = set(read_file(remove, 'lines'))
    remove_indices = []
    for i, line in enumerate(header):
        if line in remove:
            remove_indices.append(i)

    in_mat = np.delete(in_mat, remove_indices, axis=1)

    if output_file:
        print('writing output file')
        write_table(in_mat, output_file)

    return in_mat


if __name__ == "__main__":
    main()
