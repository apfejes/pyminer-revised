import argparse
import numpy as np
import os

from bin.get_stats import get_stats
from modules.common_functions import read_table
from modules.references import HSAPIENS


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-classes", "-sample_classes", "-sample_class", "-class",
                        help="the file containing the sample classes (for example WT and mutant). "
                             "This is a text file that has sample names in the first column and the "
                             "second column has the classes.",
                        required=True)
    parser.add_argument("-sample_groups", "-sg", "-unsupervised_clustering",
                        help="this file has the results of the pyminer unsupervised clustering",
                        required=True)
    parser.add_argument("-cont_table",
                        help="this file is the contingency table made from pyminer_make_cont_table.py",
                        required=True)

    parser.add_argument(
        "-id_list_file", "-ids",
        help='If we are using an hdf5 file, give the row-wise IDs in this new line delimeted file',
        type=str, required=True)

    parser.add_argument(
        "-out_dir", "-o", "-out",
        help='if you know how which samples belong to which groups, feed in a file that has the samples in '
             'the first column, and their group number (index starting at 0), in the second column. The IDs '
             'must be in the same order as in the infile too.',
        dest='out_dir',
        type=str)

    parser.add_argument(
        "-species", "-s",
        help='what species is this? Must be gProfiler compatible.',
        dest='species',
        type=str,
        default=HSAPIENS)

    parser.add_argument(
        '-no_g_profile',
        help='should we do the automated gprofiler results?',
        default=False,
        action='store_true')

    parser.add_argument(
        "-FDR", "-fdr", "-fdr_cutoff",
        help='The desired Benjamini-Hochberg False Discovery Rate (FDR) for multiple comparisons '
             'correction (default = 0.05)',
        dest='FDR_cutoff',
        type=float,
        default=0.05)

    parser.add_argument(
        "-Zscore", "-Z_score_cutoff", "-Z", "-zscore", "-z",
        help='The desired False Discovery Rate (FDR) for multiple comparisons correction (default = 0.05)',
        dest='Zscore',
        type=float,
        default=2.0)

    parser.add_argument(
        '-hdf5',
        help='The input file is an HDF5 file',
        default=False,
        action='store_true')

    parser.add_argument(
        "-columns", "-cols",
        help='If we are using an hdf5 file, give the column-wise IDs in this new line delimeted file',
        type=str)

    parser.add_argument(
        '-rows',
        help='if the samples are in rows, and variables are in columns',
        default=False,
        action='store_true')

    parser.add_argument(
        "-log", '-log2', '-log_transform',
        help='do a log transformation prior to clustering',
        action='store_true',
        default=False)

    parser.add_argument(
        '-lin_norm',
        help='should we normalize the rows before doing the stats?',
        default=False,
        action='store_true')

    parser.add_argument(
        "-process_count",
        help='multiprocessing sections of the app can spin up multiple processes to accellerate processing '
             'time - set this to the number of free CPUs available.',
        dest='process_count',
        type=int,
        default=1)

    return parser.parse_args()


def main():

    args = parse_arguments()
    if args.out[-1] != '/':
        args.out += '/'

    cont_table = np.array(read_table(args.cont_table))
    pyminer_within_group_differences(contingency_table=cont_table,
                                     infile=args.infile,
                                     sample_groups=args.sample_groups,
                                     out=args.out_dir,
                                     classes_file=args.classes,
                                     columns_file=args.columns,
                                     id_list_file=args.id_list_file,
                                     hdf5=args.hdf5,
                                     rows=args.rows,
                                     log=args.log,
                                     lin_norm=args.lin_norm,
                                     fdr_cutoff=args.FDR_cutoff,
                                     zscore=args.zscore,
                                     no_g_profile=args.no_g_profile,
                                     species=args.species,
                                     process_count=args.process_count
                                     )


def pyminer_within_group_differences(contingency_table, infile, sample_groups,  out, classes_file,
                                     columns_file, id_list_file, hdf5=False,
                                     rows=False,
                                     log=False,
                                     lin_norm=False,
                                     fdr_cutoff=0.05,
                                     zscore=2.0,
                                     no_g_profile=False,
                                     species=HSAPIENS,
                                     process_count=1
                                     ):

    cont_array = np.array(contingency_table[1:, 1:], dtype=float)

    top_dir = out + "within_group_differences/"
    if not os.path.isdir(top_dir):
        os.mkdir(top_dir)
    # figure out which groups have enough cells to be worth comparing
    # we'll use a cufoff of having at least 5 cells in each group
    ge5_array = np.array(cont_array >= 5, dtype=int)
    num_ge5 = np.sum(ge5_array, axis=1)
    groups_with_enough_cells = num_ge5 >= 2
    print(np.sum(np.array(groups_with_enough_cells, dtype=int)),
          "groups will be compared for within group differences by class")
    groups_with_enough_cells = groups_with_enough_cells.tolist()
    # go through each of the groups and run the stats
    for idx, group in enumerate(groups_with_enough_cells):
        if group:
            temp_dir = top_dir + "class_differences_in_sample_group" + str(idx)
            if not os.path.isdir(temp_dir):
                os.mkdir(temp_dir)
            get_stats(infile=infile,
                      sample_groups=sample_groups,
                      sample_dir=out,
                      id_list=id_list_file,
                      columns=columns_file,
                      within_group=str(idx),
                      class_file=classes_file,
                      hdf5=hdf5,
                      rows=rows,
                      log=log,
                      lin_norm=lin_norm,
                      fdr_cutoff=fdr_cutoff,
                      zscore=zscore,
                      no_g_profile=no_g_profile,
                      species=species,
                      process_count=process_count)


if __name__ == "__main__":
    main()
