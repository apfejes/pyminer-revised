import numpy as np
import os
import argparse
from modules.common_functions import read_table, write_table


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-classes", "-sample_classes", "-sample_class", "-class",
                        help="the file containing the sample classes (for example WT and mutant). "
                             "This is a text file that has sample names in the first column and "
                             "the second column has the classes.",
                        required=True)
    parser.add_argument("-sample_groups", "-sg", "-unsupervised_clustering",
                        help="this file has the results of the pyminer unsupervised clustering",
                        required=True)
    parser.add_argument('-out',
                        help='where to put the output file', required=True)
    return parser.parse_args()


def main():

    args = parse_arguments()
    if not args.out:
        raise Exception('output directory must be given when run at the command line.')

    if not args.out.endswith(os.sep):
        args.out = args.out + os.sep
    if not os.path.isdir(args.out):
        raise Exception('Output path {} does not exist.'.format(args.out))

    contingency_table = pyminer_make_cont_table(args.classes, args.sample_groups)
    write_table(contingency_table, args.out + "group_class_contingency_table.txt")
    print("wrote file {}".format(args.out + "group_class_contingency_table.txt"))


def pyminer_make_cont_table(classes, sample_groups):
    class_table = np.array(read_table(classes))
    groups_table = np.array(read_table(sample_groups))
    # make the group ids a bit more legible
    for i in range(np.shape(groups_table)[0]):
        groups_table[i, 1] = "sample_group_" + str(int(float(groups_table[i, 1])))

    # make sure all the ids match
    class_id_hash = {key: value for key, value in class_table.tolist()}
    group_table_ids = groups_table[:, 0].tolist()
    sg_id_hash = {key: value for key, value in groups_table.tolist()}
    for cell_index in range(0, len(group_table_ids)):
        cell = group_table_ids[cell_index]
        if cell not in class_id_hash:
            raise Exception("couldn't find {} in the classes table. If this is a header,"
                            " it's fine, but if not, we're in deep doo-doo.".format(cell))
    # make the contingency table
    unique_class_ids = list(set(class_table[:, 1].tolist()))
    unique_class_ids = sorted(unique_class_ids)
    unique_group_ids = list(set(groups_table[:, 1].tolist()))
    unique_group_ids = sorted(unique_group_ids)
    contingency_table = np.zeros((len(unique_group_ids), len(unique_class_ids)))
    group_index_hash = {key: value for value, key in enumerate(unique_group_ids)}
    class_index_hash = {key: value for value, key in enumerate(unique_class_ids)}
    for cell in group_table_ids:
        cell_class = class_id_hash[cell]
        cell_group = sg_id_hash[cell]
        row = group_index_hash[cell_group]
        col = class_index_hash[cell_class]
        contingency_table[row, col] += 1
    contingency_table = contingency_table.tolist()
    header = ['groups'] + unique_class_ids
    for idx, c_t in enumerate(contingency_table):
        contingency_table[idx] = [unique_group_ids[idx]] + c_t
    contingency_table = [header] + contingency_table

    return contingency_table


if __name__ == "__main__":
    main()
