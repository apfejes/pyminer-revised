import fileinput
import gc
import sys
import random

import argparse
import matplotlib
use('Agg')

import seaborn as sns
sns.set(color_codes=True)

import matplotlib.pyplot as plt
import scipy
import numpy as np
from scipy.stats import mannwhitneyu as mw

from modules.common_functions import read_file, strip_split, read_table

gc.enable()
matplotlib.rcParams['agg.path.chunksize'] = 10000
random.seed(12345)
np.random.seed(12345)


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-short_path_mat1", "-sp1",
                        help="the first of two shortest path matrices")
    parser.add_argument("-short_path_mat2", "-sp2",
                        help="the first of two shortest path matrices")
    parser.add_argument("-adj1", "-adj_list1",
                        help="the adjacency list for the first dataset")
    parser.add_argument("-adj2", "-adj_list2",
                        help="the adjacency list for the second dataset")
    parser.add_argument("-ID_list", "-id_list", "-ids", "-IDs",
                        help="the list of all IDs that made the networks", required=True)
    parser.add_argument("-out_dir",
                        help="the directory to place the output file(s)")

    return parser.parse_args()


def main():

    args = parse_arguments()

    if args.out_dir is not None:
        out_dir = args.out_dir
    else:
        out_dir = args.short_path_mat1.split('/')
        out_dir = '/'.join(out_dir[:-1]) + '//'

    ID_list = read_file(args.ID_list, "lines")
    ID_hash = {}
    for i in range(0, len(ID_list)):
        ID_hash[ID_list[i]] = i

    sp1 = None
    sp2 = None

    if args.adj1 is not None and args.adj2 is not None:
        # set up the sp1_relationships and sp2_relationships matrices

        sp1_relationships = np.zeros((len(ID_list), len(ID_list)), dtype='i4')
        sp2_relationships = np.zeros((len(ID_list), len(ID_list)), dtype='i4')

        print('populating the first adjacency matrix')
        first = True
        for line in fileinput.input(args.adj1):
            if first:
                first = False
            else:
                temp_line = strip_split(line)
                index1 = ID_hash[temp_line[0]]
                index2 = ID_hash[temp_line[1]]
                sp1_relationships[index1, index2] = 1
                sp1_relationships[index2, index1] = 1
        fileinput.close()

        print('populating the second adjacency matrix')
        first = True
        for line in fileinput.input(args.adj2):
            if first:
                first = False
            else:
                temp_line = strip_split(line)
                index1 = ID_hash[temp_line[0]]
                index2 = ID_hash[temp_line[1]]
                sp2_relationships[index1, index2] = 1
                sp2_relationships[index2, index1] = 1
        fileinput.close()

        print("getting shortest path 1")
        sp1 = scipy.sparse.csgraph.shortest_path(sp1_relationships)
        print("getting shortest path 2")
        sp2 = scipy.sparse.csgraph.shortest_path(sp2_relationships)

    if args.short_path_mat1 is not None and args.short_path_mat2 is not None:
        sp1 = np.array(read_table(args.short_path_mat1), dtype='f4')

        sp2 = np.array(read_table(args.short_path_mat2), dtype='f4')

    if not sp1 and not sp2:
        raise Exception("sp1 and sp2 need to have assigned values to proceed.")

    # sp_dif_mat = np.abs(sp2 - sp1)
    print('getting lower triangles')
    sp1_lower = np.tril(sp1)
    sp2_lower = np.tril(sp2)

    print('converting them to linear vectors')
    sp1_lower = sp1_lower.reshape((len(ID_list) ** 2,))
    sp2_lower = sp2_lower.reshape((len(ID_list) ** 2,))

    print('removing the zeros')
    sp1_zero_mask = sp1_lower == 0
    sp2_zero_mask = sp2_lower == 0
    zero_mask = sp1_zero_mask + sp2_zero_mask
    # print(zero_mask)
    print(np.sum(zero_mask))
    non_zero_mask = zero_mask * -1
    non_zero_mask += 1
    non_zero_mask = np.array(non_zero_mask, dtype=bool)
    print(non_zero_mask)
    print(np.sum(non_zero_mask))

    sp1_non_zero = sp1_lower[non_zero_mask]
    sp2_non_zero = sp2_lower[non_zero_mask]

    print('removing the inifinities')

    sp1_inf_mask = np.isinf(sp1_non_zero)
    sp2_inf_mask = np.isinf(sp2_non_zero)

    inf_mask = sp1_inf_mask + sp2_inf_mask

    non_inf_mask = inf_mask * -1
    non_inf_mask += 1
    non_inf_mask = np.array(non_inf_mask, dtype=bool)

    ##
    sp1_non_zero_non_inf = sp1_non_zero[non_inf_mask]
    sp2_non_zero_non_inf = sp2_non_zero[non_inf_mask]

    print(sp1_non_zero_non_inf)
    print(sp2_non_zero_non_inf)

    print('calculating stats\n\n')

    print('spearman:')
    print(scipy.stats.spearmanr(sp1_non_zero_non_inf, sp2_non_zero_non_inf))
    print('\npearson:')
    print(scipy.stats.pearsonr(sp1_non_zero_non_inf, sp2_non_zero_non_inf))

    print('\nmaking plot #1')
    ax = sns.regplot(x=sp1_non_zero_non_inf,
                     y=sp2_non_zero_non_inf, color="g",
                     ci=95,
                     x_jitter=.2,
                     y_jitter=.2,
                     scatter_kws={'alpha': 0.01})

    ax.set(xlabel='Shortest path network-1', ylabel='Shortest path network-2')

    plt.savefig(out_dir + '/short_path_correlation.png', dpi=360)

    plt.clf()

    print('making plot #2')
    m, b = np.polyfit(sp1_non_zero_non_inf, sp2_non_zero_non_inf, 1)

    ax = plt.hexbin(sp1_non_zero_non_inf, sp2_non_zero_non_inf, cmap=plt.cm.afmhot_r,
                    gridsize=int(max(sp1_non_zero_non_inf) / 2))

    plt.plot(sp1_non_zero_non_inf, m * sp1_non_zero_non_inf + b, '-', color='b', lw=2)

    # ax.set_xlabel('Shortest path network-1')
    # ax.set_ylabel('Shortest path network-2')

    plt.savefig(out_dir + '/short_path_correlation_2.png', dpi=360)

    ax = plt.hexbin(sp1_non_zero_non_inf, sp2_non_zero_non_inf, cmap=plt.cm.afmhot_r, bins='log',
                    gridsize=int(max(sp1_non_zero_non_inf) / 2))

    plt.plot(sp1_non_zero_non_inf, m * sp1_non_zero_non_inf + b, '-', color='b', lw=2)

    # ax.set_xlabel('Shortest path network-1')
    # ax.set_ylabel('Shortest path network-2')
    # ax.set(xlab='Shortest path Network1', ylab='Shortest path Network2')

    plt.savefig(out_dir + '/short_path_correlation_2_log.png', dpi=360)

    # make_contingency_table
    print('\nchi-squared')
    sp1_relationships = np.array(sp1_non_zero == 1, dtype=int)
    sp2_relationships = np.array(sp2_non_zero == 1, dtype=int)

    if args.adj1 is None and args.short_path_mat1 is None:
        sys.exit('you must either specify an adj-list set, or a shortest path matrix set')

    print('calculating the contingency table and chi-square')
    # print(sp2_relationships)
    dif_vect = sp1_relationships - sp2_relationships
    in_sp1_only = np.sum(dif_vect == 1)
    in_sp2_only = np.sum(dif_vect == -1)
    equal_in_both = np.array(sp1_relationships == sp2_relationships, dtype=bool)
    # where it's equal in both, it will be 1, and multiplying by sp1_relationships will
    # convert it to equal in both and equal to 1
    # in_both = np.sum(np.array(equal_in_both * sp1_relationships, dtype = bool))
    in_both = np.sum((sp1_relationships + sp2_relationships) == 2)
    in_neither = np.sum(equal_in_both != sp1_relationships)

    contingency_table = np.zeros((2, 2))
    contingency_table[0, 0] = in_both
    contingency_table[0, 1] = in_sp1_only
    contingency_table[1, 0] = in_sp2_only
    contingency_table[1, 1] = in_neither

    print("""\n% of graph1 also in graph2:""", in_both / (in_both + in_sp1_only) * 100)
    print("""\n% of graph2 also in graph1:""", in_both / (in_both + in_sp2_only) * 100, '\n')

    print("\ncontingency table")
    print(contingency_table)
    chi_sqr_result = scipy.stats.chi2_contingency(contingency_table)
    print('\nchi:', chi_sqr_result[0])
    print('p:', chi_sqr_result[1])
    print('df:', chi_sqr_result[2])
    print('expected:\n', chi_sqr_result[3], '\n')

    print('\nfisher exact test:')
    oddsratio, pval = scipy.stats.fisher_exact(contingency_table)
    print('odds ratio:', oddsratio)
    print('p value:', pval, '\n')

    print('fold change:\n', contingency_table / chi_sqr_result[3])
    print('log2 fold change:\n', np.log2(contingency_table / chi_sqr_result[3]))


if __name__ == "__main__":
    main()
