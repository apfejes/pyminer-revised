import os
import numpy as np
import argparse

from modules.common_functions import make_file, read_table


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-mean_expression_file", "-i",
                        help="the summary table ",
                        type=str)
    parser.add_argument("-out", "-o",
                        help="the output folder in which the final output enrichemnt files should go",
                        type=str,
                        default="")

    return parser.parse_args()


def main():

    args = parse_arguments()

    mean_expression_table = np.array(read_table(args.mean_expression_file))
    expression_numeric = np.array(mean_expression_table[1:, 1:], dtype=float)

    gene_vect = mean_expression_table[1:, 0]
    cell_types = mean_expression_table[0, 1:]

    out_expressed_gene_vects = []
    if not args.out.endswith(os.sep):
        args.out += os.sep

    expression_vect = np.sort(expression_numeric.flatten())

    # non-zero values
    expression_vect = expression_vect[np.where(expression_vect > 0)[0]]
    cutoff_idx = int(np.shape(expression_vect)[0] * 0.05)
    expression_cutoff = expression_vect[cutoff_idx]
    print('expression cutoff:', expression_cutoff)

    for i in range(0, np.shape(expression_numeric)[1]):
        expressed_indices = np.where(expression_numeric[:, i] > 0)[0]  # expression_cutoff)[0]
        out_expressed_gene_vects.append(gene_vect[expressed_indices].tolist())
        make_file('\n'.join(out_expressed_gene_vects[-1][:]), args.out + cell_types[i] + '_binary_expression.txt')


if __name__ == "__main__":
    main()
