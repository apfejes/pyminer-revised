import os
import argparse
import fileinput
from modules.common_functions import read_table, strip_split, run_cmd


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument(
        '-input', '-i',
        help="input adj list")

    parser.add_argument(
        '-output', '-o',
        help="input adj list")

    parser.add_argument(
        '-annotation_file', '-a',
        help='the annotations.tsv file created by a PyMINEr run')

    return parser.parse_args()


def get_all_ens_pairs(aliases_1, aliases_2):
    all_ensg_pairs = []
    for a1 in aliases_1:
        for a2 in aliases_2:
            all_ensg_pairs.append([a1, a2])
    return all_ensg_pairs


def translate_line(temp_line, ensg_mapping_dict):
    # check to make sure both of these genes are in the ensg dict
    if temp_line[0] not in ensg_mapping_dict or temp_line[1] not in ensg_mapping_dict:
        # print("trouble mapping:",temp_line)
        return ()
    # print(temp_line[0])
    a1 = ensg_mapping_dict[temp_line[0]]
    a2 = ensg_mapping_dict[temp_line[1]]
    # print(a1,a2)
    return get_all_ens_pairs(a1, a2)


def process_line(temp_line, out_adj_file, ensg_mapping_dict):
    new_adj_list = translate_line(temp_line, ensg_mapping_dict)
    if new_adj_list is None:
        # if the orthologues didn't both map sucessfully
        return
    else:
        for adj in new_adj_list:
            # print(adj)
            out_adj_file.write('\t'.join(adj) + '\n')


def main():

    args = parse_arguments()

    # process the annotation file #
    annotations = read_table(args.annotation_file)
    ensg_mapping_dict = {}
    for i in range(1, len(annotations)):
        temp_id = annotations[i][1]
        if "ENTREZGENE_ACC" in temp_id:
            temp_id = temp_id.replace("ENTREZGENE_ACC:", "")
            temp_id = str(float(temp_id))
        temp_ensg_id = annotations[i][3]
        if temp_ensg_id == "None":
            pass
        else:
            if temp_id in ensg_mapping_dict:
                temp_mapping_vect = ensg_mapping_dict[temp_id]
                temp_mapping_vect.append(temp_ensg_id)
            else:
                ensg_mapping_dict[temp_id] = [temp_ensg_id]

    if os.path.isfile(args.output):
        os.remove(args.output)
    if os.path.isfile(args.output + "_temp"):
        os.remove(args.output + "_temp")

    out_adj_file = open(args.output + "_temp", 'w')

    print('writing output ENSG adj list:')
    print('\t', args.output)

    first = True
    for line in fileinput.input(args.input):
        if first:
            first = False
        else:
            temp_line = strip_split(line)
            process_line(temp_line, out_adj_file, ensg_mapping_dict)

    out_adj_file.close()

    # remove the last new line
    run_cmd(["head",
             "-c", "-1",
             args.output + "_temp",
             ">" + args.output])
    if os.path.isfile(args.output + "_temp"):
        os.remove(args.output + "_temp")


if __name__ == "__main__":
    main()
