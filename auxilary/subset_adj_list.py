import argparse
import fileinput
from modules.common_functions import strip_split, read_file, make_file


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument('-in_adj_list',
                        help="""the input adjacency list which will be subset for the given subsetting variables""")
    parser.add_argument('-subset_ids',
                        help="""the IDs from the adjacency list which need to be subset out""")
    parser.add_argument('-out_adj_list',
                        help="""the path to the desired output adjacency list""")
    parser.add_argument('-any_in_subset',
                        help="""if either nodes of the adjacency pair is in the subset of interest, 
                        put that relationship in the output adj list.""",
                        action='store_true',
                        default=False)
    return parser.parse_args()


def main():

    args = parse_arguments()

    subset_vars = read_file(args.subset_ids, 'lines')

    first = True
    output = ''
    for line in fileinput.input(args.in_adj_list):
        if first:
            """
            this is for appending the header of the adjacency list
            granted - this is an assumption...
            """
            output += line
            first = False

        temp_line = strip_split(line)

        if args.any_in_subset:
            if temp_line[0] in subset_vars or temp_line[1] in subset_vars:
                output += line
        else:
            if temp_line[0] in subset_vars and temp_line[1] in subset_vars:
                output += line

    # make sure there isn't a dangling new line
    if output[-2:] == '\n':
        output = output[:-2]

    make_file(output, args.out_adj_list)


if __name__ == "__main__":
    main()
