import argparse
import os

from auxilary.pyminer_chi_res import pyminer_chi_res
from auxilary.pyminer_make_cont_table import pyminer_make_cont_table
from auxilary.pyminer_within_group_differences import pyminer_within_group_differences

from modules.references import HSAPIENS


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-classes", "-sample_classes", "-sample_class", "-class",
                        help="the file containing the sample classes (for example WT and mutant). "
                             "This is a text file that has sample names in the first column and the "
                             "second column has the classes.")
    parser.add_argument("-sample_groups", "-sg", "-unsupervised_clustering",
                        help="this file has the results of the pyminer unsupervised clustering")
    parser.add_argument("-out", "-o",
                        help="directory for output")
    parser.add_argument("-negative_control", "-neg_ctrl", "-neg", "-n",
                        help="the name of the treatment that is the negative control. This changes "
                             "the stats from a test of independence to a goodness of fit relative to "
                             "the negative control.")

    parser.add_argument('-infile', '-in', '-i', '-input',
                        dest='infile',
                        type=str)

    parser.add_argument("-within_group",
                        help='if you know how which samples belong to which groups, feed in a file that '
                             'has the samples in the first column, and their group number (index starting '
                             'at 0), in the second column. The IDs must be in the same order as in the infile too.',
                        dest='within_group',
                        type=int)

    parser.add_argument("-species", "-s",
                        help='what species is this? Must be gProfiler compatible.',
                        dest='species',
                        type=str,
                        default=HSAPIENS)

    parser.add_argument('-no_gProfile',
                        help='should we do the automated gprofiler results?',
                        default=False,
                        action='store_true')

    parser.add_argument("-FDR", "-fdr", "-fdr_cutoff",
                        help='The desired Benjamini-Hochberg False Discovery Rate (FDR) for multiple comparisons '
                             'correction (default = 0.05)',
                        dest='FDR_cutoff',
                        type=float,
                        default=0.05)

    parser.add_argument("-Zscore", "-Z_score_cutoff", "-Z", "-zscore", "-z",
                        help='The desired False Discovery Rate (FDR) for multiple comparisons '
                             'correction (default = 0.05)',
                        dest='Zscore',
                        type=float,
                        default=2.0)

    parser.add_argument('-hdf5',
                        help='The input file is an HDF5 file',
                        default=False,
                        action='store_true')

    parser.add_argument("-ID_list", "-ids",
                        help='If we are using an hdf5 file, give the row-wise IDs in this new line delimeted file',
                        type=str)

    parser.add_argument("-columns", "-cols",
                        help='If we are using an hdf5 file, give the column-wise IDs in this new line delimeted file',
                        type=str)

    parser.add_argument('-rows',
                        help='if the samples are in rows, and variables are in columns',
                        default=False,
                        action='store_true')

    parser.add_argument("-log", '-log2', '-log_transform',
                        help='do a log transformation prior to clustering',
                        action='store_true',
                        default=False)

    parser.add_argument('-lin_norm',
                        help='should we normalize the rows before doing the stats?',
                        default=False,
                        action='store_true')

    return parser.parse_args()


def main():
    args = parse_arguments()

    if args.out[-1] != '/':
        args.out += '/'
    if not os.path.isdir(args.out):
        os.mkdir(args.out)

    # first we need to make the contingency table

    cont_table = pyminer_make_cont_table(classes=args.classes,
                                         sample_groups=args.sample_groups)

    # then we'll run the chi square analysis
    pyminer_chi_res(contingency_table=cont_table, out=args.out,
                    negative_control=args.negative_control)

    # then we'll look for differences within cell types across classes
    within_class_dir = args.out  # +"class_differences_within_cell_type/"
    if not os.path.isdir(within_class_dir):
        os.mkdir(within_class_dir)
    pyminer_within_group_differences(contingency_table=cont_table,
                                     infile=args.infile,
                                     sample_groups=args.sample_group,
                                     out=within_class_dir,
                                     classes_file=args.class_file,
                                     columns_file=args.columns_file,
                                     id_list_file=args.id_list_file,
                                     hdf5=args.hdf5,
                                     rows=args.rows,
                                     log=args.log,
                                     lin_norm=args.lin_norm,
                                     fdr_cutoff=args.fdr_cutoff,
                                     zscore=args.zscore,
                                     no_g_profile=args.no_g_profile,
                                     species=args.species)


if __name__ == "__main__":
    main()
