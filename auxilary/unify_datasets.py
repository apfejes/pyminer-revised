import argparse
import fileinput
import h5py
import numpy as np
import os
import random
from gprofiler import GProfiler

from modules.common_functions import strip_split, make_file, write_table
from modules.references import SUPPORTED_ORGANISMS, SPECIES_CODES, HSAPIENS


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-in_dir", "-i",
                        help="the directory which contains the matrices to be combined into a single dataset.")
    parser.add_argument("-output_file", "-o",
                        help="the output file to be made after conversion.")
    parser.add_argument("-hdf5",
                        help="write the output file as an hdf5 file rather than tab delimted.",
                        action='store_true',
                        default=False)
    parser.add_argument(
        "-organism", '-species', '-s',
        help='which organism are we using. (Use the -species_codes to print out a list of all supported species and their codes).',
        type=str,
        default=HSAPIENS)
    parser.add_argument(
        "-species_codes",
        help="print out the species codes",
        action="store_true",
        default=False)
    parser.add_argument(
        "-num_genes_downsampled",
        '-downsample',
        '-ds',
        help="the number of genes to which expression should be downsampled to include. Samples with less than the number of indicated genes will be excluded from the final dataset.",
        type=float
    )

    return parser.parse_args()


def check_organism(organism):
    if organism not in SUPPORTED_ORGANISMS:
        raise Exception("could not find {} in the species codes. Use the -species_codes argument to see what "
                        "you should use for the -organism argument.".format(organism))


def get_files_in_folder(in_folder):
    onlyfiles = [f for f in os.listdir(in_folder) if os.path.isfile(os.path.join(in_folder, f))]
    return onlyfiles


def convert_to_ensg(in_genes, gp, organism, gene_lookup_dict={}, gene_symbol_dict={}):
    # this function will yeild a dictionary that 
    temp_gene_list = []
    for g in in_genes:
        if g not in gene_lookup_dict:
            gene_lookup_dict[g] = []

    results = gp.gconvert(in_genes, organism=organism, target='ENSG', numeric_ns="ENTREZGENE_ACC")

    for r in range(1, len(results)):
        # the original ID
        g = results[r][1]
        # clean it up if it's an entrez gene accesssion
        g = g.replace("ENTREZGENE_ACC:", "")
        # the current orthologue list
        try:
            gene_lookup_dict[g]
        except:
            print('weird mapping event:')
            print(g, results[r])
        else:
            if results[r][4] is not None:
                gene_symbol_dict[results[r][3]] = results[r][4]
            if results[r][3] is not None:
                temp_list = gene_lookup_dict[g]
                temp_list += [results[r][3]]
                gene_lookup_dict[g] = temp_list
                temp_gene_list.append(results[r][3])

    new_gene_ids = []
    for k in list(gene_lookup_dict.keys()):
        new_gene_ids += gene_lookup_dict[k]
    new_gene_ids = list(set(new_gene_ids))

    for gene in new_gene_ids:
        if gene not in gene_symbol_dict:
            gene_symbol_dict[gene] = "None"

    temp_gene_list = list(set(temp_gene_list))
    return gene_lookup_dict, gene_symbol_dict, temp_gene_list


#####################################################################################
# go through all of the files and collect the gene ids and sample_ids

def get_sample_names(in_file):
    for line in fileinput.input(in_file):
        temp_line = strip_split(line)
        break
    fileinput.close()
    temp_line = temp_line[1:]
    top_dir = in_file.split('/')
    file_name = top_dir[-1][:-4]
    for i in range(0, len(temp_line)):
        temp_line[i] = file_name + "||" + temp_line[i]
        # print(temp_line[i])
    return temp_line


def get_genes(in_file):
    gene_list = []
    first = True
    for line in fileinput.input(in_file):
        if first:
            first = False
        else:
            temp_line = strip_split(line)
            gene_list.append(temp_line[0])
    fileinput.close()
    # gene_list = int_gene_convert(gene_list)
    return gene_list


def get_array_addition(temp_gene, f, temp_line_num, gene_lookup_dict, all_ensg_positions_dict, sample_offset):

    # get the indices of the samples in the final dataset
    num_samples = np.shape(temp_line_num)[0]
    col_idxs = list(range(sample_offset[f], (num_samples + sample_offset[f])))

    # get the genes & output rows that this accession/symbol corresponds to
    target_genes = gene_lookup_dict[temp_gene]
    if not target_genes:
        return None, None, None
    row_idxs = []
    for gene in target_genes:
        row_idxs.append(all_ensg_positions_dict[gene])
    num_targets = len(row_idxs)

    # make an array that segregates
    expression_sub_array = np.zeros((num_targets, num_samples))
    for row in range(0, num_targets):
        expression_sub_array += temp_line_num / num_targets

    return row_idxs, col_idxs, expression_sub_array


def lin_norm_vect(vect, min_non_zero, epsilon=0.0):
    vect = vect - min_non_zero
    vect[np.where(vect < 0.0)[0]] = -epsilon
    vect = vect / (max(vect) / 1000)
    vect += epsilon
    # fix floating point errors
    vect[np.where(vect < 0.0)[0]] = 0.0
    return vect


def downsample_and_rank_transform(vect, cutoff):
    # print(vect)
    ranked_original = rankdata(vect)
    ranked_original = ranked_original - min(ranked_original)
    # print(ranked_original)
    subtract = max(ranked_original) - cutoff - 1
    # print(subtract)
    ranked_original -= subtract
    # print(ranked_original)
    flatten_to_zero_indices = np.where(ranked_original <= 0)[0]
    # print(flatten_to_zero_indices)
    ranked_original[flatten_to_zero_indices] = 0.0
    # print(ranked_original)
    # now that we know where the top n genes are, flatten the rest to zero in the original vector
    included_indices = np.where(ranked_original > 0)[0]
    # flattened_vect = included_binary_vect * vect
    flattened_vect = vect[:]
    vect[flatten_to_zero_indices] = 0
    min_non_zero = min(flattened_vect[included_indices])
    norm_vect = lin_norm_vect(flattened_vect, min_non_zero)
    return norm_vect
    # return(ranked_original)


def quantile(vect, final_quantile_master_vect):
    copy_master = final_quantile_master_vect[:]
    sort_order = np.argsort(vect)
    # print(sort_order)
    out = np.zeros((np.shape(vect)[0]))
    for i in range(0, np.shape(sort_order)[0]):
        temp_idx = int(sort_order[i])
        out[temp_idx] = copy_master[i]
    return out


def main():

    args = parse_arguments()

    if args.species_codes:
        print(SPECIES_CODES)
        return

    check_organism(organism=args.organism)

    gp = GProfiler('PyMINEr_' + str(random.randint(0, int(1e6))), want_header=True)

    os.chdir(args.in_dir)
    # def int_gene_convert(gene_list):
    #     out_genes = []
    #     for gene in gene_list:
    #         try:
    #             int(gene)
    #         except:
    #             out_genes.append(gene)
    #         else:
    #             out_genes.append(int(gene))
    #     return(out_genes)
    ########################################################
    all_files = get_files_in_folder(args.in_dir)
    
    all_samples = []  # list of lists containing each sample in each dataset
    all_genes = []  # list of lists containing each gene in each dataset
    # this will be a simple linear list with each sample in the order that it will be in the output file
    all_samples_linear = []
    sample_offset = []
    
    for f in all_files:
        print('working on', f)
        all_samples.append(get_sample_names(f))
        print('\tfound', len(all_samples[-1]), 'new samples')
        print('\t\t', all_samples[-1][:1], "...")
        all_genes.append(get_genes(f))
        print('\tfound', len(all_genes[-1]), 'new genes')
        print('\t\t', all_genes[-1][:1], "...")
    
    # make all samples linear list
    for s in all_samples:
        all_samples_linear += s
    
    # calculate the column offset for each file
    previous_length = 0
    for s in all_samples:
        sample_offset.append(previous_length)
        previous_length += len(s)
    
    # now we need to get all the genes
    gene_lookup_dict = {}
    gene_symbol_dict = {}
    
    print('converting all of the gene ids to ENSG')
    individual_gene_lists = []
    for g in all_genes:
        gene_lookup_dict, gene_symbol_dict, temp_gene_list = convert_to_ensg(g, gene_symbol_dict=gene_symbol_dict,
                                                                             gene_lookup_dict=gene_lookup_dict,
                                                                             gp=gp,
                                                                             organism=args.organism)
        individual_gene_lists.append(temp_gene_list)
    
    # for g in all_genes:
    #     for temp_g in g:
    #         print(temp_g, gene_lookup_dict[temp_g])
    
    # now organize the genes, and get ready for laying out the output table
    all_gene_lists = list(gene_lookup_dict.values())
    print("all_gene_lists", len(all_gene_lists))
    all_ensg_genes = []
    for i in range(0, len(all_gene_lists)):
        all_ensg_genes += all_gene_lists[i]
    
    # filtering for genes that were found in all datasets
    all_full_ensg_genes = list(set(all_ensg_genes))
    all_ensg_genes = []
    remove_ensg_genes = []
    for gene in all_full_ensg_genes:
        for temp_genes in individual_gene_lists:
    
            if gene not in temp_genes:
                # print(gene,"not found in all datasets")
                remove_ensg_genes.append(gene)
                break
    
    remove_ensg_genes = list(set(remove_ensg_genes))
    
    for gene in all_full_ensg_genes:
        if gene not in remove_ensg_genes:
            all_ensg_genes.append(gene)
    
    all_ensg_genes = list(set(all_ensg_genes))
    
    print(len(all_ensg_genes), 'ENSG genes found in each dataset')
    print(len(remove_ensg_genes), 'found in only some datasets')
    all_ensg_genes.sort()
    remove_ensg_genes.sort()
    
    # remove missing genes from other datasets
    print("removing ENSG genes that weren't found in all datasets")
    for gene in list(gene_lookup_dict.keys()):
        temp_list = gene_lookup_dict[gene]
        new_list = []
        for i in range(0, len(temp_list)):
            if temp_list[i] in remove_ensg_genes:
                pass
            else:
                new_list.append(temp_list[i])
        gene_lookup_dict[gene] = new_list
    
    all_ensg_genes.sort()
    print(all_ensg_genes[:5], '...')
    
    all_ensg_positions_dict = {key: value for value, key in enumerate(all_ensg_genes)}
    
    for i in range(0, 5):
        temp_gene = all_ensg_genes[i]
        print(temp_gene, all_ensg_positions_dict[temp_gene])
    
    ##################################################################################################
    # now we actually start making the output
    
    if args.hdf5:
        # set up the hdf5 file
    
        outfile = os.path.splitext(args.output_file)[0] + '.hdf5'
        if os.path.isfile(outfile):
            os.remove(outfile)
        h5_f = h5py.File(outfile, "w")
    
        # set up the data matrix (this assumes float32)
        out_mat = h5_f.create_dataset("temp_infile", (len(all_ensg_genes), len(all_samples_linear)), dtype=np.float32,
                                      maxshape=(None, None))
    
        # out_mat = np.zeros((len(all_ensg_genes),len(all_samples_linear)))
    
    else:
        out_mat = np.zeros((len(all_ensg_genes), len(all_samples_linear)))
    
    
    # go through each file populating the output matrix
    print('making matrix for output', len(all_ensg_genes), 'genes and', len(all_samples_linear), 'samples')
    
    # clean the all_samples_linear.. I noticed that quotes sometimes screw things up
    for i in range(0, len(all_samples_linear)):
        all_samples_linear[i] = all_samples_linear[i].replace('"', '')
    
    for f in range(0, len(all_files)):
        first = True
        print('\tworking on', all_files[f])
        for line in fileinput.input(all_files[f]):
            if first:
                first = False
                temp_num_samples = len(strip_split(line)) - 1
                count = 0
            else:
                count += 1
                if temp_num_samples > 2000:
                    if count % 2000 == 0:
                        print('\t\t', count)
                temp_line = strip_split(line)
                # print(temp_line)
                temp_gene = temp_line[0]
                temp_line_num = np.array(temp_line[1:], dtype=float)
                # get the gene row(s) to add these values too
                row_idxs, col_idxs, expression_sub_array = get_array_addition(temp_gene, f, temp_line_num)
                if row_idxs is not None:
                    for temp_row in row_idxs:
                        out_mat[temp_row, col_idxs] += expression_sub_array[0, :]
    
    
    #########################################################
    
    
    # figure out how many genes are expressed in each of the samples
    if args.num_genes_downsampled:
        print(np.shape(out_mat))
        out_mat = out_mat[:np.shape(out_mat)[0], :]
        # print(out_mat)
        sample_wise_gene_express_count = np.sum(out_mat > 0.0, axis=0)
        print(np.shape(sample_wise_gene_express_count))
        print(sample_wise_gene_express_count)
        samples_to_keep = np.where(sample_wise_gene_express_count >= args.num_genes_downsampled)[0]
        print("samples_to_keep", samples_to_keep)
        print("sample_wise_gene_express_count", sample_wise_gene_express_count)
        print(len(samples_to_keep), (len(samples_to_keep) / len(sample_wise_gene_express_count)),
              "samples had enough expressed genes to keep")
        # do the subsetting
        out_mat = out_mat[:, samples_to_keep]
        all_samples_linear = np.array(all_samples_linear)
        all_samples_linear = all_samples_linear[samples_to_keep].tolist()
        sample_wise_gene_express_count = sample_wise_gene_express_count[samples_to_keep]
        # new_sample_wise_gene_express_count = np.sum(out_mat != 0.0, axis = 0)
        # sys.exit(new_sample_wise_gene_express_count == sample_wise_gene_express_count)
    
        # do the rank transformation

        print('rank transforming all of the samples')
        print(np.shape(out_mat))
        for i in range(0, np.shape(out_mat)[1]):
            # print(out_mat[:,i])
            out_mat[:, i] = downsample_and_rank_transform(out_mat[:, i], args.num_genes_downsampled)
            # print(out_mat[:,i])
        print("col NonZero Sum", np.sum(out_mat != 0.0, axis=0))
        print("colmax", np.max(out_mat, axis=0))
        print("colmin", np.min(out_mat, axis=0))
        print('colsum', np.sum(out_mat, axis=0))
        print(out_mat)

    # remove the unexpressed genes
    print('removing unexpressed genes.\nOriginal Shape', np.shape(out_mat))
    gene_express_sum = np.sum(out_mat, axis=1)
    non_zero_genes = np.where(gene_express_sum != 0.0)[0]
    print('non_zero_genes', non_zero_genes)
    all_ensg_genes = np.array(all_ensg_genes)
    all_ensg_genes = all_ensg_genes[non_zero_genes].tolist()
    out_mat = out_mat[non_zero_genes, :]
    print('  New Shape:', np.shape(out_mat))
    
    # quantile normalize giving each dataset equal weight
    dataset_vect = []
    dataset_dict = {}
    for i in range(len(all_samples_linear)):
        sample = all_samples_linear[i]
        temp = sample.split("||")
        temp_dataset = temp[0]
        dataset_vect.append(temp_dataset)
        if temp[0] not in dataset_dict:
            dataset_dict[temp_dataset] = [i]
        else:
            temp_index = dataset_dict[temp_dataset]
            temp_index.append(i)
            dataset_dict[temp_dataset] = temp_index
    
    dataset_list = list(dataset_dict.keys())
    dataset_averages = np.zeros((len(all_ensg_genes), len(dataset_list)))
    
    # get the average sorted vector for each dataset
    for d in range(len(dataset_list)):
        dataset = dataset_list[d]
        for col in dataset_dict[dataset]:
            dataset_averages[:, d] += np.sort(out_mat[:, col], kind="mergesort")  # note that these are in decending order
        dataset_averages[:, d] /= len(dataset_dict[dataset])
    
    print("dataset averages:\n", dataset_averages)
    final_quantile_master_vect = np.mean(dataset_averages, axis=1)
    print("master quantile vector (highest to lowest):\n", final_quantile_master_vect[::-1])

    # go back through the whole dataset and update the values to match the master quantile normalization vector
    if True:
        print('\n\nupdating values to relfect the quantile normalized values')
        for col in range(0, np.shape(out_mat)[1]):
            if col % 1000 == 0:
                print('\t', col)
            out_mat[:, col] = quantile(out_mat[:, col])
        print("col NonZero Sum", np.sum(out_mat != 0.0, axis=0))

    print(out_mat)
    print('writing the output file')
    if not args.hdf5:
    
        nrow = np.shape(out_mat)[0] + 1
        ncol = np.shape(out_mat)[1] + 1
        # print(all_samples_linear)
        # print(len(all_samples_linear))
        out_str = [["gene"] + all_samples_linear]
        # print(out_str)
        # gene_array = ["gene"]+all_ensg_genes
        for i in range(0, len(out_mat)):
            out_str.append([all_ensg_genes[i]] + out_mat[i].tolist())
            # print(out_str[-1])
        out_str = np.array(out_str)
    
        write_table(out_str, args.output_file)
    else:
        # h5_f.close()
        # create the final dataset with the removed samples and genes
    
        # outfile_final = os.path.splitext(args.output_file)[0]+'.hdf5'
        # h5_final = h5py.File(outfile_final, "w")
    
        # h5_final.create_dataset("infile", (len(all_ensg_genes),len(all_samples_linear)), dtype=np.float32)
        # h5_final["infile"] = np.zeros((len(all_ensg_genes),len(all_samples_linear)))
        # print(h5_final["infile"] + out_mat)
    
        h5_f.create_dataset("infile", (len(all_ensg_genes), len(all_samples_linear)), dtype=np.float32,
                            maxshape=(None, None), data=out_mat)
        # h5_f["infile"] = h5_f["temp_infile"][non_zero_genes,samples_to_keep]
        # h5_f["shit_fucker"] = h5_f["infile"][:]
    
        # #del out_mat
        # print("col NonZero Sum",np.sum(final_out_mat != 0.0, axis = 0))
        # print("colmax",np.max(final_out_mat,axis=0))
        # print("colmin",np.min(final_out_mat,axis=0))
        # print('colsum',np.sum(final_out_mat,axis=0))
        # print(final_out_mat)
    
        print("col NonZero Sum", np.sum(h5_f["temp_infile"] != 0.0, axis=0))
        print("colmax", np.max(h5_f["temp_infile"], axis=0))
        print("colmin", np.min(h5_f["temp_infile"], axis=0))
        print('colsum', np.sum(h5_f["temp_infile"], axis=0))
        print(h5_f["temp_infile"])
    
        print("col NonZero Sum", np.sum(h5_f["infile"] != 0.0, axis=0))
        print("colmax", np.max(h5_f["infile"], axis=0))
        print("colmin", np.min(h5_f["infile"], axis=0))
        print('colsum', np.sum(h5_f["infile"], axis=0))
        print(h5_f["infile"])
    
        temp = str(args.output_file).split('/')
        temp = ('/').join(temp[:-1])
    
        # write the row and column info
        make_file('\n'.join(["variables"] + all_samples_linear), temp + '/column_IDs.txt')
        make_file('\n'.join(all_ensg_genes), temp + '/ID_list.txt')
    
        del h5_f["temp_infile"]
        # close the hdf5 file
        h5_f.close()


if __name__ == "__main__":
    main()
