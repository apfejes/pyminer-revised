import os

import argparse
import fileinput
import numpy as np

from modules.common_functions import write_table, strip_split

EDGE_DELIM = '|'


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument(
        '-file_list', '-in', '-i', '-input',
        dest='file_list',
        help="files that contain the input file adj lists",
        nargs='+'  # ,
    )

    parser.add_argument(
        '-out_dir', '-out', '-o',
        type=str)

    parser.add_argument(
        '-percent_cutoff', '-p',
        type=float,
        default=0.33333)

    return parser.parse_args()


def get_unique(temp_line, edge_delim=EDGE_DELIM):
    temp_line = sorted(temp_line)
    temp_line = edge_delim.join(temp_line)
    return temp_line


def get_file_adj_idset(f):
    print('\t\tgetting edge IDs from', f)
    # gets a list of unique IDs for all edges of
    temp_adj_pairs = []
    first = True
    for line in fileinput.input(f):
        if first:
            first = False
        else:
            temp_line = strip_split(line)
            if len(temp_line) == 1:  # possible orphan nodes shouldn't be there, but just in case
                pass
            else:
                # order the two nodes alphabetically so that each pair
                # can only appear once. Because this is for undirected
                # co-expression networks, it shouldn't make a difference
                # and it will cut the run time in half
                unique_id = get_unique(temp_line)
                temp_adj_pairs.append(unique_id)
    fileinput.close()
    print('\t\t', len(temp_adj_pairs))
    return temp_adj_pairs


def dictify(in_list):
    return {key: idx for idx, key in enumerate(in_list)}


def main():

    args = parse_arguments()

    file_list = args.file_list
    out_dir = args.out_dir
    if out_dir[-1] != '/':
        out_dir += '/'

    if os.path.isdir(out_dir):
        pass
    else:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)

    count_cutoff = args.percent_cutoff * len(file_list)
    print('\n\n\ncount cutoff', count_cutoff, '\n\n\n')

    # go through each file getting the full list of all neighbors
    all_adj_pairs = []  # all adj pairs is the list of all edges with a unique identifier
    print('cateloguing all of the edges:')
    for f in file_list:
        print('\t', f)
        all_adj_pairs += get_file_adj_idset(f)
        print('\trunning total:', len(all_adj_pairs))

    # make it unique
    all_adj_pairs = sorted(list(set(all_adj_pairs)))

    print('\n\nfound', len(all_adj_pairs), 'edges in total\n\n')

    # the boolean matrix of whether an edge is present in a given dataset
    # rows for each edge and columns are each network
    adj_bool_mat = np.zeros((len(all_adj_pairs), len(file_list)), dtype=int)

    # go through each file again now looking for each of the full edge list
    print('\n\ncateloguing the number of times each edge appears:')
    for i in range(0, len(file_list)):
        f = file_list[i]
        print('\t', f)
        temp_adj_dict = dictify(get_file_adj_idset(f))
        print('\t\tfound', len(list(temp_adj_dict.keys())), 'edges in this adj list')
        for j in range(0, len(all_adj_pairs)):
            if all_adj_pairs[j] in temp_adj_dict:
                adj_bool_mat[j, i] = 1
            else:
                pass  # print(all_adj_pairs[j])

    # count how many datasets each edge is in
    adj_count = np.sum(adj_bool_mat, axis=1)

    # make the output files
    print("making the output files")
    out_count = [["node_1", "node_2", "edge_count"]]
    filtered_out_adj = [["node_1", "node_2"]]
    for i, temp_edge in enumerate(all_adj_pairs):
        temp_edge = temp_edge.split(EDGE_DELIM)
        if adj_count[i] > count_cutoff:
            filtered_out_adj.append(temp_edge[:])
        temp_edge += [adj_count[i]]
        out_count.append(temp_edge)

    write_table(out_count, out_dir + 'full_adj_list_counts.txt')
    write_table(filtered_out_adj, out_dir + 'filtered_adj_list.txt')

    # write the boolean matrix
    bool_out = [['edge'] + file_list]
    for i in range(0, len(all_adj_pairs)):
        temp_edge = [all_adj_pairs[i]] + adj_bool_mat[i, :].tolist()
        bool_out.append(temp_edge)

    write_table(bool_out, out_dir + 'boolean_adj_list_table.txt')


if __name__ == "__main__":
    main()
