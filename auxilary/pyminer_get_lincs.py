import glob
import os
import random
import sys
import argparse

import requests
import json

from modules.common_functions import write_table, read_file, import_dict


URL = 'http://amp.pharm.mssm.edu/L1000CDS2/query'


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-full_lineage",
                        help="the directory for all of the cell type pairs",
                        type=str)

    parser.add_argument("-lineage_dir",
                        help="the directory for this cell type pair created by ",
                        type=str)

    parser.add_argument("-up",
                        help="a file containing genes that go up",
                        type=str)

    parser.add_argument("-down",
                        help="a file containing genes that go down",
                        type=str)

    parser.add_argument("-human_symbol_dict",
                        help="the pickled file created by pyminer_gprofiler_converter.py",
                        type=str)

    parser.add_argument("-out_dir", '-o',
                        help="the output prefix",
                        type=str)

    return parser.parse_args()


def upper_genes(genes):
    # The app uses uppercase gene symbols. So it is crucial to perform upper_genes() step.
    return [gene.upper() for gene in genes]


def get_lincs(up, down):
    # convert to gene symbols
    # gene-set search example
    data = {"upGenes": up,
            "dnGenes": down}
    data['upGenes'] = upper_genes(data['upGenes'])
    data['dnGenes'] = upper_genes(data['dnGenes'])
    config = {"aggravate": True, "searchMethod": "geneSet", "share": False, "combination": True, "db-version": "latest"}
    metadata = [{"key": "Tag", "value": "PyMINEr_" + str(random.randint(0, int(1e6)))}]
    payload = {"data": data, "config": config, "meta": metadata}
    headers = {'content-type': 'application/json'}
    r = requests.post(URL, data=json.dumps(payload), headers=headers)
    return r.json()


def parse_hit(hit, keep):
    out_list = []
    for k in keep:
        if k in hit:
            out_list.append(str(hit[k]))
        else:
            out_list.append('N/A')
    return out_list


def get_human_symbol_names(original_name_vect, human_symbol_dict):
    temp_ids = []
    for temp_id in original_name_vect:
        temp_ids += human_symbol_dict[temp_id]
    return temp_ids


def parse_group_name(file_name):
    file_name = file_name.split('/')
    file_name = file_name[-1]
    file_name = file_name.split('.')
    file_name = file_name[:-1]
    return '.'.join(file_name)


def parse_up_down_files(up_file, down_file, human_symbol_dict):
    up_original_ids = read_file(up_file)
    down_original_ids = read_file(down_file)
    up_ids = get_human_symbol_names(up_original_ids, human_symbol_dict=human_symbol_dict)
    down_ids = get_human_symbol_names(down_original_ids, human_symbol_dict=human_symbol_dict)
    up_name = parse_group_name(up_file)
    down_name = parse_group_name(down_file)
    return up_original_ids, down_original_ids, up_ids, down_ids, up_name, down_name


def parse_lincs_result(lincs_result, up_name, down_name, out_dir):
    for line in lincs_result:
        print(line)
        for line2 in lincs_result[line]:
            print('\t', line2)

    top_hits = lincs_result["topMeta"]

    treat_titles = ['score', 'pert_desc', 'sig_id', 'pert_id', 'pubchem_id', 'drugbank_id', 'pert_dose',
                    'pert_dose_unit', 'cell_id', 'pert_time', 'pert_time_unit', 'DEGcount', 'overlap']
    single_treatment_table = [treat_titles]
    for hit in top_hits:
        # for hit(list(hit.keys()))
        single_treatment_table.append(parse_hit(hit, treat_titles))

    write_table(single_treatment_table, out_dir + down_name + '_to_' + up_name + '_single_treatments.txt')


def get_files_from_lineage_dir(in_dir):
    out_files = []
    os.chdir(in_dir)
    for temp_file in glob.glob('*.txt'):
        print(temp_file)
        if 'gProfiler' not in temp_file:
            trial_name = parse_group_name(temp_file)
            print(trial_name)
            try:
                int(trial_name)
            except ValueError:
                pass
            else:
                out_files.append(temp_file)
    print(out_files)
    if len(out_files) == 2:
        return out_files[0], out_files[1]
    else:
        sys.exit("couldn't find the right up and down files - try doing it the manual way")


def do_analysis(up_ids, down_ids, up_name, down_name, out_dir):
    lincs_result_1 = get_lincs(up_ids, down_ids)
    parse_lincs_result(lincs_result_1, up_name, down_name, out_dir=out_dir)
    return


def do_lineage_dir(lineage_dir, human_symbol_dict):
    print('doing', lineage_dir)

    if lineage_dir[-1] != '/':
        lineage_dir += '/'
    # figure out which of the files are the actual gene lists
    file1, file2 = get_files_from_lineage_dir(lineage_dir)

    # parse them
    up_original_ids, down_original_ids, up_ids, down_ids, up_name, down_name = \
        parse_up_down_files(file1, file2, human_symbol_dict=human_symbol_dict)

    # do the analysis going in both directions
    do_analysis(up_ids, down_ids, up_name, down_name, out_dir=lineage_dir)
    do_analysis(down_ids, up_ids, down_name, up_name, out_dir=lineage_dir)


def get_lineage_directories(in_dir):
    os.chdir(in_dir)
    lineage_dirs = []
    for temp_file in glob.glob('*'):
        if '_vs_' in temp_file and os.path.isdir(temp_file):
            temp_full_path = os.path.abspath(temp_file)
            lineage_dirs.append(temp_full_path)
    return lineage_dirs


def main():

    args = parse_arguments()

    # import the symbol dictionary
    human_symbol_dict, human_def_dict = import_dict(args.human_symbol_dict)

    # write the output for single treatments
    if args.out_dir is not None:
        if args.out_dir[-1] != '/':
            args.out_dir += '/'

    # import the up and down lists
    if args.full_lineage is not None:
        lineage_dirs = get_lineage_directories(args.full_lineage)
        for lineage in lineage_dirs:
            do_lineage_dir(lineage, human_symbol_dict=human_symbol_dict)

    elif args.lineage_dir is not None:
        do_lineage_dir(args.lineage_dir, human_symbol_dict=human_symbol_dict)

    else:
        # if this is a specific analysis with a desired up or down, we won't do it bi-directionally
        # so then we'll just read in the up and down files and run it on those
        up_original_ids, down_original_ids, up_ids, down_ids, up_name, down_name = \
            parse_up_down_files(args.up, args.down, human_symbol_dict)
        do_analysis(up_ids, down_ids, up_name, down_name, out_dir=args.out_dir)


if __name__ == "__main__":
    main()
