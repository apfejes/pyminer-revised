import gc

import argparse
from modules.common_functions import read_table, write_table
gc.enable()

def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument(
        '-input', '-i',
        help="input adj list")

    parser.add_argument(
        '-output', '-o',
        help="input adj list")

    parser.add_argument(
        '-annotation_file', '-a',
        help='the annotations.tsv file created by a PyMINEr run')

    return parser.parse_args()


def get_out_lines(in_line, ensg_mapping_dict):
    temp_gene = in_line[0]
    if temp_gene not in ensg_mapping_dict:
        return ()
    else:
        ensg_list = ensg_mapping_dict[temp_gene]
        out_lines = []
        for temp_ens in ensg_list:
            temp_line = [temp_ens] + in_line[1:]
            out_lines.append(temp_line)
    return (out_lines)


def main():

    args = parse_arguments()

    # process the annotation file
    annotations = read_table(args.annotation_file)
    ensg_mapping_dict = {}
    for i in range(1, len(annotations)):
        temp_id = annotations[i][1]
        if "ENTREZGENE_ACC" in temp_id:
            temp_id = temp_id.replace("ENTREZGENE_ACC:", "")
            temp_id = str(float(temp_id))
        temp_ensg_id = annotations[i][3]
        if temp_ensg_id == "None":
            pass
        else:
            if temp_id in ensg_mapping_dict:
                temp_mapping_vect = ensg_mapping_dict[temp_id]
                temp_mapping_vect.append(temp_ensg_id)
            else:
                ensg_mapping_dict[temp_id] = [temp_ensg_id]

    input_table = read_table(args.input)
    output = [input_table[0]]

    for i in range(1, len(input_table)):
        output += get_out_lines(input_table[i])

    write_table(output, args.output)


if __name__ == "__main__":
    main()
