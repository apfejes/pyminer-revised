import sys
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.stats import chi2, chi2_contingency
from scipy.stats.contingency import margins
import argparse

import matplotlib.pyplot as plt

from modules.common_functions import write_table, read_table


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-input_file", "-i", "-infiles",
                        help="the file containing the contingency table")
    parser.add_argument("-out",
                        help="directory for output")
    parser.add_argument("-negative_control", "-neg_ctrl", "-neg", "-n",
                        help="the name of the treatment that is the negative control. This changes the stats "
                             "from a test of independence to a goodness of fit relative to the negative control.")

    return parser.parse_args()


def residuals(observed, expected):
    return (observed - expected) / np.sqrt(expected)


def adj_res(observed, expected=None):
    # https://journals.sagepub.com/doi/pdf/10.1177/0013164403251280
    n = observed.sum()
    rsum, csum = margins(observed)
    if expected is not None:
        f = expected
    else:
        f = rsum * csum / n
    res = (observed - f) / np.sqrt(f)
    return res / np.sqrt((1 - rsum / n) * (1 - csum / n))


def res_to_p(residual, dof):
    chi = residual ** 2
    return 1 - chi2.cdf(chi, dof)


def cont_to_p_independent(observed, leave_out=None):
    if leave_out is not None:
        row_vect = np.arange(np.shape(observed)[0]).tolist()

        # remove the left out index
        row_vect.pop(leave_out)
        row_vect = np.array(row_vect)
        observed = observed[row_vect, :]
    chi2, p, dof, expected = chi2_contingency(observed)
    adjusted_residuals = adj_res(observed)
    p = res_to_p(adjusted_residuals, dof)
    p = correct_pvalues_for_multiple_testing(p)
    return p, adjusted_residuals


def cont_to_p_relative(observed, col_index, leave_out=None):
    # if we need to leave out a row, leave it out
    if leave_out is not None:
        row_vect = np.arange(np.shape(observed)[0]).tolist()
        # remove the left out index
        row_vect.pop(leave_out)
        row_vect = np.array(row_vect)
        observed = observed[row_vect, :]
    # calculate the column sums
    csum = np.sum(observed, axis=0)
    # calculate the expected relative proportions and counts
    col_expected_proportions = observed[:, col_index] / csum[col_index]
    expected = np.zeros(np.shape(observed))
    for i in range(0, np.shape(observed)[1]):
        expected[:, i] = col_expected_proportions * csum[i]
    # make the minimum expected value 1...
    num_expected_zero = np.sum(expected == 0)
    if num_expected_zero > 0:
        print('\tWARNING: we found', num_expected_zero,
              'fields that had an expected value == 0. P-values may be a bit wonky...')
        expected[expected == 0] = 1

    # degree of freedom is calculate as per usual with one except. We subtract the number
    # of cells from the negative control, because we already know their values.
    dof = observed.size - sum(observed.shape) + observed.ndim - 1 - observed.shape[1]

    # calculate the residuals
    adjusted_residuals = adj_res(observed, expected=expected)
    p = res_to_p(adjusted_residuals, dof)
    p = correct_pvalues_for_multiple_testing(p)
    return p, adjusted_residuals


def reinsert_missing_row(table, index):
    temp_table = np.zeros((table.shape[0] + 1, table.shape[1]))
    temp_table[:index, :] = table[:index, :]
    temp_table[index, :] = np.nan
    temp_table[index + 1:, :] = table[index:, :]
    return temp_table


def leave_one_out_chi(observed, col_index=None, gof=False):
    # leave one out relative goodness of fit
    p_list = []
    adj_res_list = []

    # go through each row, leaving it out
    for i in range(0, np.shape(observed)[0]):
        if gof:
            temp_p, temp_adj_res = cont_to_p_relative(observed, col_index, leave_out=i)
        else:
            temp_p, temp_adj_res = cont_to_p_independent(observed, leave_out=i)
        temp_p = reinsert_missing_row(temp_p, i)
        temp_adj_res = reinsert_missing_row(temp_adj_res, i)
        p_list.append(temp_p)
        adj_res_list.append(temp_adj_res)
    p_list = np.array(p_list)
    adj_res_list = np.array(adj_res_list)

    # get the minimum abs residual, and maximum p-values
    p_list = np.nanmax(p_list, axis=0)

    # print("\n\nfinal p-value matrix:\n",p_list)
    adj_res_mat = np.zeros(p_list.shape)
    for i in range(0, adj_res_list.shape[1]):
        for j in range(0, adj_res_list.shape[2]):
            temp_argmax = np.nanargmin(abs(adj_res_list[:, i, j]))
            adj_res_mat[i, j] = adj_res_list[temp_argmax, i, j]
    print("\n\nfinal adj residual matrix:\n", adj_res_mat)
    print(p_list.shape, adj_res_mat.shape)
    return p_list, adj_res_mat


# this function was adopted from emre's stackoverflow answer found here:
# https://stackoverflow.com/questions/7450957/how-to-implement-rs-p-adjust-in-python
def correct_pvalues_for_multiple_testing(pvalues, correction_type="Benjamini-Hochberg"):
    """                                                                                                   
    consistent with R - print correct_pvalues_for_multiple_testing([0.0, 0.01, 0.029,
    0.03, 0.031, 0.05, 0.069, 0.07, 0.071, 0.09, 0.1])
    """
    from numpy import array, empty
    pvalues = array(pvalues)
    original_shape = None

    # convert to linear if needed
    if len(np.shape(pvalues)) > 1:
        needs_reshaping = True
        original_shape = np.shape(pvalues)
        new_shape = pvalues.size
        pvalues = pvalues.reshape(new_shape)
    else:
        needs_reshaping = False

    n = int(pvalues.shape[0])
    new_pvalues = empty(n)
    if correction_type == "Bonferroni":
        new_pvalues = n * pvalues
    elif correction_type == "Bonferroni-Holm":
        values = [(pvalue, i) for i, pvalue in enumerate(pvalues)]
        values.sort()
        for rank, vals in enumerate(values):
            pvalue, i = vals
            new_pvalues[i] = (n - rank) * pvalue
    elif correction_type == "Benjamini-Hochberg":
        values = [(pvalue, i) for i, pvalue in enumerate(pvalues)]
        values.sort()
        values.reverse()
        new_values = []
        for i, vals in enumerate(values):
            rank = n - i
            pvalue, index = vals
            new_values.append((n / rank) * pvalue)
        for i in range(0, int(n) - 1):
            if new_values[i] < new_values[i + 1]:
                new_values[i + 1] = new_values[i]
        for i, vals in enumerate(values):
            pvalue, index = vals
            new_pvalues[index] = new_values[i]
    if needs_reshaping:
        new_pvalues = new_pvalues.reshape(original_shape)
    return new_pvalues


def process_table(in_table, colnames, rownames):
    in_table = in_table.tolist()
    in_table = [colnames] + in_table
    for i in range(1, len(rownames)):
        in_table[i] = [rownames[i]] + in_table[i]
    return in_table


def do_heatmap(in_mat_str, global_cmap, out, mat_name=None, file_name=None):
    plt.clf()
    try:
        in_mat_num = np.array(in_mat_str[1:, 1:], dtype=float)
    except Exception:
        in_mat_num = in_mat_str[1:, 1:].tolist()
        for i in range(0, len(in_mat_num)):
            for j in range(0, len(in_mat_num[0])):
                in_mat_num[i][j] = eval(in_mat_num[i][j])
        in_mat_num = np.array(in_mat_num, dtype=bool)
    in_mat_df = pd.DataFrame(in_mat_num,
                             columns=in_mat_str[0, 1:],
                             index=in_mat_str[1:, 0],
                             copy=True)
    heatmap = sns.clustermap(in_mat_df, cmap=global_cmap)
    heatmap.fig.suptitle(mat_name)
    plt.savefig(out + file_name + '.png', dpi=600, bbox_inches='tight')


def main():

    args = parse_arguments()

    if args.out[-1] != '/':
        args.out += '/'

    contingency_table = read_table(args.contingency_file)
    pyminer_chi_res(contingency_table, args.out, args.negative_control)


def pyminer_chi_res(contingency_table, out, negative_control):
    # read in the contingency table
    chi_table_str = np.array(contingency_table)
    colnames = chi_table_str[0, :].tolist()
    rownames = chi_table_str[:, 0].tolist()
    treatment_vector = chi_table_str[0, 1:].tolist()
    for row in range(len(rownames)):
        rownames[row] = rownames[row].replace("sample_group_", "")
    treatment_idx_dict = {key: value for value, key in enumerate(treatment_vector)}
    chi_table_int = np.array(np.array(chi_table_str[1:, 1:], dtype=float), dtype=int)
    obs = chi_table_int[:]
    # example
    # obs = np.array([[33, 196, 136, 32], [55, 190, 71, 13], [150, 130, 75, 31]])
    if negative_control is not None:
        if negative_control not in treatment_idx_dict:
            sys.exit("couldn't find {} in the contingency table!".format(negative_control))
        neg_index = treatment_idx_dict[negative_control]
        # if there is a designated negative control
        # manually calculate the chi-square based on the expected values
        # from the negative control vector
        print("\n\np-values when relative to:", neg_index, '\n')
        p_table, temp_adj_residuals = leave_one_out_chi(obs, neg_index, gof=True)
    # print(p_table)
    # print(temp_adj_residuals)
    else:
        p_table, temp_adj_residuals = leave_one_out_chi(obs, gof=False)

    global_cmap = sns.color_palette("coolwarm", 256)
    # get rid of pesky zeros from floating point error...
    tiny = np.finfo(p_table.dtype).tiny
    neg_log_10_p = -np.log10(p_table + tiny)
    sig_bool_table = p_table < 0.05
    sig_bool_table_str = np.array(process_table(sig_bool_table))
    p_table_str = np.array(process_table(p_table, colnames=colnames, rownames=rownames))
    neg_log_10_p_str = np.array(process_table(neg_log_10_p, colnames=colnames, rownames=rownames))
    temp_adj_residuals_str = np.array(process_table(temp_adj_residuals, colnames=colnames, rownames=rownames))

    print("p_table_str", p_table_str)
    print("neg_log_10_p_str", neg_log_10_p_str)
    print("sig_bool_table_str", sig_bool_table_str)
    print("temp_adj_residuals_str", temp_adj_residuals_str)

    do_heatmap(p_table_str, out=out, mat_name="BH corrected\np-values",
               file_name="bh_corrected_pvals", global_cmap=global_cmap)
    do_heatmap(neg_log_10_p_str, out=out, mat_name="-log10 BH corrected\np-values",
               file_name="neg_log10_BH_pvals", global_cmap=global_cmap)
    do_heatmap(temp_adj_residuals_str, out=out, mat_name="Adjusted residuals",
               file_name="adjusted_residuals", global_cmap=global_cmap)
    do_heatmap(sig_bool_table_str, out=out, mat_name="Significance boolean table",
               file_name="significance_boolean_table", global_cmap=global_cmap)

    # make some files
    write_table(p_table_str, out + "BH_p_table.tsv")
    write_table(neg_log_10_p_str, out + "neg_log10_BH_p_table.tsv")
    write_table(temp_adj_residuals_str, out + "adjusted_residuals_table.tsv")
    write_table(sig_bool_table_str, out + "significance_boolean_table.tsv")


if __name__ == "__main__":
    main()
