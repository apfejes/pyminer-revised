import numpy as np
import argparse
import matplotlib.pyplot as plt

from modules.common_functions import import_dict, read_table


def parse_arguments(parser=None):
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("-cont_table",
                        help="the file containing the contingency table")
    parser.add_argument("-residuals",
                        help="the file containing the residuals table")
    parser.add_argument("-lineage_dict",
                        help="the lineage dictionary .pkl file")
    parser.add_argument("-out",
                        help="directory for output")

    return parser.parse_args()


def get_norm_vect(in_vect):
    temp_max = max(abs(np.array(in_vect)))
    in_vect = list(in_vect)
    fake_vect = [-temp_max] + in_vect + [temp_max]
    fake_vect = np.array(fake_vect)
    fake_vect = fake_vect - min(fake_vect)
    fake_vect = fake_vect / max(fake_vect)
    return fake_vect[1:-1].tolist()


def make_master_plot(pos, base_colors, name_dict):

    for i, colour in enumerate(base_colors):
        plt.scatter(pos[i, 0], pos[i, 1],
                    s=1000, c=np.array([colour]), label=name_dict[i])


def add_coloration(pos, colors):
    plt.scatter(pos[:, 0], pos[:, 1],
                s=400, c=colors, edgecolors="white", cmap="coolwarm",
                linewidths=2.5)


def main():

    args = parse_arguments()

    if args.out[-1] != '/':
        args.out += '/'

    master_lineage_dict = import_dict(args.lineage_dict)
    resid_table = np.array(read_table(args.residuals))
    classes = resid_table[0, 1:].tolist()
    resid_array = np.array(resid_table[1:, 1:], dtype=float)

    # get the positions of all the points
    pos = master_lineage_dict["plot"]["pos"]
    colors = master_lineage_dict["plot"]["colors"]
    names = master_lineage_dict["plot"]["node_names"]

    # first go through just plotting the
    for i in range(0, len(classes)):
        temp_res_vect = resid_array[:, i]

        # convert residuals to colors get_colors
        color_vect = get_norm_vect(temp_res_vect)
        make_master_plot(pos, colors, names)
        add_coloration(pos, color_vect)
        plt.show()


if __name__ == "__main__":
    main()
