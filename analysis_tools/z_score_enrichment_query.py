

import argparse
import logging

logger = logging.getLogger(__name__)


def parse_arguments(parser=None):
    """ Define the arguments available on the command line."""
    if not parser:
        parser = argparse.ArgumentParser()

    parser.add_argument("--input_file",
                        help="the input matrix which needs to be transformed",
                        required=True)

    parser.add_argument("--col_order",
                        help="column order to search for in the enrichment score",
                        required=True)

    parser.add_argument('--keep_nan',
                        help="remove lines ",
                        action='store_false',
                        default=True)

    args = parser.parse_args()
    return args


class ZScoreEnrichment(object):

    def __init__(self, enrichment_file, strip_nan=True):
        self.data = self.read_enrichment_file(enrichment_file, strip_nan)

    def read_enrichment_file(self, enrichment_file, strip_nan=True):
        """
        Take a file and convert it into a data set for further use.
        :param enrichment_file: the z_score_enrichment.txt file
        :param strip_nan: remove lines with 'nan' in the first sample.  (Usually, theyre' all nan or not)
        :return:
        """

        data = {}
        with open(enrichment_file, "r") as e_f:
            not_first = False
            for line in e_f:
                if not_first:
                    split_line = line.split('\t')
                    if strip_nan and split_line[1] == 'nan':
                        continue
                    gene = split_line[0]
                    data[gene] = [float(i) for i in split_line[1:]]
                else:  # header
                    not_first = True

        return data

    def find_order(self, order):
        """
        figure out if the order of the enrichmnt scores matches what
        :param order:
        :return:
        """
        if not order:
            raise Exception("No order given")

        pattern_matched = []
        for row in self.data:
            fits_pattern = True

            for i in range(len(order) - 1):
                if not fits_pattern:
                    break
                t = self.data[row]
                if t[order[i]] > t[order[i+1]]:
                    fits_pattern = False

            if fits_pattern and self.data[row][-1] > 10:
                pattern_matched.append(row)

        return pattern_matched

    def print_enrichment(self, genes, sort_column=-1):

        print_list = [[g] + self.data[g] for g in genes]
        print_list.sort(key=lambda x: x[sort_column])
        return print_list


def main():
    """ Wrapper to run the code in this file from the command line."""
    args = parse_arguments()

    zs = ZScoreEnrichment(enrichment_file=args.input_file,
                          strip_nan=args.keep_nan)

    o = [int(i) for i in args.col_order.split(',')]
    order_genes = zs.find_order(order=o)
    print_list = zs.print_enrichment(order_genes)

    for p in print_list:
        logger.debug(p)

    for p in print_list:
        logger.debug(p[0])

    logger.info(len(print_list), "records found")


if __name__ == "__main__":
    main()
