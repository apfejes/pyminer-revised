""" Unit test for generic process"""

import time
import unittest

from multiprocessing import Process
from multiprocessing import Queue

from modules.multiprocessing.generic_process import TERMINATOR, process_monitor, LockedInteger, LockedBoolean, \
    GenericProcess


class TestGenericThread(unittest.TestCase):
    def test_single_generic_processes(self):

        input_queue = Queue(100)
        output_queue = Queue(100)
        error_queue = Queue(100)
        terminator_obj = LockedInteger()
        death_obj = LockedBoolean()
        process_list = []

        time_start = time.time()
        g_process = GenericProcess(
            incoming_queue=input_queue,
            outgoing_queue=output_queue,
            error_queue=error_queue,
            terminator_obj=terminator_obj,
            death_obj=death_obj,
            terminators_expected=1,
            process_name="generic process",
        )
        proc = Process(target=g_process.run)
        proc.daemon = True
        proc.start()

        process_list.append(proc)

        for line_no in range(100000):
            input_queue.put(f"sample data {str(line_no)}")

        input_queue.put(TERMINATOR)

        process_monitor(error_queue=error_queue, process_list=process_list)

        print(f"time taken = {time.time() - time_start}")
        self.assertEqual(terminator_obj.value(), 1)

    def test_two_generic_processes(self):
        process_count = 2

        input_queue = Queue(100)
        output_queue = Queue(100)
        error_queue = Queue(100)
        terminator_obj = LockedInteger()
        death_obj = LockedBoolean()
        process_list = []

        time_start = time.time()
        for process_idx in range(process_count):
            g_process = GenericProcess(
                incoming_queue=input_queue,
                outgoing_queue=output_queue,
                error_queue=error_queue,
                terminator_obj=terminator_obj,
                death_obj=death_obj,
                terminators_expected=process_count,
                process_name=f"generic process {process_idx}",
            )
            proc = Process(target=g_process.run)
            proc.daemon = True
            proc.start()

            process_list.append(proc)

        for line_no in range(100000):
            input_queue.put(f"sample data {str(line_no)}")

        input_queue.put(TERMINATOR)

        process_monitor(error_queue=error_queue, process_list=process_list)

        print(f"time taken = {time.time() - time_start}")
        self.assertEqual(terminator_obj.value(), 1)

    def test_ten_generic_processes(self):
        process_count = 10

        input_queue = Queue(100)
        output_queue = Queue(100)
        error_queue = Queue(100)
        terminator_obj = LockedInteger()
        death_obj = LockedBoolean()
        process_list = []

        time_start = time.time()
        for process_idx in range(process_count):
            g_process = GenericProcess(
                incoming_queue=input_queue,
                outgoing_queue=output_queue,
                error_queue=error_queue,
                terminator_obj=terminator_obj,
                death_obj=death_obj,
                terminators_expected=1,
                process_name=f"generic process {process_idx}",
            )
            proc = Process(target=g_process.run)
            proc.daemon = True
            proc.start()

            process_list.append(proc)

        for line_no in range(100000):
            input_queue.put(f"sample data {str(line_no)}")

        input_queue.put(TERMINATOR)

        process_monitor(error_queue=error_queue, process_list=process_list)

        print(f"time taken = {time.time() - time_start}")
        self.assertEqual(terminator_obj.value(), 1)
